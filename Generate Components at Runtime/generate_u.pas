unit generate_u;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls;

type
  TForm1 = class(TForm)
    btnGenerate: TButton;
    Panel1: TPanel;
    btnGenratePanel: TButton;
    procedure FormCreate(Sender: TObject);
    procedure myClick(sender: TObject);
    procedure btnGenerateClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  Counter:Integer;
  i:integer = 0;

implementation

{$R *.dfm}


procedure TForm1.myClick(Sender: TObject);
begin
  with Sender as TPanel do
    Self.Caption := ClassName + ' ' + Name;
end;

procedure TForm1.btnGenerateClick(Sender: TObject);
begin
    with TPanel.Create(self) do
    begin
      Left := 380;
      Top := 8 + i;
      Width := 120;
      Height := 40;
      Name := 'ThisButton' + IntToStr(i);
      Caption := 'There' + IntToStr(i);
      OnClick :=  MyClick;  { a procedure I defined somewhere else }
      Parent := Form1;
    end; {end with}
  inc(i, 40);
end;

procedure TForm1.FormCreate(Sender: TObject);
var
  B: TButton;
  P: TPanel;
  i: Integer;

begin
    Counter:=0;
  for i := 0 to 9 do
  begin
    B := TButton.Create(Self);
    B.Caption := Format('Button %d', [i]);
    B.Parent := Self;
    B.Height := 23;
    B.Width := 100;
    B.Left := 10;
    B.Top := 10 + i * 25;

    P := TPanel.Create(Self);
    P.Caption := Format('Button %d', [i]);
    P.Parent := Self;
    P.Height := 23;
    P.Width := 100;
    P.Left := 250;
    P.Top := 10 + i * 25;
  end;
end;



end.
