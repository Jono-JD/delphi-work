unit term3Test_u;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, MAth, ComCtrls, ExtCtrls, jpeg, pngimage, Buttons;

type
  TfrmTest = class(TForm)
    pcTest: TPageControl;
    pcQuestion1: TTabSheet;
    GroupBox1: TGroupBox;
    edtInput: TEdit;
    btnChange: TButton;
    rbCopy: TRadioButton;
    rbCopyBackwards: TRadioButton;
    pnlInfo: TPanel;
    pcQuestion2: TTabSheet;
    btnGenRandom: TButton;
    lblRandNum: TLabel;
    btnFactor: TButton;
    btnDigitSum: TButton;
    btnDivide: TButton;
    pcQuestion3: TTabSheet;
    lblFactor: TLabel;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    GroupBox4: TGroupBox;
    GroupBox5: TGroupBox;
    redOut: TRichEdit;
    btnPlayer1: TButton;
    bpRestart: TBitBtn;
    imgDie: TImage;
    Label1: TLabel;
    lblHighScore: TLabel;
    procedure FormActivate(Sender: TObject);
    procedure btnChangeClick(Sender: TObject);
    procedure btnGenRandomClick(Sender: TObject);
    procedure btnFactorClick(Sender: TObject);
    procedure btnDigitSumClick(Sender: TObject);
    procedure btnDivideClick(Sender: TObject);
    procedure bpRestartClick(Sender: TObject);
    procedure btnPlayer1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTest: TfrmTest;
  iNum: integer;
  iFact: integer;
  iSum: integer;
  highScore: integer;

implementation

{$R *.dfm}

procedure TfrmTest.FormActivate(Sender: TObject);
begin
  // Question 1.1.1 - 1.1.5
  btnChange.Caption := 'Make Change';
  edtInput.Clear;
  rbCopy.Checked := True;
  pnlInfo.Color := clTeal;
  frmTest.Font.Size := 14;
  highScore := 0;
end;

procedure TfrmTest.btnChangeClick(Sender: TObject);
var
  sInput: string;
  sBack: string;
  i: integer;
begin
  // Question 1.2.1 - 1.2.3
  sInput := edtInput.Text;
  sBack := '';
  for i := 1 to length(sInput) do
  begin
    sBack := sInput[i] + sBack;
  end;
  if rbCopy.Checked then
  begin
    pnlInfo.Caption := sInput;
  end
  else if rbCopyBackwards.Checked then
  begin
    pnlInfo.Caption := sBack;
  end;

end;

procedure TfrmTest.btnGenRandomClick(Sender: TObject);
begin
  // Question 2.1
  iNum := RandomRange(100, 10000);
  lblRandNum.Caption := IntToStr(iNum);
end;

procedure TfrmTest.btnFactorClick(Sender: TObject);
var
  i: integer;
begin
  // Question 2.2
  iFact := 0;
  for i := 1 to iNum do
  begin
    if iNum mod i = 0 then
    begin
      inc(iFact);
    end;
  end;
  lblFactor.Caption := IntToStr(iFact);

end;

procedure TfrmTest.btnDigitSumClick(Sender: TObject);
var
  i: integer;
  sNum: string;
begin
  // Question 2.3
  sNum := FormatFloat('0', iNum);
  for i := 1 to length(sNum) do
  begin
    iSum := iSum + StrToInt(sNum[i]);
  end;
  showMessage('The Sum of the digits is ' + IntToStr(iSum));
end;

procedure TfrmTest.btnDivideClick(Sender: TObject);
var
  myResult: real;
begin
  // Question 2.4
  if iFact = 0 then
  begin
    showMessage('Cannot Divide by 0, try Calculating your factors');
  end
  else
  begin
    myResult := iNum / iFact
  end;
  pnlInfo.Caption := FormatFloat('0.00', myResult);
end;

procedure TfrmTest.btnPlayer1Click(Sender: TObject);
var
  isFirst: boolean;
  twelve: boolean;
  dice1, dice2: integer;
  score: integer;

begin
  // Question 3.1
  redOut.Clear;
  twelve := false;
  score:=0;
  while not twelve do
  begin
    score := score + 1;
    dice1 := RandomRange(1, 7);
    dice2 := RandomRange(1, 7);
    redOut.Lines.Add(IntToStr(dice1) + ',' + IntToStr(dice2));
    if dice1 + dice2 = 12 then
    begin
      twelve := True;
    end;
  end;
  if (highScore = 0) or (score > highscore) then
    begin
      highScore := score;
      lblHighScore.Caption := IntToStr(Highscore);
    end;

end;

procedure TfrmTest.bpRestartClick(Sender: TObject);
begin
  // Question 3.2
  lblHighScore.Caption := '';
  highScore := 0;
  redOut.Clear;
end;

end.
