unit clsLearners;

interface

uses math, strutils, sysutils;

type
  TLearner = class(TObject)

  private
    fName: String;
    fSurname: String;
    fYear: integer;
    fPassword: string;
    fUsername: string;
    fEmail: String;
  public
    constructor create(name, surname: string; Year: integer);
    function ToString: string;
    function getUsername : string;
    procedure makeUsername(sName, sSurname:string ; iYear:integer);
    procedure makePassword;
    function validate(sUsername:string):boolean;
    procedure setPassword(sPassword:string);
  end;

implementation

{ TLearner }

constructor TLearner.create(name, surname: string; Year: integer);
begin
  fName := name;
  fSurname := surname;
  fYear := Year;
  makeUsername(fName, fSurname, fYear);
  makePassword;
  if validate(fUsername) then
  begin
    fEmail:=fUsername+'phahs.org.za';
  end
  else
  begin
    fEmail:='Invalid Username';
  end;
end;

function TLearner.getUsername: string;
begin
    Result:=fUsername;
end;

procedure TLearner.makePassword;
begin
    fPassword:='pha'+IntToStr(RandomRange(0, 9))+INtToStr(RandomRange(0, 9))+INtToStr(RandomRange(0, 9))+INtToStr(RandomRange(0, 9));
end;

procedure TLearner.makeUsername(sName, sSurname:string; iYear:integer);
var
  sYear:string;
begin
    sYear:=IntToStr(iYear);
    fUsername:= copy(sSurname, 0, 3);
    fUsername:= fUsername + copy(sName, 0, 3);
    fUsername:= fUsername + sYear[length(sYear)-1] + sYear[length(sYear)];
end;

procedure TLearner.setPassword(sPassword: string);
begin
  fPassword:=sPassword;
end;

function TLearner.ToString: string;
begin
  result := 'Name: ' + fName + #13 + 'Surname: ' + fSurname + #13 +
    'Username: ' + fUsername + #13 + 'Password: ' + fPassword + #13 +
    'Email: ' + fEmail;
end;


function TLearner.validate(sUsername:string): boolean;
var
  i: Integer;
begin
    for i := 0 to length(sUsername) do
    begin
      if sUsername[i] = ' ' then
      begin
        Result:=False;
      end;
    end;
    Result:=True;
end;

end.
