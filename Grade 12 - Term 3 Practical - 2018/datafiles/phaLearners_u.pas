unit phaLearners_u;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, clsLearners, Spin, ComCtrls, ExtCtrls, DateUtils;

type
  TfrmRegister = class(TForm)
    lblName: TLabel;
    lblSurname: TLabel;
    edtName: TEdit;
    edtSurname: TEdit;
    btnPassword: TButton;
    sedYear: TSpinEdit;
    lblYEar: TLabel;
    edtPassword: TEdit;
    btnCreateUser: TButton;
    chkCustomPassword: TCheckBox;
    redOut: TRichEdit;
    Panel1: TPanel;
    lblDisplay: TLabel;
    procedure FormActivate(Sender: TObject);
    procedure btnCreateUserClick(Sender: TObject);
    procedure chkCustomPasswordClick(Sender: TObject);
    procedure btnPasswordClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRegister: TfrmRegister;
  objLearner: Tlearner;

implementation

{$R *.dfm}


procedure TfrmRegister.btnCreateUserClick(Sender: TObject);
begin
    objLearner:=TLearner.create(edtName.Text, edtSurname.Text, sedYear.Value);
    redOut.Clear;
    chkCustomPassword.Enabled:=True;
    redOut.Lines.Add(objLearner.ToString);
end;

procedure TfrmRegister.btnPasswordClick(Sender: TObject);
begin
    if edtPassword.Text = '' then
    begin
      showMessage('Password Field may not be empty');
    end
    else if length(edtPassword.Text) < 6 then
    begin
      showMessage('Password has to be longer that 6 characters');
    end
    else
    begin
      objLearner.setPassword(edtPassword.Text);
      redout.Clear;
      redout.Lines.Add(objLearner.ToString);
    end;
end;

procedure TfrmRegister.chkCustomPasswordClick(Sender: TObject);
begin
    if chkCustomPassword.Checked then
    begin
        btnPassword.Enabled:=True;
        edtPassword.Enabled:=True;
    end
    else
    begin
        btnPassword.Enabled:=false;
        edtPassword.Enabled:=False;
    end;
end;

procedure TfrmRegister.FormActivate(Sender: TObject);
var
    date:string;
begin
    ShortDateFormat:='yyyy/mm/dd';
    date:=DateToStr(now);
    sedYear.Value:=StrToInt(copy(date, 0, 4));
    chkCustomPassword.Enabled:=False;
    btnPassword.Enabled:=false;
    edtPassword.Enabled:=False;


end;

end.
