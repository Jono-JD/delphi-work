object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'The Cell Shop'
  ClientHeight = 340
  ClientWidth = 729
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 16
  object GroupBox1: TGroupBox
    Left = 8
    Top = 8
    Width = 361
    Height = 321
    Caption = 'Register new clients'
    TabOrder = 0
    object edtNameSurname: TLabeledEdit
      Left = 16
      Top = 48
      Width = 121
      Height = 24
      EditLabel.Width = 114
      EditLabel.Height = 16
      EditLabel.Caption = 'Name and Surname'
      TabOrder = 0
    end
    object edtCellNr: TLabeledEdit
      Left = 16
      Top = 104
      Width = 121
      Height = 24
      EditLabel.Width = 70
      EditLabel.Height = 16
      EditLabel.Caption = 'Cell Number'
      TabOrder = 1
    end
    object btnValidateName: TButton
      Left = 192
      Top = 46
      Width = 137
      Height = 25
      Caption = 'Validate Name'
      TabOrder = 2
      OnClick = btnValidateNameClick
    end
    object btnFormatCell: TButton
      Left = 192
      Top = 102
      Width = 137
      Height = 25
      Caption = 'Format Cell Number'
      TabOrder = 3
      OnClick = btnFormatCellClick
    end
  end
  object GroupBox2: TGroupBox
    Left = 375
    Top = 8
    Width = 346
    Height = 324
    Caption = 'Competition'
    TabOrder = 1
    object btnPossible: TButton
      Left = 16
      Top = 46
      Width = 112
      Height = 25
      Caption = 'Possible Winners'
      TabOrder = 0
      OnClick = btnPossibleClick
    end
    object redOutput: TRichEdit
      Left = 134
      Top = 48
      Width = 209
      Height = 249
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
    end
    object btnWinners: TButton
      Left = 16
      Top = 102
      Width = 97
      Height = 25
      Caption = 'Winners'
      TabOrder = 2
      OnClick = btnWinnersClick
    end
    object btnWriteFile: TButton
      Left = 16
      Top = 158
      Width = 97
      Height = 25
      Caption = 'Write to File'
      TabOrder = 3
      OnClick = btnWriteFileClick
    end
    object BitBtn1: TBitBtn
      Left = 24
      Top = 272
      Width = 75
      Height = 25
      DoubleBuffered = True
      Kind = bkClose
      ParentDoubleBuffered = False
      TabOrder = 4
    end
  end
end
