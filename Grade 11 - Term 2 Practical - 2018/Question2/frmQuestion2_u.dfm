object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Question 2'
  ClientHeight = 378
  ClientWidth = 930
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnActivate = FormActivate
  PixelsPerInch = 96
  TextHeight = 19
  object GroupBox1: TGroupBox
    Left = 8
    Top = 8
    Width = 321
    Height = 289
    Caption = 'Dog Breed Life Expectancy and Popularity'
    TabOrder = 0
    object btnQ2_1: TButton
      Left = 40
      Top = 40
      Width = 233
      Height = 25
      Caption = 'Q2_1 Random Popularity'
      TabOrder = 0
      OnClick = btnQ2_1Click
    end
    object btnQ2_2: TButton
      Left = 40
      Top = 88
      Width = 233
      Height = 25
      Caption = 'Q2_2 Display Information'
      TabOrder = 1
      OnClick = btnQ2_2Click
    end
    object btnQ2_3Stats: TButton
      Left = 40
      Top = 135
      Width = 233
      Height = 25
      Caption = 'Q2_3 Calculate Stats'
      TabOrder = 2
      OnClick = btnQ2_3StatsClick
    end
    object btnQ2_4Popular: TButton
      Left = 40
      Top = 184
      Width = 233
      Height = 25
      Caption = 'Q2_4 Popularity 5 to 7'
      TabOrder = 3
      OnClick = btnQ2_4PopularClick
    end
    object btnQ2_5Sort: TButton
      Left = 40
      Top = 233
      Width = 233
      Height = 25
      Caption = 'Q2_5 Sorted List'
      TabOrder = 4
      OnClick = btnQ2_5SortClick
    end
  end
  object redOutput: TRichEdit
    Left = 352
    Top = 47
    Width = 537
    Height = 250
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    ScrollBars = ssVertical
    TabOrder = 1
  end
  object edtAverage: TLabeledEdit
    Left = 360
    Top = 336
    Width = 177
    Height = 27
    EditLabel.Width = 166
    EditLabel.Height = 19
    EditLabel.Caption = 'Average life expectancy'
    TabOrder = 2
  end
  object edtHighest: TLabeledEdit
    Left = 608
    Top = 336
    Width = 233
    Height = 27
    EditLabel.Width = 228
    EditLabel.Height = 19
    EditLabel.Caption = 'Dog with highest life expectancy'
    TabOrder = 3
  end
  object BitBtn1: TBitBtn
    Left = 128
    Top = 337
    Width = 75
    Height = 25
    DoubleBuffered = True
    Kind = bkClose
    ParentDoubleBuffered = False
    TabOrder = 4
  end
end
