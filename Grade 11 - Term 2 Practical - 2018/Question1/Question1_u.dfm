object frmRandom: TfrmRandom
  Left = 0
  Top = 0
  Caption = 'Question 2-'
  ClientHeight = 331
  ClientWidth = 468
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnActivate = FormActivate
  PixelsPerInch = 96
  TextHeight = 13
  object lblOutput: TLabel
    Left = 8
    Top = 221
    Width = 3
    Height = 13
  end
  object lblNum1: TLabel
    Left = 83
    Top = 82
    Width = 7
    Height = 25
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object lblNum2: TLabel
    Left = 336
    Top = 82
    Width = 7
    Height = 25
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object lblAND: TLabel
    Left = 200
    Top = 79
    Width = 46
    Height = 29
    Caption = 'AND'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -24
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object pnlInfo: TPanel
    Left = 0
    Top = 24
    Width = 468
    Height = 49
    Caption = 'Generate a voucher number'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -27
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
  end
  object pnlOutput: TPanel
    Left = 0
    Top = 126
    Width = 465
    Height = 79
    TabOrder = 1
    object btnHighest: TButton
      Left = 43
      Top = 25
      Width = 75
      Height = 33
      Caption = 'Highest'
      TabOrder = 0
      OnClick = btnHighestClick
    end
    object btnAve: TButton
      Left = 192
      Top = 25
      Width = 75
      Height = 33
      Caption = 'Average'
      Enabled = False
      TabOrder = 1
      OnClick = btnAveClick
    end
    object btnFactor: TButton
      Left = 345
      Top = 25
      Width = 75
      Height = 33
      Caption = 'Factor'
      Enabled = False
      TabOrder = 2
      OnClick = btnFactorClick
    end
  end
  object bmbRetry: TBitBtn
    Left = 368
    Top = 216
    Width = 97
    Height = 32
    DoubleBuffered = True
    Kind = bkCancel
    ParentDoubleBuffered = False
    TabOrder = 2
    OnClick = bmbRetryClick
  end
  object pnlVoucher: TPanel
    Left = 0
    Top = 272
    Width = 468
    Height = 49
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -27
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 3
  end
end
