unit text_u;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls;

type
  TForm1 = class(TForm)
    btnImport: TButton;
    btinExport: TButton;
    redOut: TRichEdit;
    Button1: TButton;
    procedure btnImportClick(Sender: TObject);
    procedure btinExportClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  F:textFile;
  fileName:string;
  line:string;

implementation

{$R *.dfm}

procedure TForm1.btinExportClick(Sender: TObject);
var
  i: Integer;
begin
    if inputQuery('Export File', 'Please insert file name', FileName) then
    begin
        FileName:='textfiles/'+fileName;
        if FileExists(fileName) then
        begin
          //showMessage('File Exists');
            AssignFile(F, fileName);
            Rewrite(F);
            for i := 0 to redOut.Lines.Count do
            begin
                writeln(F, redOut.Lines.Strings[i]);
            end;
            CloseFile(F);
        end
        else
        begin
          showMessage('File Does not Exist');
        end;
    end;
end;

procedure TForm1.btnImportClick(Sender: TObject);
begin
    if inputQuery('Import File', 'Please insert file name', FileName) then
    begin
        FileName:='textfiles/'+fileName;
        if FileExists(fileName) then
        begin
          //showMessage('File Exists');
            AssignFile(F, fileName);
            Reset(F);
            while not EoF(F) do
            begin
                readln(F, line);
                redOut.Lines.Add(line);
            end;
            CloseFile(F);
        end
        else
        begin
          showMessage('File Does not Exist');
        end;
    end;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
    redOut.Clear;
end;

end.
