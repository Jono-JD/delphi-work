object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 202
  ClientWidth = 447
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object btnImport: TButton
    Left = 8
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Import'
    TabOrder = 0
    OnClick = btnImportClick
  end
  object btinExport: TButton
    Left = 8
    Top = 39
    Width = 75
    Height = 25
    Caption = 'Export'
    TabOrder = 1
    OnClick = btinExportClick
  end
  object redOut: TRichEdit
    Left = 89
    Top = 8
    Width = 350
    Height = 186
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
  end
  object Button1: TButton
    Left = 8
    Top = 70
    Width = 75
    Height = 25
    Caption = 'Clear'
    TabOrder = 3
    OnClick = Button1Click
  end
end
