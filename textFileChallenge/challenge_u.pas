unit challenge_u;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls;

type
  TForm1 = class(TForm)
    redOut: TRichEdit;
    btnImportData: TButton;
    btnImportDelimeted: TButton;
    procedure btnImportDataClick(Sender: TObject);
    procedure btnImportDelimetedClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.btnImportDataClick(Sender: TObject);
var
    F:textfile;
    arrData: array[1..3] of string;
    len:integer;
    temp:string;
    i: Integer;
    j: Integer;
    v1,v2,v3:string;

begin
    redOut.Paragraph.TabCount:=3;
    redOut.Paragraph.Tab[0]:=100;
    redOut.Paragraph.Tab[1]:=200;
    redOut.Paragraph.Tab[2]:=300;
    redOut.SelAttributes.Style := redOut.SelAttributes.Style + [fsBold];
    redOut.SelAttributes.Style := redOut.SelAttributes.Style + [fsUnderline];
    redOut.SelText:= 'Name';
    redOut.SelAttributes.Style := redOut.SelAttributes.Style - [fsBold];
    redOut.SelAttributes.Style := redOut.SelAttributes.Style - [fsUnderline];
    redOut.SelText:= #9;
    redOut.SelAttributes.Style := redOut.SelAttributes.Style + [fsBold];
    redOut.SelAttributes.Style := redOut.SelAttributes.Style + [fsUnderline];
    redOut.SelText:= 'Surname';
    redOut.SelAttributes.Style := redOut.SelAttributes.Style - [fsBold];
    redOut.SelAttributes.Style := redOut.SelAttributes.Style - [fsUnderline];
    redOut.SelText:= #9;
    redOut.SelAttributes.Style := redOut.SelAttributes.Style + [fsBold];
    redOut.SelAttributes.Style := redOut.SelAttributes.Style + [fsUnderline];
    redOut.SelText:= 'Age';
    redOut.SelAttributes.Style := redOut.SelAttributes.Style - [fsBold];
    redOut.SelAttributes.Style := redOut.SelAttributes.Style - [fsUnderline];
    redOut.SelText:=#10;




    // This is my solution
    if fileExists('data.txt') then
    begin
        assignFile(F, 'data.txt');
        reset(F);
        len := 0;
        while not EoF(F) do
        begin
            readln(F, temp);
            inc(len);
        end;
        reset(F);
        for i := 1 to (len div 3) do
        begin
            for j := 1 to 3 do
            begin
                readln(F, arrData[j]);
            end;
            redOut.SelText:= arrData[1] +#9+ arrData[2] +#9+ arrData[3] + #10;
        end;
    end;

    redOut.SelText:=#10#13;
    // This is Sir's Solution
    {
    if fileExists('data.txt') then
    begin
      assignFile(F, 'data.txt');
      reset(F);
      while not EoF(F) do
      begin
        readln(F, v1);
        readln(F, v2);
        readln(F, v3);
        redOut.SelText:= v1+ #9 + v2 + #9 + v3 + #10;
      end;
    end;
    }
end;

procedure TForm1.btnImportDelimetedClick(Sender: TObject);
var
    F:textFile;
    line:string;
    sName, sSurname, sAge:string;

begin
    redOut.Paragraph.TabCount:=3;
    redOut.Paragraph.Tab[0]:=100;
    redOut.Paragraph.Tab[1]:=200;
    redOut.Paragraph.Tab[2]:=300;
    redOut.SelAttributes.Style := redOut.SelAttributes.Style + [fsBold];
    redOut.SelAttributes.Style := redOut.SelAttributes.Style + [fsUnderline];
    redOut.SelText:= 'Name';
    redOut.SelAttributes.Style := redOut.SelAttributes.Style - [fsBold];
    redOut.SelAttributes.Style := redOut.SelAttributes.Style - [fsUnderline];
    redOut.SelText:= #9;
    redOut.SelAttributes.Style := redOut.SelAttributes.Style + [fsBold];
    redOut.SelAttributes.Style := redOut.SelAttributes.Style + [fsUnderline];
    redOut.SelText:= 'Surname';
    redOut.SelAttributes.Style := redOut.SelAttributes.Style - [fsBold];
    redOut.SelAttributes.Style := redOut.SelAttributes.Style - [fsUnderline];
    redOut.SelText:= #9;
    redOut.SelAttributes.Style := redOut.SelAttributes.Style + [fsBold];
    redOut.SelAttributes.Style := redOut.SelAttributes.Style + [fsUnderline];
    redOut.SelText:= 'Age';
    redOut.SelAttributes.Style := redOut.SelAttributes.Style - [fsBold];
    redOut.SelAttributes.Style := redOut.SelAttributes.Style - [fsUnderline];
    redOut.SelText:=#10;


    if fileExists('data2.txt') then
    begin
        assignFile(F, 'data2.txt');
        reset(F);
      while not EoF(F) do
      begin
        readln(F, line);
        sName:= copy(line, 1, pos(',', line)-1);
        delete(line, 1, pos(',', line));

        sSurname:= copy(line, 1, pos(',', line)-1);
        delete(line, 1, pos(',', line));

        sAge:=line;

        redOut.SelText:= sName + #9 + sSurname + #9 + sAge + #10;
      end;
    end;
    redOut.SelText:=#10#13;
end;

end.
