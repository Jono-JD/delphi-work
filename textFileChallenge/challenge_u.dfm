object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 237
  ClientWidth = 580
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object redOut: TRichEdit
    Left = 8
    Top = 8
    Width = 401
    Height = 221
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
  end
  object btnImportData: TButton
    Left = 415
    Top = 8
    Width = 146
    Height = 33
    Caption = 'Import Data'
    TabOrder = 1
    OnClick = btnImportDataClick
  end
  object btnImportDelimeted: TButton
    Left = 415
    Top = 47
    Width = 146
    Height = 34
    Caption = 'Import Data 2 (Delimeted)'
    TabOrder = 2
    OnClick = btnImportDelimetedClick
  end
end
