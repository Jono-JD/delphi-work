object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 233
  ClientWidth = 498
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid1: TDBGrid
    Left = 0
    Top = 0
    Width = 497
    Height = 137
    DataSource = dmData.dsOwner
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object btnPopulate: TButton
    Left = 16
    Top = 143
    Width = 81
    Height = 25
    Caption = 'Populate'
    TabOrder = 1
    OnClick = btnPopulateClick
  end
  object cbPlateNr: TComboBox
    Left = 112
    Top = 143
    Width = 145
    Height = 21
    TabOrder = 2
  end
  object cbFirstName: TComboBox
    Left = 272
    Top = 143
    Width = 145
    Height = 21
    TabOrder = 3
  end
end
