unit ComboBox;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DataModule, StdCtrls, Grids, DBGrids;

type
  TForm1 = class(TForm)
    DBGrid1: TDBGrid;
    btnPopulate: TButton;
    cbPlateNr: TComboBox;
    cbFirstName: TComboBox;
    procedure btnPopulateClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.btnPopulateClick(Sender: TObject);
begin
    with dmData do
    begin
        tblOwner.Open;
        tblOwner.First;
        while not tblOwner.Eof do
        begin
            cbPlateNr.Items.Add(tblOwner['LicensePlateNum']);
            cbFirstName.Items.Add(tblOwner['FirstName']);
            tblowner.Next;
        end;
        //tblOwner.Close;
    end;
end;

end.
