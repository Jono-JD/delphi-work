Tier1_1
Klavek Class Pilot Raptor
10000
250000
Hull Strength: 36,225
Shield Modifier: 0.97
Crew: 120
Fore Weapons: 5
Aft Weapons: 2
Device Slots: 2
Bridge Officer Stations: 
	1 Lieutenant Tactical, 
	1 Commander Tactical/Pilot, 
	1 Lieutenant Engineering/Pilot, 
	1 Lieutenant Commander Science, 
	1 Lieutenant Universal
Console Modifications: 
	5 Tactical, 
	2 Engineering, 
	4 Science
Base Turn Rate: 20
Impulse Modifier: 0.24
Inertia: 75
+10 Weapon Power, +10 Engine Power
Can Load Dual Cannons
Console - Universal
Spatial Sinkhole Generator
Pilot Maneuvers
Cloaking Device
Starship Ability Package
Precise Weapon Systems
Tactical Maneuvering
Enhanced Weapon Systems
Devastating Weaponry
Subwarp Sheath


Tier1_2
Kortar Class Pilot Raptor
12000
300000
Hull Strength: 36,225
Shield Modifier: 0.97
Crew: 120
Fore Weapons: 5
Aft Weapons: 2
Device Slots: 2
Bridge Officer Stations: 
	1 Lieutenant Commander Tactical, 
	1 Commander Tactical/Pilot, 
	1 Lieutenant Engineering, 
	1 Lieutenant Science/Pilot, 
	1 Lieutenant Universal
Console Modifications: 
	5 Tactical, 
	3 Engineering, 
	3 Science
Base Turn Rate: 20
Impulse Modifier: 0.24
Inertia: 75
+10 Weapon Power, +10 Engine Power
Can Load Dual Cannons
Console - Universal
Microprojectile Barrage Launcher
Pilot Maneuvers
Cloaking Device
Starship Ability Package 
Precise Weapon Systems 
Tactical Maneuvering 
Enhanced Weapon Systems 
Devastating Weaponry 
Stay On Target


Tier2_1
Negh'Tev Heavy Battlecruiser
20000
500000
Hull Strength:
Shield Modifier: 1
Crew: 250
Fore Weapons: 4
Aft Weapons: 4
Device Slots: 3
Bridge Officer Stations: 
	1 Lieutenant Commander Tactical,
	1 Lieutenant Commander Engineer, 
	1 Commander Engineering, 
	1 Lieutenant Science, 
	1 Ensign Universal
Console Modifications: 
	3 Tactical, 
	4 Engineering, 
	3 Science
Base Turn Rate: 9 degrees/second
Impulse Modifier: 0.15
Friction/Traction: 25
+10 to Weapon Power and Engine Power
Console � Universal
Molecular Cohesion Nullifier
Can Equip Dual Cannons
Starship Ability Package
Absorptive Plating
Enhanced Weapon Banks
Enhanced Plating
Armored Hull
Explosive Polarity Shift
Cruiser Command Array
Command � Strategic Maneuvering
Command � Shield Frequency Modulation
Command � Weapon System Efficiency


Tier2_2
Kurak Class Battlecruiser
25000
750000
Hull Strength: 43,125
Shield Modifier: 1
Crew: 200
Fore Weapons: 5
Aft Weapons: 3
Device Slots: 3
Bridge Officer Stations: 
	1 Ensign Tactical, 
	1 Lieutenant Commander Tactical,
	1 Lieutenant Commander Intelligence,
	1 Command Engineering, 
	1 Lieutenant Science, 
	1 Lieutenant Commander Universal
Console Modifications: 
	4 Tactical, 
	5 Engineering, 
	1 Science
Base Turn Rate: 9 degrees/second
Impulse Modifier: 0.15
Inertia: 50
+10 Weapon Power, +10 Engine Power
Can Load Cannons
Cloaking Device
Console - Universal
Ablative Hazard Shielding
Starship Ability Package 
Absorptive Plating 
Enhanced Weapon Banks 
Enhanced Plating 
Armored Hull 
Emergency Weapon Cycle 
Cruiser Communications Array
Command - Strategic Maneuvering
Command - Shield Frequency Modulation
Command - Weapon System Efficiency


Tier3_1
Klinzhai Class Battlecruiser
35000
1000000
Hull Strength: 49,335
Shield Modifier: 1.1
Crew: 150
Fore Weapons: 4
Aft Weapons: 4
Device Slots: 4
Bridge Officer Stations: 
	1 Lieutenant Tactical/Command, 
	1 Lieutenant Engineering, 
	1 Commander Engineering/Command, 
	1 Lieutenant Commander Science, 
	1 Lieutenant Universal
Console Modifications:
 	3 Tactical, 
	4 Engineering, 
	4 Science
Base Turn Rate: 8
Impulse Modifier: 0.15
Inertia: 35
+10 Weapon Power, +10 Engine Power
Can Load Dual Cannons
Hangar Bays: 1
Hangar Bays loaded with To'Duj Fighters
Console - Universal
Tachyon Pulse Platform
Cloaking Device
Inspiration Abilities
Battle Preparation AI
Starship Ability Package
Absorptive Plating
Enhanced Weapon Banks
Enhanced Plating
Armored Hull
Improved Tachyon Beam
Cruiser Communications Array
Command - Shield Frequency Modulation
Command - Attract Fire


Tier3_2
An'Quat Class Battlecruiser
55000
1350000
Hull Strength: 49,335
Shield Modifier: 1.1
Crew: 1500
Fore Weapons: 4
Aft Weapons: 4
Device Slots: 4
Bridge Officer Stations: 
	1 Lieutenant Tactical, 
	1 Lieutenant Engineer, 
	1 Commander Engineer, 
	1 Lieutenant Science/Command, 
	1 Lieutenant Universal
Console Modifications: 
	3 Tactical, 
	5 Engineering, 
	3 Science
Base Turn Rate: 8
Impulse Modifier: 0.15
Inertia: 35
+10 Weapon Power, +10 Engine Power
Can Load Dual Cannons
Hangar Bays: 1
Hangar Bays loaded with To'Duj Fighters
Console - Universal
Fleet Support Platform
Cloaking Device
Inspiration Abilities
Battle Preparation
Starship Ability Package (Battlecruiser)
Absorptive Plating
Enhanced Weapon Banks
Enhanced Plating
Armored Hull
Improved Brace for Impact (Starship Trait)
Cruiser Communications Array
Command - Shield Frequency Modulation
Command - Attract Fire
