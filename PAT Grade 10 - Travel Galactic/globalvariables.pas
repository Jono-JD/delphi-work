unit globalVariables;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

{

These variables will be accessed through most Forms in this application.
This unit file serves as a storage area for all variables that contain
crutial information that is shared between more than one form
}

var
  SessionName:string;
  SessionSurname:string;
  SessionEmail:string;
  SessionContact:string;

  DestinationStatus:boolean;
  ShipStatus:boolean;
  ProtectionStatus:boolean;
  InvoiceStatus:boolean;

  SessionDestination:string;
  SessionDistance:Real;
  SessionTier:byte;
  SessionRace:string;

  SessionShipName:string;
  SessionShipPrice:Integer;
  SessionShipSpeed:Integer;
  SessionFuelType:String;
  SessionFuelPrice:Real;
  SessionTotalFuelPrice:Real;

  OxygenCost:Real;
  HydrogenCost:Real;
  NitrogenCost:Real;

  ProtectionPrice:Integer;
  ChamberPrice:Integer;

  SessionProtectionCost:Integer;
  SessionProtectionAmount:Integer;
  SessionChamberCost:Integer;
  SessionChamberAmount:Integer;
  SessionServiceCost:Integer;

  F:textFile;

implementation

end.

