unit login;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, dbf, db, FileUtil, Forms, Controls, Graphics, Dialogs,
  StdCtrls;

type

  { TloginForm }

  TloginForm = class(TForm)
    DataAccess: TDbf;
    email: TEdit;
    errorMsg1: TLabel;
    errorMsg2: TLabel;
    loginBtn: TButton;
    password: TEdit;
    signupBtn: TLabel;
    signupLabel: TLabel;
    slogan1: TLabel;
    slogan2: TLabel;
    title: TLabel;
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure loginBtnClick(Sender: TObject);
    procedure signupBtnClick(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  loginForm: TloginForm;

implementation

uses
  signup, dashboard, admin, globalVariables;

{$R *.lfm}

{ TloginForm }

procedure TloginForm.loginBtnClick(Sender: TObject);

begin
  //Make sure that there are values in the email and password field
  if email.text = '' then
     errorMsg1.caption:='*Required'
  else
     errorMsg1.caption:='';

  if password.text = '' then
     errorMsg2.caption:='*Required'
  else
     errorMsg1.caption:='';


  // If both fields have values...
  if (email.text <> '') and (password.text <> '') then
  begin
       // Create an instance of the database connection
       // and configure it
       DataAccess:=TDbf.Create(self);
       DataAccess.TableName:='databases\UserData.dbf';
       DataAccess.Open;
       DataAccess.First;
       { Run through the database and check for an email match
         If one is found then it will check that the password of the email
         is the same as the password given by the user
         Error will be prompted if any requirments are not met }
       while not DataAccess.EOF do
       begin
            if DataAccess.Fields[3].AsString = Email.text then
            begin
                 if DataAccess.Fields[4].AsString = Password.text then
                 begin
                      SessionName:=DataAccess.Fields[1].AsString;
                      SessionSurname:=DataAccess.Fields[2].AsString;
                      SessionEmail:=DataAccess.Fields[3].AsString;
                      SessionContact:=DataAccess.Fields[5].AsString;

                      DestinationStatus:=False;
                      ShipStatus:=False;
                      ProtectionStatus:=False;
                      InvoiceStatus:=False;

                      DataAccess.Close;

                      { Redirenct routes to admin's backoffice or
                        the user interface }
                      if sessionEmail = 'admin@gmail.com' then
                         adminForm.Show
                      else
                          dashboardForm.show;
                      Hide;
                 end
                 else
                 begin
                      DataAccess.Next;
                 end;
            end
            else
            begin
                 DataAccess.Next;
            end;
       end;
       DataAccess.Close;
       errorMsg1.caption:= '**Invalid Credentials';
       errorMsg2.caption:= '**';
  end;
end;

procedure TloginForm.FormActivate(Sender: TObject);
begin
     // Clears all fields and errors for a fresh start of the screen
     errorMsg1.caption:='';
     errorMsg2.caption:='';
     email.text:='';
     password.text:='';
end;

procedure TloginForm.FormCreate(Sender: TObject);
begin
     // Set the relative path for the database so that it may connect
     // in any directory
     Login.loginForm.DataAccess.Active:=False;
     Login.loginForm.DataAccess.FilePath:='databases\';
     Login.loginForm.DataAccess.Active:=True;
end;

procedure TloginForm.signupBtnClick(Sender: TObject);
begin
     // Simple Route redirection... You're gonne see this a lot
     signupForm.show;
     Hide;
end;

end.

