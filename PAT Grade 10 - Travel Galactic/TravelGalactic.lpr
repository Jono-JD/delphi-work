program TravelGalactic;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, dbflaz, login, signup, robots, dashboard, globalVariables, planTrip,
  destination, spacecraft, warning, tier1_1, tier1_2, tier2_1, tier2_2, tier3_1,
  tier3_2, settings, admin, protection, contact, invoice, final
  { you can add units after this };

{$R *.res}

begin
  RequireDerivedFormResource:=True;
  Application.Initialize;
  OxygenCost:=0.01;
  HydrogenCost:=0.012;
  NitrogenCost:=0.025;
  ProtectionPrice:=1000;
  ChamberPrice:=200;
  Application.CreateForm(TloginForm, loginForm);
  Application.CreateForm(TsignupForm, signupForm);
  Application.CreateForm(TdashboardForm, dashboardForm);
  Application.CreateForm(TplanTripForm, planTripForm);
  Application.CreateForm(TdestinationForm, destinationForm);
  Application.CreateForm(TspacecraftForm, spacecraftForm);
  Application.CreateForm(TwarningForm, warningForm);
  Application.CreateForm(Ttier1_1Form, tier1_1Form);
  Application.CreateForm(Ttier1_2Form, tier1_2Form);
  Application.CreateForm(Ttier2_1Form, tier2_1Form);
  Application.CreateForm(Ttier2_2Form, tier2_2Form);
  Application.CreateForm(Ttier3_1Form, tier3_1Form);
  Application.CreateForm(Ttier3_2Form, tier3_2Form);
  Application.CreateForm(TsettingsForm, settingsForm);
  Application.CreateForm(TadminForm, adminForm);
  Application.CreateForm(TprotectionForm, protectionForm);
  Application.CreateForm(TcontactForm, contactForm);
  Application.CreateForm(TinvoiceForm, invoiceForm);
  Application.CreateForm(TfinalForm, finalForm);
  clearSessionData;                    // See Robots
  resetFuelStats;                      // See Robots
  Application.Run;
end.

