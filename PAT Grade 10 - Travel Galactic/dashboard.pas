unit dashboard;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  StdCtrls, Buttons;

type

  { TdashboardForm }

  TdashboardForm = class(TForm)
    TripBtn: TButton;
    SettingsBtn: TButton;
    ContactBtn: TButton;
    LogoutBtn: TButton;
    TripImage: TImage;
    SettingsImage: TImage;
    ContactImage: TImage;
    LogoutImage: TImage;
    Message: TLabel;
    WelcomeLabel: TLabel;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    PanelWrapper1: TPanel;
    PanelWrapper2: TPanel;
    PanelWrapper3: TPanel;
    PanelWrapper4: TPanel;
    Username: TLabel;
    procedure FormClose(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure TripBtnClick(Sender: TObject);
    procedure SettingsBtnClick(Sender: TObject);
    procedure ContactBtnClick(Sender: TObject);
    procedure LogoutBtnClick(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  dashboardForm: TdashboardForm;

implementation

uses
  login, planTrip, settings, contact, robots, globalVariables;

{$R *.lfm}

{ TdashboardForm }



{ ******************* }
{
  Nothing special in this view.
  This screen serves as a main gateway to
  all the different parts of the application
}
{ ******************* }


procedure TdashboardForm.TripBtnClick(Sender: TObject);
begin
     planTripForm.show;
     Hide;
end;

procedure TdashboardForm.FormShow(Sender: TObject);
begin
     Username.caption:=SessionName + ' ' + SessionSurname;
end;

procedure TdashboardForm.FormClose(Sender: TObject);
begin
     Application.Terminate;
end;

procedure TdashboardForm.SettingsBtnClick(Sender: TObject);
begin
     settingsForm.Show;
     Hide;
end;

procedure TdashboardForm.ContactBtnClick(Sender: TObject);
begin
     contactForm.Show;
     Hide
end;

procedure TdashboardForm.LogoutBtnClick(Sender: TObject);
begin
     clearSessionData;
     resetFuelStats;
     loginForm.show;
     Hide;
end;

end.

