unit spacecraft;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  StdCtrls;

type

  { TspacecraftForm }

  TspacecraftForm = class(TForm)
    backBtn: TButton;
    divider3: TPanel;
    HelpLabel: TLabel;
    XindiBtn: TButton;
    OwnShipBtn: TButton;
    XindiWrapper: TPanel;
    HumanBtn: TButton;
    EdosianBtn: TButton;
    EdosianWrapper: TPanel;
    tier1_1: TImage;
    tier1_2: TImage;
    tier2_1: TImage;
    tier2_2: TImage;
    tier3_1: TImage;
    tier3_2: TImage;
    divider1: TPanel;
    divider2: TPanel;
    heading: TPanel;
    divider: TPanel;
    HumanWrapper: TPanel;
    TypeLabel: TLabel;
    Tier1Label: TLabel;
    Tier2Label: TLabel;
    Tier3Label: TLabel;
    Title: TLabel;
    username: TLabel;
    OwnShipWrapper: TPanel;
    procedure backBtnClick(Sender: TObject);
    procedure EdosianBtnClick(Sender: TObject);
    procedure FormClose(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure HumanBtnClick(Sender: TObject);
    procedure OwnShipBtnClick(Sender: TObject);
    procedure tier1_1Click(Sender: TObject);
    procedure tier1_2Click(Sender: TObject);
    procedure tier2_1Click(Sender: TObject);
    procedure tier2_2Click(Sender: TObject);
    procedure tier3_1Click(Sender: TObject);
    procedure tier3_2Click(Sender: TObject);
    procedure XindiBtnClick(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  spacecraftForm: TspacecraftForm;

implementation

uses
  planTrip,
  warning,
  tier1_1,
  tier1_2,
  tier2_1,
  tier2_2,
  tier3_1,
  tier3_2,
  robots,
  globalVariables;

{$R *.lfm}

{ TspacecraftForm }

procedure TspacecraftForm.backBtnClick(Sender: TObject);
begin
     clearCompare;
     planTripForm.show;
     Hide;
end;

procedure TspacecraftForm.EdosianBtnClick(Sender: TObject);
begin
     // Loads all the images of the specific spaceships according to the button
     // That was clicked
     // Simple Wrapper Highlighting is also coded for a friendly user interface

     HumanWrapper.color:=$000000;
     EdosianWrapper.color:=$FF0000;
     XindiWrapper.color:=$000000;
     SessionRace:='Edosian';
     clearCompare;
     tier1_1.Picture.LoadFromFile('images\E-T1.1.png');
     tier1_2.Picture.LoadFromFile('images\E-T1.2.png');
     tier2_1.Picture.LoadFromFile('images\E-T2.1.png');
     tier2_2.Picture.LoadFromFile('images\E-T2.2.png');
     tier3_1.Picture.LoadFromFile('images\E-T3.1.png');
     tier3_2.Picture.LoadFromFile('images\E-T3.2.png');
end;

procedure TspacecraftForm.FormClose(Sender: TObject);
begin
     Application.Terminate;
end;

procedure TspacecraftForm.FormShow(Sender: TObject);
begin
     Username.caption:=SessionName + ' ' + SessionSurname;
     HumanBtn.Click;
     { Sets the restrictions for the ships that the user will b
       allowed to use based on the planet that the user picked

       If the user picks a planet that requires a tier 1 rocket
       then the user will be able to choose from tiers 1, 2 and 3
       If the user picks a planet that requires a tier 2 rocket
       then the user will only be allowed to use tier 2 and 3 rockets
       You may always use higher tiers, but not lower ones
     }
     if SessionTier = 1 then
     begin
          tier1_1.Enabled:=True;
          tier1_2.Enabled:=True;
          tier2_1.Enabled:=True;
          tier2_2.Enabled:=True;
          tier3_1.Enabled:=True;
          tier3_2.Enabled:=True;
     end
     else if SessionTier = 2 then
     begin
          tier1_1.Enabled:=False;
          tier1_2.Enabled:=False;
          tier2_1.Enabled:=True;
          tier2_2.Enabled:=True;
          tier3_1.Enabled:=True;
          tier3_2.Enabled:=True;
     end
     else
     begin
          tier1_1.Enabled:=False;
          tier1_2.Enabled:=False;
          tier2_1.Enabled:=False;
          tier2_2.Enabled:=False;
          tier3_1.Enabled:=True;
          tier3_2.Enabled:=True;
     end;
end;

procedure TspacecraftForm.HumanBtnClick(Sender: TObject);
begin
     HumanWrapper.color:=$FF0000;
     EdosianWrapper.color:=$000000;
     XindiWrapper.color:=$000000;
     SessionRace:='Human';
     clearCompare;
     tier1_1.Picture.LoadFromFile('images\H-T1.1.png');
     tier1_2.Picture.LoadFromFile('images\H-T1.2.png');
     tier2_1.Picture.LoadFromFile('images\H-T2.1.png');
     tier2_2.Picture.LoadFromFile('images\H-T2.2.png');
     tier3_1.Picture.LoadFromFile('images\H-T3.1.png');
     tier3_2.Picture.LoadFromFile('images\H-T3.2.png');
end;

procedure TspacecraftForm.OwnShipBtnClick(Sender: TObject);
begin
     warningForm.show;
     Hide;
end;

procedure TspacecraftForm.tier1_1Click(Sender: TObject);
begin
     tier1_1Form.show;
end;

procedure TspacecraftForm.tier1_2Click(Sender: TObject);
begin
     tier1_2Form.show;
end;

procedure TspacecraftForm.tier2_1Click(Sender: TObject);
begin
     tier2_1Form.show;
end;

procedure TspacecraftForm.tier2_2Click(Sender: TObject);
begin
     tier2_2Form.show;
end;

procedure TspacecraftForm.tier3_1Click(Sender: TObject);
begin
     tier3_1Form.show;
end;

procedure TspacecraftForm.tier3_2Click(Sender: TObject);
begin
     tier3_2Form.show;
end;

procedure TspacecraftForm.XindiBtnClick(Sender: TObject);
begin
     HumanWrapper.color:=$000000;
     EdosianWrapper.color:=$000000;
     XindiWrapper.color:=$FF0000;
     SessionRace:='Xindi';
     clearCompare;
     tier1_1.Picture.LoadFromFile('images\X-T1.1.png');
     tier1_2.Picture.LoadFromFile('images\X-T1.2.png');
     tier2_1.Picture.LoadFromFile('images\X-T2.1.png');
     tier2_2.Picture.LoadFromFile('images\X-T2.2.png');
     tier3_1.Picture.LoadFromFile('images\X-T3.1.png');
     tier3_2.Picture.LoadFromFile('images\X-T3.2.png');
end;

end.

