unit invoice;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  ExtCtrls;

type

  { TinvoiceForm }

  TinvoiceForm = class(TForm)
    backBtn: TButton;
    Bevel1: TBevel;
    Bevel2: TBevel;
    Bevel3: TBevel;
    Bevel4: TBevel;
    Bevel5: TBevel;
    Bevel6: TBevel;
    Bevel7: TBevel;
    Bevel8: TBevel;
    IDestinationBtn: TButton;
    IDistanceBtn: TButton;
    IShipBtn: TButton;
    IFuelBtn: TButton;
    IProtectionBtn: TButton;
    IChamberBtn: TButton;
    IConfirmBtn: TButton;
    divider1: TBevel;
    divider2: TBevel;
    heading: TPanel;
    Label1: TLabel;
    Label10: TLabel;
    IDestination: TLabel;
    Label11: TLabel;
    IShipCost: TLabel;
    IFuelCost: TLabel;
    IProtectionCost: TLabel;
    IChamberCost: TLabel;
    Label12: TLabel;
    TotalCost: TLabel;
    IShipTier: TLabel;
    IDistance: TLabel;
    Label2: TLabel;
    IShipName: TLabel;
    IFuelType: TLabel;
    IProtectionAmount: TLabel;
    IChamberAmount: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Title: TLabel;
    username: TLabel;
    procedure backBtnClick(Sender: TObject);
    procedure FormClose(Sender: TObject);
    procedure IDestinationBtnClick(Sender: TObject);
    procedure IDistanceBtnClick(Sender: TObject);
    procedure IShipBtnClick(Sender: TObject);
    procedure IFuelBtnClick(Sender: TObject);
    procedure IProtectionBtnClick(Sender: TObject);
    procedure IChamberBtnClick(Sender: TObject);
    procedure IConfirmBtnClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  invoiceForm: TinvoiceForm;

implementation

{$R *.lfm}

{ TinvoiceForm }
uses
    planTrip, destination, spacecraft, protection, globalVariables;


{

    Displays a summary of all the details that make up the trip
    The Varibales are read from the global variables unit

}
procedure TinvoiceForm.FormShow(Sender: TObject);
begin
    Username.caption:=SessionName + ' ' + SessionSurname;
    IDestination.caption:=SessionDestination;
    IDistance.caption:=FloatToStr(SessionDistance) + 'Mm';
    IShipName.caption:='Ship: ' + SessionShipName;
    IShipTier.caption:='( Tier ' + IntToStr(SessionTier) + ' )';
    IShipCost.caption:=IntToStr(SessionShipPrice);
    IFuelType.caption:='Total Fuel Cost: ( ' + SessionFuelType + ' )';
    IFuelCost.caption:=FloatToStr(SessionTotalFuelPrice);
    IProtectionAmount.Caption:='Protection Unit ( ' + IntToStr(SessionProtectionAmount) + ' )';
    IProtectionCost.caption:=IntTOStr(SessionProtectionCost);
    IChamberAmount.caption:='Hyperbaric Chambers ( ' + IntToStr(SessionChamberAmount) + ' )';
    IChamberCost.caption:=IntToStr(SessionChamberCost);
    TotalCost.caption:=FloatToStr(SessionShipPrice+SessionTotalFuelPrice+SessionProtectionCost+SessionChamberCost);

end;








{

    Redirecting features for changing aspects of the trip that
    a user is not happy with and Other Navigation...

}
procedure TinvoiceForm.backBtnClick(Sender: TObject);
begin
     planTripForm.Show;
     Hide;
end;

procedure TinvoiceForm.FormClose(Sender: TObject);
begin
     Application.Terminate;
end;

procedure TinvoiceForm.IDestinationBtnClick(Sender: TObject);
begin
     destinationForm.Show;
     Hide;
end;

procedure TinvoiceForm.IDistanceBtnClick(Sender: TObject);
begin
    destinationForm.Show;
    Hide;
end;

procedure TinvoiceForm.IShipBtnClick(Sender: TObject);
begin
    spacecraftForm.Show;
    Hide;
end;

procedure TinvoiceForm.IFuelBtnClick(Sender: TObject);
begin
    spacecraftForm.SHow;
    Hide;
end;

procedure TinvoiceForm.IProtectionBtnClick(Sender: TObject);
begin
    ProtectionForm.SHow;
    Hide;
end;

procedure TinvoiceForm.IChamberBtnClick(Sender: TObject);
begin
    protectionForm.Show;
    HIde;
end;

procedure TinvoiceForm.IConfirmBtnClick(Sender: TObject);
begin
    InvoiceStatus:=True;
    planTripForm.SHow;
    Hide;
end;

end.

