unit final;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  StdCtrls;

type

  { TfinalForm }

  TfinalForm = class(TForm)
    backBtn: TButton;
    Message: TLabel;
    LogoutBtn: TButton;
    heading: TPanel;
    Title: TLabel;
    procedure backBtnClick(Sender: TObject);
    procedure FormClose(Sender: TObject);
    procedure LogoutBtnClick(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  finalForm: TfinalForm;

implementation

{$R *.lfm}

{ TfinalForm }
uses
    planTrip, login, robots;

procedure TfinalForm.backBtnClick(Sender: TObject);
begin
    planTripForm.show;
    hide;
end;

procedure TfinalForm.FormClose(Sender: TObject);
begin
     Application.Terminate;
end;

procedure TfinalForm.LogoutBtnClick(Sender: TObject);
begin
     clearSessionData;
     resetFuelStats;
     loginForm.show;
     Hide;
end;

end.

