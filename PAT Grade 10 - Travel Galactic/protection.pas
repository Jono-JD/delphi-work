unit protection;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  ExtCtrls;

type

  { TprotectionForm }

  TprotectionForm = class(TForm)
    backBtn: TButton;
    ConfirmBtn: TButton;
    heading: TPanel;
    PAmountLabel: TLabel;
    PAmount: TLabel;
    CAmountLabel: TLabel;
    CAmount: TLabel;
    PTitle: TLabel;
    CTotalPrice: TLabel;
    TotalPriceLabel: TLabel;
    TotalPrice: TLabel;
    CTitle: TLabel;
    PCurrentPriceLabel: TLabel;
    PCurrentPrice: TLabel;
    PTotalPriceLabel: TLabel;
    PTotalPrice: TLabel;
    CCurrentPriceLabel: TLabel;
    CCurrentPrice: TLabel;
    CTotalPriceLabel: TLabel;
    Divider: TPanel;
    ChamberBar: TScrollBar;
    ProtectionBar: TScrollBar;
    Title: TLabel;
    username: TLabel;
    procedure backBtnClick(Sender: TObject);
    procedure ChamberBarChange(Sender: TObject);
    procedure ConfirmBtnClick(Sender: TObject);
    procedure FormClose(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ProtectionBarChange(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  protectionForm: TprotectionForm;

implementation

{$R *.lfm}

{ TprotectionForm }
uses
    globalVariables, planTrip;

// Gives the form a fresh look
procedure TprotectionForm.FormCreate(Sender: TObject);
begin
     PCurrentPrice.Caption:=IntToStr(ProtectionPrice);
     CCurrentPrice.Caption:=IntToStr(ChamberPrice);
     PAmount.Caption:='0';
     CAmount.Caption:='0';
     PTotalPrice.Caption:='0';
     CTotalPrice.Caption:='0';
     TotalPrice.Caption:='0';
     ProtectionBar.Position:=0;
     ChamberBar.Position:=0;

end;

procedure TprotectionForm.FormShow(Sender: TObject);
begin
     username.caption:=SessionName + ' ' + SessionSurname;
end;


// Indicates the Number of chambers that a user will be hiring as well
// as the price. The price is set in the TravelGalactic.lpr file.
procedure TprotectionForm.ChamberBarChange(Sender: TObject);
begin
     CAmount.Caption:=IntToStr(ChamberBar.Position);
     CTotalPrice.Caption:=IntToStr(ChamberBar.Position*ChamberPrice);
     TotalPrice.caption:=IntToStr(StrToInt(PTotalPrice.Caption)+StrToInt(CTotalPrice.Caption));
end;


// Saves all data in variables that are initialized in the globalVariables unit
procedure TprotectionForm.ConfirmBtnClick(Sender: TObject);
begin
     SessionProtectionCost:=StrToInt(PTotalPrice.Caption);
     SessionProtectionAmount:=StrToInt(PAmount.Caption);
     SessionChamberCost:=StrTOInt(CTotalPrice.Caption);
     SessionChamberAmount:=StrToInt(CAmount.Caption);
     SessionServiceCost:=StrToInt(TotalPrice.Caption);
     protectionStatus:=True;
     planTripForm.show;
     Hide;
end;

procedure TprotectionForm.FormClose(Sender: TObject);
begin
    Application.Terminate;
end;

procedure TprotectionForm.backBtnClick(Sender: TObject);
begin
     planTripForm.show;
     Hide;
end;


// Indicates the Number of ships that a user will be hiring for protection as well
// as the price. The price is set in the TravelGalactic.lpr file.
procedure TprotectionForm.ProtectionBarChange(Sender: TObject);
begin
     PAmount.Caption:=IntToStr(ProtectionBar.Position);
     PTotalPrice.Caption:=IntToStr(ProtectionBar.Position*ProtectionPrice);
     TotalPrice.caption:=IntToStr(StrToInt(PTotalPrice.Caption)+StrToInt(CTotalPrice.Caption));
end;

end.

