unit contact;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, dbf, FileUtil, Forms, Controls, Graphics, Dialogs,
  ExtCtrls, StdCtrls;

type

  { TcontactForm }

  TcontactForm = class(TForm)
    backBtn: TButton;
    DataAccess: TDbf;
    DisplayAmount: TLabel;
    ErrorMsg1: TLabel;
    ErrorMsg2: TLabel;
    sendBtn: TButton;
    TopicSelect: TComboBox;
    heading: TPanel;
    MessageBox: TMemo;
    Title: TLabel;
    username: TLabel;
    procedure backBtnClick(Sender: TObject);
    procedure FormClose(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure MessageBoxChange(Sender: TObject);
    procedure sendBtnClick(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  contactForm: TcontactForm;

implementation

{$R *.lfm}

{ TcontactForm }
uses
    dashboard, globalVariables;

{
    It is important to know that this form will connect to one of three
    Databases for each instance of the view
    1. Compliments.dbf
    2. Comments.dbf
    3. Complaints.dbf
    All three databses are in the ...\databses\ directory
}

// Basic Redirection
procedure TcontactForm.backBtnClick(Sender: TObject);
begin
     dashboardForm.show;
     hide;
end;

// Termination of the program on closing of the form
procedure TcontactForm.FormClose(Sender: TObject);
begin
     Application.Terminate;
end;


// Sets the relative path for the DataAccess point of the databases
// This ensures that the application connects to the databases irrespective
// of the directory that the application is in
procedure TcontactForm.FormCreate(Sender: TObject);
begin
     Contact.contactForm.DataAccess.Active:=False;
     Contact.contactForm.DataAccess.FilePath:='databases\';
     Contact.contactForm.DataAccess.Active:=True;
end;


// Giving the form a fresh look
procedure TcontactForm.FormShow(Sender: TObject);
begin
     Username.caption:=SessionName + ' ' + SessionSurname;
     ErrorMsg1.Visible:=False;
     ErrorMsg2.Visible:=False;
     DisplayAmount.caption:=IntToStr(Length(MessageBox.Text)) + ' / 255';
     TopicSelect.ItemIndex:=-1;
     TopicSelect.Text:='Select Topic';
end;


// Character count for the limitation of the database
// The Database will not accept more than 255 charaters

// The limit can be found in the 'Compliments', 'comments' and 'complaints'
// field defs of the respective databases
procedure TcontactForm.MessageBoxChange(Sender: TObject);
begin
     DisplayAmount.caption:=IntToStr(Length(MessageBox.Text)) + ' / 255';
end;


// Does some varification
{ One of the three options in the dropdown needs to be selected:
      1. Compliment
      2. Comment
      3. Complaint
  If this is not done then an error will be provided

  There also needs to be a value in the messageBox for the data to
  be posted to the database
}
procedure TcontactForm.sendBtnClick(Sender: TObject);
var
   postError:boolean;

begin
     PostError:=False;
     if MessageBox.Text = '' then
     begin
          postError:=True;
          ErrorMsg2.Visible:=True;
     end
     else
     begin
         ErrorMsg2.Visible:=False;
     end;

     if topicSelect.ItemIndex = -1 then
     begin
          postError:=True;
          ErrorMsg1.Visible:=True;
     end
     else
     begin
          ErrorMsg1.Visible:=False;
     end;

     if not postError then
     begin
          DataAccess:=TDbf.Create(self);
          if topicSelect.ItemIndex = 0 then
          begin
             //showMessage('Compliment');
             DataAccess.TableName:='databases\Compliments.dbf';
          end
          else if topicSelect.ItemIndex = 1 then
          begin
             //showMessage('Comment');
             DataAccess.TableName:='databases\Comments.dbf';
          end
          else
          begin
             //showMessage('Complaint');
             DataAccess.TableName:='databases\Complaints.dbf';
          end;
          DataAccess.Open;
          DataAccess.Append;
          DataAccess.Fields[1].AsString:=SessionEmail;
          DataAccess.Fields[2].AsString:=MessageBox.Text;
          DataAccess.Post;
          DataAccess.Close;
          ShowMessage('Message Delivered!');
          MessageBox.Clear;
          dashboardForm.Show;
          Hide;
     end;
end;

end.

