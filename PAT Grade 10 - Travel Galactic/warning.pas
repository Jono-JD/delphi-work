unit warning;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls;

type

  { TwarningForm }

  TwarningForm = class(TForm)
    CancelBtn: TButton;
    ConfirmBtn: TButton;
    Message: TLabel;
    Title: TLabel;
    procedure CancelBtnClick(Sender: TObject);
    procedure ConfirmBtnClick(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  warningForm: TwarningForm;

implementation

uses
  spacecraft, planTrip, robots, globalVariables;

{$R *.lfm}

{ TwarningForm }

{
  ************************

  Nothing Special going on here
  Just Variable assignment and
  redirection

  ************************
}


procedure TwarningForm.CancelBtnClick(Sender: TObject);
begin
     spacecraftForm.show;
     Hide;
end;

procedure TwarningForm.ConfirmBtnClick(Sender: TObject);
begin
     SessionShipName:='Own Ship';
     SessionShipPrice:=0;
     SessionShipSpeed:=0;
     SessionTier:=0;
     SessionFuelType:='Own Fuel';
     SessionFuelPrice:=0.00;
     SessionTotalFuelPrice:=0.00;
     clearCompare;
     ShipStatus:=True;
     spacecraftForm.Hide;
     planTripForm.Show;
     Hide;
end;

end.

