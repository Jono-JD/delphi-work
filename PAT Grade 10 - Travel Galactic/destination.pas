unit destination;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  StdCtrls, Buttons;

type

  { TdestinationForm }

  TdestinationForm = class(TForm)
    backBtn: TButton;
    confirmBtn: TButton;
    tier: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    distance: TLabel;
    Label5: TLabel;
    Panel1: TPanel;
    destinationName: TLabel;
    planetDisplay: TImage;
    jupiter: TImage;
    mars: TImage;
    mercury: TImage;
    moon: TImage;
    neptune: TImage;
    pluto: TImage;
    saturn: TImage;
    System: TImage;
    heading: TPanel;
    Title: TLabel;
    uranus: TImage;
    username: TLabel;
    venus: TImage;
    procedure backBtnClick(Sender: TObject);
    procedure confirmBtnClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject);
    procedure jupiterClick(Sender: TObject);
    procedure marsClick(Sender: TObject);
    procedure mercuryClick(Sender: TObject);
    procedure moonClick(Sender: TObject);
    procedure neptuneClick(Sender: TObject);
    procedure plutoClick(Sender: TObject);
    procedure saturnClick(Sender: TObject);
    procedure uranusClick(Sender: TObject);
    procedure venusClick(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  destinationForm: TdestinationForm;

implementation

uses
  planTrip, robots, globalVariables;

{$R *.lfm}

{ TdestinationForm }

procedure TdestinationForm.backBtnClick(Sender: TObject);
begin
     planTripForm.show;
     Hide;
end;

// Saves data that has been obtained to variables that have been
// initialised in the globalVariables unit
procedure TdestinationForm.confirmBtnClick(Sender: TObject);
begin
     SessionDestination:= destinationName.caption;
     SessionDistance:=StrToFloat(distance.caption);
     if tier.caption = 'Tier 1' then
        SessionTier:=1
     else if tier.caption = 'Tier 2' then
        SessionTier:=2
     else
         SessionTier:=3;
     DestinationStatus:=True;
     resetFuelStats;
     planTripForm.Show;
     Hide;
end;

procedure TdestinationForm.FormActivate(Sender: TObject);
begin
     username.caption:= SessionName + ' ' + SessionSurname;
end;

procedure TdestinationForm.FormClose(Sender: TObject);
begin
     Application.Terminate;
end;



// From here on down:
// Clicking on a planet will display a image of the planet,
// The distance to the planet and the tier required
procedure TdestinationForm.jupiterClick(Sender: TObject);
begin
     planetDisplay.Picture.LoadFromFile('images\planet5.jpg');
     destinationName.caption:='Jupiter Inc.';
     distance.caption:='628730';
     tier.caption:='Tier 2';
     confirmBtn.Enabled:=True;
end;


procedure TdestinationForm.marsClick(Sender: TObject);
begin
     planetDisplay.Picture.LoadFromFile('images\planet4.jpg');
     destinationName.caption:='Mars';
     distance.caption:='78340';
     tier.caption:='Tier 1';
     confirmBtn.Enabled:=True;
end;

procedure TdestinationForm.mercuryClick(Sender: TObject);
begin
     planetDisplay.Picture.LoadFromFile('images\planet1.jpg');
     destinationName.caption:='Mercury';
     distance.caption:='91691';
     tier.caption:='Tier 1';
     confirmBtn.Enabled:=True;
end;

procedure TdestinationForm.moonClick(Sender: TObject);
begin
     planetDisplay.Picture.LoadFromFile('images\planet3.jpg');
     destinationName.caption:='Moon Station';
     distance.caption:='3844';
     tier.caption:='Tier 1';
     confirmBtn.Enabled:=True;
end;

procedure TdestinationForm.neptuneClick(Sender: TObject);
begin
     planetDisplay.Picture.LoadFromFile('images\planet8.jpg');
     destinationName.caption:='Neptune';
     distance.caption:='4351400';
     tier.caption:='Tier 3';
     confirmBtn.Enabled:=True;
end;

procedure TdestinationForm.plutoClick(Sender: TObject);
begin
     planetDisplay.Picture.LoadFromFile('images\planet9.jpg');
     destinationName.caption:='Pluto Labs';
     distance.caption:='7500000';
     tier.caption:='Tier 3';
     confirmBtn.Enabled:=True;
end;

procedure TdestinationForm.saturnClick(Sender: TObject);
begin
    planetDisplay.Picture.LoadFromFile('images\planet6.jpg');
    destinationName.caption:='Saturn Station';
    distance.caption:='1275000';
    tier.caption:='Tier 2';
    confirmBtn.Enabled:=True;
end;

procedure TdestinationForm.uranusClick(Sender: TObject);
begin
    planetDisplay.Picture.LoadFromFile('images\planet7.jpg');
    destinationName.caption:='Uranus';
    distance.caption:='2723950';
    tier.caption:='Tier 3';
    confirmBtn.Enabled:=True;
end;

procedure TdestinationForm.venusClick(Sender: TObject);
begin
    planetDisplay.Picture.LoadFromFile('images\planet2.jpg');
    destinationName.caption:='Venus';
    distance.caption:='41400';
    tier.caption:='Tier 2';
    confirmBtn.Enabled:=True;
end;

end.

