unit signup;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, dbf, db, FileUtil, Forms, Controls, Graphics, Dialogs,
  StdCtrls, ExtCtrls, DBGrids, DbCtrls;

type

  { TsignupForm }

  TsignupForm = class(TForm)
    DataSource1: TDataSource;
    DataAccess: TDbf;
    description: TLabel;
    errorMsg1: TLabel;
    errorMsg2: TLabel;
    errorMsg3: TLabel;
    errorMsg4: TLabel;
    errorMsg5: TLabel;
    errorMsg6: TLabel;
    signupBtn: TButton;
    loginLabel: TLabel;
    loginBtn: TLabel;
    firstname: TEdit;
    surname: TEdit;
    passwordConfirm: TEdit;
    email: TEdit;
    contact: TEdit;
    password: TEdit;
    Image1: TImage;
    procedure clearSignupFields();
    procedure clearSignupErrors();
    procedure FormClose(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure loginBtnClick(Sender: TObject);
    procedure signupBtnClick(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  signupForm: TsignupForm;

implementation

uses
  login, globalVariables;

{$R *.lfm}

{ TsignupForm }

// Clears the Values in Fields on the Signup Form
procedure TsignupForm.clearSignupFields;
begin
   firstname.Text:='';
   surname.Text:='';
   email.Text:='';
   contact.text:='';
   password.text:='';
   passwordConfirm.text:='';
end;

// Clears the errors in the signUp Form
procedure TsignupForm.clearSignupErrors;
begin
   errorMsg1.caption:='';
   errorMsg2.caption:='';
   errorMsg3.caption:='';
   errorMsg4.caption:='';
   errorMsg5.caption:='';
   errorMsg6.caption:='';
end;

procedure TsignupForm.FormClose(Sender: TObject);
begin
     Application.Terminate;
end;

procedure TsignupForm.FormCreate(Sender: TObject);
begin
     // Set the relative path for the database so that it may connect
     // in any directory
     Signup.signupForm.DataAccess.Active:=False;
     Signup.signupForm.DataAccess.FilePath:='databases\';
     Signup.signupForm.DataAccess.Active:=True;
end;

// Takes a user to the Login Form
procedure TsignupForm.loginBtnClick(Sender: TObject);
begin
   loginForm.show;
   clearSignupFields();
   clearSignupErrors();
   Hide;
end;

procedure TsignupForm.signupBtnClick(Sender: TObject);
var
  varified:boolean;
  emailCharacter:boolean;
  i:integer;

begin
   varified:=True;
   emailcharacter:=False;



   // Validation for Name Field
   if firstname.text = '' then
   begin
        errorMsg1.caption := '*Required';
        varified:=False;
   end
   else
   begin
        errorMsg1.caption := '';
   end;



   // Validation for Surname Field
   if surname.text = '' then
   begin
        errorMsg2.caption := '*Required';
        varified:=False;
   end
   else
   begin
        errorMsg2.caption := '';
   end;



   // Validation for Email Field
   { First we check if the email field has a value
     Second we check that the email contains the appropriate '@' symbol
     Third we check that the eamil is unique by running it through the database
     If it is then it clears the varification
     If any requirment is not met then it fails the validation test and error will follow }
   if email.text = '' then
   begin
        errorMsg3.caption := '*Required';
        varified:=False;
   end
   else
   begin
        for i:=1 to length(email.text) do
        begin
             if email.text[i] = '@' then
             begin
                emailCharacter:=True;
                errorMsg3.caption := '';
             end;
        end;

        if emailCharacter then
        begin
            DataAccess:=TDbf.Create(self);
            DataAccess.TableName:='databases\UserData.dbf';
            DataAccess.Open;
            DataAccess.First;
            while not DataAccess.EOF do
            begin
                 if DataAccess.Fields[3].AsString = email.text then
                 begin
                      errorMsg3.caption := '*User already exists, please login';
                      varified:=False;
                      DataAccess.Close;
                 end
                 else
                 begin
                      errorMsg3.caption := '';
                      DataAccess.Next;
                 end;
            end;
            DataAccess.Close;
        end
        else
        begin
             errorMsg3.caption := '*Incorrect Email Format';
             varified:=False;
        end;
   end;


   // Validation for Contact Field
   // Has to be 10 or more characters long
   if contact.text = '' then
   begin
        errorMsg4.caption := '*Required';
        varified:=False;
   end
   else
   begin
        errorMsg4.caption := '';
        if Length(contact.text) < 10 then
        begin
            errorMsg4.caption := '*Invalid Contact Details';
            varified:=False;
        end
        else
        begin
             errorMsg4.caption := '';
        end;
   end;


   // Validation for Password Field
   if (password.text = '') and (passwordConfirm.text = '') then
   begin
        errorMsg5.caption := '*Required';
        errorMsg6.caption := '*Required';
        varified:=False;
   end
   else
   begin
        if password.text = passwordConfirm.text then
        begin
             errorMsg5.caption := '';
             errorMsg6.caption := '';
        end
        else
        begin
             errorMsg5.caption := '**Passwords do not match';
             errorMsg6.caption := '**';
             varified:=False;
        end;
   end;


   // All requirements have been met
   // Posting to the database will comence
   if varified then
   begin
        DataAccess:=TDbf.Create(self);
        DataAccess.TableName := 'databases\UserData.dbf';
        DataAccess.Open;
        DataAccess.Append;
        DataAccess.Fields[1].AsString:=firstname.text;
        DataAccess.Fields[2].AsString:=surname.text;
        DataAccess.Fields[3].AsString:=email.text;
        DataAccess.Fields[4].AsString:=password.text;
        DataAccess.Fields[5].AsString:=contact.text;
        DataAccess.Post;
        DataAccess.Close;

        clearSignupFields();
        loginForm.show;
        Hide;
   end;
end;

end.

