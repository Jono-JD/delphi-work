unit tier3_1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  StdCtrls;

type

  { Ttier3_1Form }

  Ttier3_1Form = class(TForm)
    confirmBtn: TButton;
    costLabel: TLabel;
    description: TLabel;
    descriptionForm: TScrollBox;
    descriptionLabel: TLabel;
    divider: TPanel;
    fuelPrice: TLabel;
    fuelPriceLabel: TLabel;
    fuelType: TRadioGroup;
    fuelTypeLabel: TLabel;
    Hydrogen: TRadioButton;
    Nitrogen: TRadioButton;
    Oxygen: TRadioButton;
    shipPrice: TLabel;
    shipPriceLabel: TLabel;
    speed: TLabel;
    speedLabel: TLabel;
    title: TLabel;
    totalFuelPrice: TLabel;
    totalFuelPriceLabel: TLabel;
    visual: TImage;
    procedure confirmBtnClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure HydrogenClick(Sender: TObject);
    procedure NitrogenClick(Sender: TObject);
    procedure OxygenClick(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  tier3_1Form: Ttier3_1Form;

implementation

{$R *.lfm}

{ Ttier3_1Form }
uses
    globalvariables, robots, spacecraft, planTrip;

var
  InitialSpeed:Integer;
  SelectedFuel:string;

procedure Ttier3_1Form.FormShow(Sender: TObject);
var
  F:textFile;
  Line:string;
begin
     if sessionRace = 'Human' then
     begin
          visual.Picture.LoadFromFile('images\H-T3.1.png');
          AssignFile(F, 'databases\HumanShipDetails.txt');
     end
     else if sessionRace = 'Edosian' then
     begin
          visual.Picture.LoadFromFile('images\E-T3.1.png');
          AssignFile(F, 'databases\EdosianShipDetails.txt');
     end
     else
     begin
          visual.Picture.LoadFromFile('images\X-T3.1.png');
          AssignFile(F, 'databases\XindiShipDetails.txt');
     end;
     Reset(F);
     repeat
           readln(F, Line);
     until 'Tier3_1' = Line;
     readln(F, Line);
     tier3_1Form.Caption:='Details for '+Line;
     title.Caption:=Line;
     readln(F, Line);
     speed.Caption:=Line;
     InitialSpeed:=StrToInt(Line);
     readln(F, Line);
     shipPrice.Caption:=Line;
     description.caption:='';
     repeat
           readln(F, Line);
           description.caption:=description.caption+Line+LineEnding;
     until Line = '';
     CloseFile(F);
end;

procedure Ttier3_1Form.confirmBtnClick(Sender: TObject);
begin
     SessionShipName:=title.caption;
     SessionShipPrice:=StrToInt(shipPrice.caption);
     SessionShipSpeed:=StrToInt(speed.caption);
     if SelectedFuel = 'Oxygen' then
        SessionFuelType:='Liquid Oxygen'
     else if SelectedFuel = 'Hydrogen' then
        SessionFuelType:='Liquid Hydrogen'
     else
         SessionFuelType:='Nitrogen Tetroxide';
     SessionFuelPrice:=StrToFloat(fuelPrice.caption);
     SessionTotalFuelPrice:=StrToFloat(totalFuelPrice.caption);
     clearCompare;
     ShipStatus:=True;
     spacecraftForm.Hide;
     planTripForm.Show;
end;

procedure Ttier3_1Form.HydrogenClick(Sender: TObject);
begin
     speed.caption := FloatToStr(InitialSpeed + (InitialSpeed div 100*25));
     FuelPrice.caption := FloatToStr(HydrogenCost);
     TotalFuelPrice.caption := FloatToStr(HydrogenCost*SessionDistance);
     SelectedFuel:='Hydrogen';
end;

procedure Ttier3_1Form.NitrogenClick(Sender: TObject);
begin
     speed.caption := FloatToStr(InitialSpeed+InitialSpeed div 100*50);
     FuelPrice.caption := FloatToStr(NitrogenCost);
     TotalFuelPrice.caption := FloatToStr(NitrogenCost*SessionDistance);
     SelectedFuel:='Nitrogen';
end;

procedure Ttier3_1Form.OxygenClick(Sender: TObject);
begin
     speed.caption := FloatToStr(InitialSpeed);
     FuelPrice.caption := FloatToStr(OxygenCost);
     TotalFuelPrice.caption := FloatToStr(OxygenCost*SessionDistance);
     SelectedFuel:='Oxygen';
end;

end.

