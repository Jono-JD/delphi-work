unit settings;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, dbf, FileUtil, Forms, Controls, Graphics, Dialogs,
  StdCtrls, ExtCtrls;

type

  { TsettingsForm }

  TsettingsForm = class(TForm)
    backBtn: TButton;
    DataAccess: TDbf;
    errorMsg2: TLabel;
    errorMsg3: TLabel;
    errorMsg4: TLabel;
    errorMsg5: TLabel;
    errorMsg6: TLabel;
    errorMsg7: TLabel;
    heading: TPanel;
    errorMsg1: TLabel;
    saveLabel: TLabel;
    passwordConfirm: TEdit;
    newPassword: TEdit;
    surname: TEdit;
    firstname: TEdit;
    email: TEdit;
    contact: TEdit;
    saveChangesBtn: TButton;
    password: TEdit;
    Title: TLabel;
    username: TLabel;
    procedure backBtnClick(Sender: TObject);
    procedure FormClose(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure saveChangesBtnClick(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  settingsForm: TsettingsForm;

implementation

{$R *.lfm}

{ TsettingsForm }
uses
    dashboard, globalVariables;

procedure TsettingsForm.backBtnClick(Sender: TObject);
begin
     dashboardForm.Show;
     Hide;
end;

procedure TsettingsForm.FormClose(Sender: TObject);
begin
     Application.Terminate;
end;

procedure TsettingsForm.FormCreate(Sender: TObject);
begin
     // Set the relative path for the database so that it may connect
     // in any directory
     Settings.settingsForm.DataAccess.Active:=False;
     Settings.settingsForm.DataAccess.FilePath:='databases\';
     Settings.settingsForm.DataAccess.Active:=True;
end;

procedure TsettingsForm.FormShow(Sender: TObject);
begin
     // Compares the SessionEmail to all the emails in the database
     // Once found it will fetch all the data and insert into the
     // Appropriate edit boxes and make it ready for editing
     Username.caption:=SessionName + ' ' + SessionSurname;
     DataAccess:=TDbf.create(self);
     DataAccess.TableName:='databases\UserData.dbf';
     DataAccess.Open;
     DataAccess.First;
     while not DataAccess.EOF do
     begin
          if DataAccess.Fields[3].AsString = SessionEmail then
          begin
               firstname.text:=DataAccess.Fields[1].AsString;
               surname.text:=DataAccess.Fields[2].AsString;
               email.text:=DataAccess.Fields[3].AsString;
               contact.text:=DataAccess.Fields[5].AsString;
               DataAccess.Close;
          end
          else
          begin
               DataAccess.Next;
          end;
     end;
     DataAccess.Close;
     password.text:='';
     newPassword.text:='';
     passwordConfirm.text:='';
     errorMsg1.caption:='';
     errorMsg2.caption:='';
     errorMsg3.caption:='';
     errorMsg4.caption:='';
     errorMsg5.caption:='';
     errorMsg6.caption:='';
     errorMsg7.caption:='';
end;

procedure TsettingsForm.saveChangesBtnClick(Sender: TObject);
var
   varified:boolean;
   emailAvailable:boolean;
   passwordEmpty:boolean;
   emailCharacter:boolean;
   i:integer;

begin
     { Mostly the same varification is used as on the signup screen however
       major tweaks and features have been added to the editing of a user's
       password... Please have a look and enjoy }
     saveLabel.Visible:=False;
     varified:=True;
     emailAvailable:=True;
     passwordEmpty:=True;
     emailCharacter:=False;

     // Varify Name Field
     if firstname.text = '' then
     begin
          errorMsg1.caption:='*Required';
          varified:=False;
     end
     else
          errorMsg1.caption:='';

     // Varify Surname Field
     if surname.text = '' then
     begin
         errorMsg2.caption:='*Required';
         varified:=False;
     end
     else
         errorMsg2.caption:='';

     // Varify Email Field
     // Exactly the same as signup email varification
     if email.text = '' then
     begin
          errorMsg3.caption:='*Required';
          varified:=False;
     end
     else
     begin
          for i:=1 to length(email.text) do
          begin
               if email.text[i] = '@' then
               begin
                    emailCharacter:=True;
                    errorMsg3.caption := '';
               end;
          end;

          if emailCharacter then
          begin
               if Email.text <> SessionEmail then
               begin
                    DataAccess.Open;
                    DataAccess.First;
                    while (emailAvailable) and (not DataAccess.EOF) do
                    begin
                         if DataAccess.Fields[3].AsString = email.text then
                         begin
                              errorMsg3.caption:='*Email is already in use';
                              varified:=False;
                              emailAvailable:=False;
                         end
                         else
                              errorMsg3.caption:='';
                         DataAccess.Next;
                    end;
                    DataAccess.Close;
               end;
          end
          else
          begin
             errorMsg3.caption := '*Incorrect Email Format';
             varified:=False;
          end;
     end;

     // Varify Contact Detail Field
     // Has to be more than 10 characters long
     if contact.text = '' then
     begin
         errorMsg4.caption:='*Required';
         varified:=False;
     end
     else
     begin
         errorMsg4.caption:='';
         if Length(contact.text) < 10 then
         begin
              errorMsg4.caption := '*Invalid Contact Details';
              varified:=False;
         end
         else
         begin
              errorMsg4.caption := '';
         end;
     end;

     // Varify Password Fields
     { Checks if all password related fields are empty or nor
       If it is empty then it will skip the varification
       If not ALL 3 are empty then it will make sure that all of them have values
       If not then they will be prompted with errors
       The password in the database linked to the users email in the database will be compared to the old password
       If it is equal then it will continue to varify the new password else it will fail varification
       The new password will  have to be confirmed and if it passes confirmation then it will pass the validation
       Else errors will be prompted}
     if (password.text <> '') and (newPassword.text <> '') and (passwordConfirm.text <> '') then
     begin
          errorMsg5.caption:='';
          errorMsg6.caption:='';
          errorMsg7.caption:='';
          DataAccess.Open;
          DataAccess.First;
          while not (DataAccess.Fields[3].AsString = SessionEmail) do
          begin
               DataAccess.Next;
          end;
          showMessage(DataAccess.Fields[3].AsString);
          if DataAccess.Fields[4].AsString = password.text then
          begin
               if newPassword.text <> passwordConfirm.text then
               begin
                    errorMsg6.Caption:='**Password do not match';
                    errorMsg7.Caption:='**';
                    varified:=False;
               end;
          end
          else
          begin
               errorMsg5.Caption:='*Incorrect Password';
               varified:=False;
          end;
          PasswordEmpty:=False;
     end
     else
     begin
          if (password.text = '') and (newPassword.text = '') and (passwordConfirm.text = '') then
          begin
               passwordEmpty:=True;
               //ShowMessage('Fields are empty');
          end
          else
          begin
               passwordEmpty:=False;
               //ShowMessage('Fields are not Empty');
          end;

          if passwordEmpty then
          begin
               errorMsg5.caption:='';
               errorMsg6.caption:='';
               errorMsg7.caption:='';
          end
          else
          begin
             if password.Text = '' then
              begin
                  errorMsg5.caption:= '*Required';
                  varified:=False;
              end
              else
              begin
                  errorMsg5.caption:='';
              end;
              if newPassword.Text = '' then
              begin
                  errorMsg6.caption:= '*Required';
                  varified:=False;
              end
              else
              begin
                  errorMsg6.caption:='';
              end;
              if passwordConfirm.Text = '' then
              begin
                  errorMsg7.caption:= '*Required';
                  varified:=False;
              end
              else
              begin
                  errorMsg7.caption:= '';
              end;
          end;
     end;

     if varified then
     begin
         DataAccess.Open;
         DataAccess.First;
         while not DataAccess.EOF do
         begin
              if DataAccess.Fields[3].AsString = SessionEmail then
              begin
                  DataAccess.Edit;
                  DataAccess.Fields[1].AsString := firstname.text;
                  DataAccess.Fields[2].AsString := surname.text;
                  DataAccess.Fields[3].AsString := email.text;
                  if not passwordEmpty then
                  begin
                      DataAccess.Fields[4].AsString := newPassword.Text;
                      //ShowMessage('Saving new password');
                  end;
                  DataAccess.Fields[5].AsString := contact.text;
                  DataAccess.Post;
              end;
              DataAccess.Next;
         end;
         DataAccess.Close;
         saveLabel.Visible:=True;
         SessionName:=firstName.text;
         SessionSurname:=surname.text;
         SessionEmail:=email.text;
         SessionContact:=contact.text;

     end;
end;

end.

