unit admin;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, dbf, db, FileUtil, Forms, Controls, Graphics, Dialogs,
  DBGrids, DbCtrls, ExtCtrls, StdCtrls;

type

  { TadminForm }

  TadminForm = class(TForm)
    AccountsBtn: TButton;
    ComplimentsBtn: TButton;
    CommentsBtn: TButton;
    ComplaintsBtn: TButton;
    ComplimentsWrapper: TPanel;
    CommentsWrapper: TPanel;
    ComplaintsWrapper: TPanel;
    backBtn: TButton;
    DataSource: TDataSource;
    DataAccess: TDbf;
    DBGrid: TDBGrid;
    DBNavigator1: TDBNavigator;
    heading: TPanel;
    AccountsWrapper: TPanel;
    Title: TLabel;
    procedure AccountsBtnClick(Sender: TObject);
    procedure backBtnClick(Sender: TObject);
    procedure CommentsBtnClick(Sender: TObject);
    procedure ComplaintsBtnClick(Sender: TObject);
    procedure ComplimentsBtnClick(Sender: TObject);
    procedure FormClose(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  adminForm: TadminForm;

implementation

uses
    login;
{$R *.lfm}

{ TadminForm }

procedure TadminForm.backBtnClick(Sender: TObject);
begin
     LoginForm.Show;
     Hide;
end;

procedure TadminForm.CommentsBtnClick(Sender: TObject);
begin
     // Simple Highlighting for the buttons
     // Connects to the appropriate table
     AccountsWrapper.Color:=$000000;
     ComplimentsWrapper.Color:=$000000;
     CommentsWrapper.Color:=$FF0000;
     ComplaintsWrapper.Color:=$000000;
     DbGrid.AutoFillColumns:=False;
     DataAccess.Active:=False;
     DataAccess.TableName:='databases\Comments.dbf';
     DataAccess.Active:=True;
end;

procedure TadminForm.ComplaintsBtnClick(Sender: TObject);
begin
     // Simple Highlighting for the buttons
     // Connects to the appropriate table
     AccountsWrapper.Color:=$000000;
     ComplimentsWrapper.Color:=$000000;
     CommentsWrapper.Color:=$000000;
     ComplaintsWrapper.Color:=$FF0000;
     DbGrid.AutoFillColumns:=False;
     DataAccess.Active:=False;
     DataAccess.TableName:='databases\Complaints.dbf';
     DataAccess.Active:=True;
end;

procedure TadminForm.ComplimentsBtnClick(Sender: TObject);
begin
     // Simple Highlighting for the buttons
     // Connects to the appropriate table
     AccountsWrapper.Color:=$000000;
     ComplimentsWrapper.Color:=$FF0000;
     CommentsWrapper.Color:=$000000;
     ComplaintsWrapper.Color:=$000000;
     DbGrid.AutoFillColumns:=False;
     DataAccess.Active:=False;
     DataAccess.TableName:='databases\Compliments.dbf';
     DataAccess.Active:=True;
end;

procedure TadminForm.FormClose(Sender: TObject);
begin
    Application.Terminate;
end;

procedure TadminForm.FormCreate(Sender: TObject);
begin
     // Set the relative path for the database so that it may connect
     // in any directory
     Admin.adminForm.DataAccess.Active:=False;
     Admin.adminForm.DataAccess.FilePath:='databases\';
     Admin.adminForm.DataAccess.Active:=True;
end;

procedure TadminForm.FormShow(Sender: TObject);
begin
     AccountsBtn.Click;
end;

procedure TadminForm.AccountsBtnClick(Sender: TObject);
begin
     AccountsWrapper.Color:=$FF0000;
     ComplimentsWrapper.Color:=$000000;
     CommentsWrapper.Color:=$000000;
     ComplaintsWrapper.Color:=$000000;
     DbGrid.AutoFillColumns:=True;
     DataAccess.Active:=False;
     DataAccess.TableName:='databases\UserData.dbf';
     DataAccess.Active:=True;
end;

end.

