Greetings to the user of this fine application
The UserData.dbf database has already been populated with 2 Accounts


For Logging In!
===============================
The one account is an admin account. 
This is the only admin account and no other admin accounts can 
be created at this stage of the applications development.
The other account is a user account.

1.) The Admin
Email = admin@gmail.com
Password = admin

2.) The User
Email = user@gmail.com
Password = user

More user accounts my be created by clicking on the signup button at the bottom
of the login form and completing it to the best of your ability

Please enjoy the application!
Developer
Jonathan de Kock