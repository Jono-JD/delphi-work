unit tier1_1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  StdCtrls;

type

  { Ttier1_1Form }

  Ttier1_1Form = class(TForm)
    confirmBtn: TButton;
    description: TLabel;
    descriptionForm: TScrollBox;
    descriptionLabel: TLabel;
    fuelPriceLabel: TLabel;
    costLabel: TLabel;
    fuelPrice: TLabel;
    Oxygen: TRadioButton;
    Hydrogen: TRadioButton;
    Nitrogen: TRadioButton;
    shipPrice: TLabel;
    speed: TLabel;
    totalFuelPriceLabel: TLabel;
    speedLabel: TLabel;
    shipPriceLabel: TLabel;
    title: TLabel;
    divider: TPanel;
    fuelTypeLabel: TLabel;
    fuelType: TRadioGroup;
    totalFuelPrice: TLabel;
    visual: TImage;
    procedure confirmBtnClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure HydrogenClick(Sender: TObject);
    procedure NitrogenClick(Sender: TObject);
    procedure OxygenClick(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  tier1_1Form: Ttier1_1Form;

implementation

{$R *.lfm}

{ Ttier1_1Form }
uses
    globalvariables, robots, spacecraft, planTrip;

var
  InitialSpeed:Integer;
  SelectedFuel:string;

procedure Ttier1_1Form.FormShow(Sender: TObject);
var
  F:textFile;
  Line:string;
begin
     {
       The Comments in this section is used for all tier related forms
       Tier 1_1 to Tier 3_2
     }
     // Each form checks what species has been selected and then loads
     // the ships information from the appropriate text files located
     // within the ...\databases\ directory
     if sessionRace = 'Human' then
     begin
          visual.Picture.LoadFromFile('images\H-T1.1.png');
          AssignFile(F, 'databases\HumanShipDetails.txt');
     end
     else if sessionRace = 'Edosian' then
     begin
          visual.Picture.LoadFromFile('images\E-T1.1.png');
          AssignFile(F, 'databases\EdosianShipDetails.txt');
     end
     else
     begin
          visual.Picture.LoadFromFile('images\X-T1.1.png');
          AssignFile(F, 'databases\XindiShipDetails.txt');
     end;
     Reset(F);
     repeat
           readln(F, Line);
     until 'Tier1_1' = Line;
     readln(F, Line);
     tier1_1Form.Caption:='Details for '+Line;
     title.Caption:=Line;
     readln(F, Line);
     speed.Caption:=Line;
     InitialSpeed:=StrToInt(Line);
     readln(F, Line);
     shipPrice.Caption:=Line;
     description.caption:='';
     repeat
           readln(F, Line);
           description.caption:=description.caption+Line+LineEnding;
     until Line = '';
     CloseFile(F);
end;

{
    After a ship has been chosen and the confim button is clicked;
    values will be set to the appropriate variables and the other forms
    wil be closed
}
procedure Ttier1_1Form.confirmBtnClick(Sender: TObject);
begin
     SessionShipName:=title.caption;
     SessionShipPrice:=StrToInt(shipPrice.caption);
     SessionShipSpeed:=StrToInt(speed.caption);
     if SelectedFuel = 'Oxygen' then
        SessionFuelType:='Liquid Oxygen'
     else if SelectedFuel = 'Hydrogen' then
        SessionFuelType:='Liquid Hydrogen'
     else
         SessionFuelType:='Nitrogen Tetroxide';
     SessionFuelPrice:=StrToFloat(fuelPrice.caption);
     SessionTotalFuelPrice:=StrToFloat(totalFuelPrice.caption);
     clearCompare;
     ShipStatus:=True;
     spacecraftForm.Hide;
     planTripForm.Show;
end;

{
    The Radio buttons will change the average speed of the speed
    of the ship and the price of the fuel depending on which type
    of the radio button is linked to
}
procedure Ttier1_1Form.HydrogenClick(Sender: TObject);
begin
     speed.caption := FloatToStr(InitialSpeed + (InitialSpeed div 100*25));
     FuelPrice.caption := FloatToStr(HydrogenCost);
     TotalFuelPrice.caption := FloatToStr(HydrogenCost*SessionDistance);
     SelectedFuel:='Hydrogen';
end;

procedure Ttier1_1Form.NitrogenClick(Sender: TObject);
begin
     speed.caption := FloatToStr(InitialSpeed+InitialSpeed div 100*50);
     FuelPrice.caption := FloatToStr(NitrogenCost);
     TotalFuelPrice.caption := FloatToStr(NitrogenCost*SessionDistance);
     SelectedFuel:='Nitrogen';
end;

procedure Ttier1_1Form.OxygenClick(Sender: TObject);
begin
     speed.caption := FLoatToStr(InitialSpeed);
     FuelPrice.caption := FloatToStr(OxygenCost);
     TotalFuelPrice.caption := FloatToStr(OxygenCost*SessionDistance);
     SelectedFuel:='Oxygen';
end;

end.

