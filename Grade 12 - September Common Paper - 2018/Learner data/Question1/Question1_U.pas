unit Question1_u;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, ComCtrls;

type
  TfrmQuestion1 = class(TForm)
    pnlQ1_1: TPanel;
    pnlQ1_2: TPanel;
    pnlQ1_3: TPanel;
    pnlQ1_4: TPanel;
    pnlQ1_5: TPanel;
    ledAmountD: TLabeledEdit;
    btnQ1_1: TButton;
    Label1: TLabel;
    edtRand: TEdit;
    Label2: TLabel;
    edtBricks: TEdit;
    redOutput: TRichEdit;
    Label3: TLabel;
    btnQ1_2: TButton;
    btnQ1_3: TButton;
    btnQ1_5: TButton;
    btnQ1_4: TButton;
    memOutput: TMemo;
    cbbBricks: TComboBox;
    chbTransport: TCheckBox;
    rgpPayment: TRadioGroup;
    Label5: TLabel;
    Label6: TLabel;
    ledTransaction: TLabeledEdit;
    pnlPrize: TPanel;
    redTotalCost: TRichEdit;
    procedure btnQ1_1Click(Sender: TObject);
    procedure btnQ1_2Click(Sender: TObject);
    procedure btnQ1_3Click(Sender: TObject);
    procedure btnQ1_4Click(Sender: TObject);
    procedure btnQ1_5Click(Sender: TObject);

  private
    iBricks,iMonth:integer;    // Given global variables

  public
    { Public declarations }
  end;

// Given constants

const rDollarToRand = 13.45;
      arrFuel:array[0..11] of real =( 11.95, 12.2, 12.15, 11.97, 12.37, 12.13, 11.49, 11.73, 12.29, 12.64, 12.79, 12.74);
      arrPrize:array[0..3] of string=('Grocery voucher','Jacket','Water bottle','Dinner voucher');
      rLoadCost=300;
var
  frmQuestion1: TfrmQuestion1;

implementation

{$R *.dfm}


procedure TfrmQuestion1.btnQ1_1Click(Sender: TObject);
begin
  //Question 1.1
end;

procedure TfrmQuestion1.btnQ1_2Click(Sender: TObject);
begin
   //Question 1.2
end;

procedure TfrmQuestion1.btnQ1_3Click(Sender: TObject);
begin
  //Question 1.3
end;

procedure TfrmQuestion1.btnQ1_4Click(Sender: TObject);
begin
  //Question 1.4
end;

procedure TfrmQuestion1.btnQ1_5Click(Sender: TObject);
begin
  //Question 1.5
end;

end.
