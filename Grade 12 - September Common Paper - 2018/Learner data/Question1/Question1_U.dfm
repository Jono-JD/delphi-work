object frmQuestion1: TfrmQuestion1
  Left = 0
  Top = 0
  Caption = 'Question 1'
  ClientHeight = 514
  ClientWidth = 711
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object pnlQ1_1: TPanel
    Left = 8
    Top = 8
    Width = 345
    Height = 121
    Alignment = taLeftJustify
    Caption = ' Question1.1'
    TabOrder = 0
    VerticalAlignment = taAlignTop
    object Label1: TLabel
      Left = 8
      Top = 67
      Width = 73
      Height = 13
      Caption = 'Output in Rand'
    end
    object ledAmountD: TLabeledEdit
      Left = 8
      Top = 40
      Width = 121
      Height = 21
      EditLabel.Width = 78
      EditLabel.Height = 13
      EditLabel.Caption = 'Amount in Dollar'
      TabOrder = 0
    end
    object btnQ1_1: TButton
      Left = 192
      Top = 38
      Width = 121
      Height = 25
      Caption = '1.1 Convert to Rand'
      TabOrder = 1
      OnClick = btnQ1_1Click
    end
    object edtRand: TEdit
      Left = 10
      Top = 86
      Width = 121
      Height = 21
      TabOrder = 2
    end
  end
  object pnlQ1_2: TPanel
    Left = 8
    Top = 135
    Width = 345
    Height = 170
    Alignment = taLeftJustify
    Caption = ' Question 1.2'
    TabOrder = 1
    VerticalAlignment = taAlignTop
    object Label2: TLabel
      Left = 8
      Top = 32
      Width = 123
      Height = 13
      Caption = 'Number of bricks needed:'
    end
    object Label3: TLabel
      Left = 128
      Top = 51
      Width = 121
      Height = 13
      Caption = 'Monthly balance of bricks'
    end
    object edtBricks: TEdit
      Left = 192
      Top = 24
      Width = 121
      Height = 21
      TabOrder = 0
      Text = '10000'
    end
    object redOutput: TRichEdit
      Left = 128
      Top = 70
      Width = 185
      Height = 83
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      ScrollBars = ssVertical
      TabOrder = 1
    end
    object btnQ1_2: TButton
      Left = 8
      Top = 120
      Width = 97
      Height = 25
      Caption = '1.2 Display bricks'
      TabOrder = 2
      OnClick = btnQ1_2Click
    end
  end
  object pnlQ1_3: TPanel
    Left = 8
    Top = 311
    Width = 345
    Height = 195
    Alignment = taLeftJustify
    Caption = ' Question 1.3'
    TabOrder = 2
    VerticalAlignment = taAlignTop
    object btnQ1_3: TButton
      Left = 8
      Top = 30
      Width = 169
      Height = 25
      Caption = '1.3 Calculate highest fuel price'
      TabOrder = 0
      OnClick = btnQ1_3Click
    end
    object memOutput: TMemo
      Left = 10
      Top = 72
      Width = 321
      Height = 105
      TabOrder = 1
    end
  end
  object pnlQ1_4: TPanel
    Left = 359
    Top = 8
    Width = 345
    Height = 297
    Alignment = taLeftJustify
    Caption = ' Question 1.4'
    TabOrder = 3
    VerticalAlignment = taAlignTop
    object Label5: TLabel
      Left = 184
      Top = 199
      Width = 77
      Height = 13
      Caption = 'Total bricks cost'
    end
    object Label6: TLabel
      Left = 16
      Top = 24
      Width = 62
      Height = 13
      Caption = 'Type of brick'
    end
    object btnQ1_4: TButton
      Left = 17
      Top = 187
      Width = 121
      Height = 25
      Caption = '1.4 Calculate payment'
      TabOrder = 0
      OnClick = btnQ1_4Click
    end
    object cbbBricks: TComboBox
      Left = 16
      Top = 59
      Width = 145
      Height = 21
      TabOrder = 1
      Items.Strings = (
        'FBS: Face brick standard'
        'FBX: Face brick extra'
        'NFX: Non-facing extra'
        'NFP: Non-facing plastered')
    end
    object chbTransport: TCheckBox
      Left = 16
      Top = 104
      Width = 97
      Height = 17
      Caption = 'Transport'
      TabOrder = 2
    end
    object rgpPayment: TRadioGroup
      Left = 160
      Top = 103
      Width = 161
      Height = 67
      Caption = 'Payment options'
      Items.Strings = (
        'Cash'
        'Card')
      TabOrder = 3
    end
    object redTotalCost: TRichEdit
      Left = 160
      Top = 218
      Width = 169
      Height = 60
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
    end
  end
  object pnlQ1_5: TPanel
    Left = 359
    Top = 312
    Width = 346
    Height = 193
    Alignment = taLeftJustify
    Caption = ' Question 1.5'
    TabOrder = 4
    VerticalAlignment = taAlignTop
    object btnQ1_5: TButton
      Left = 16
      Top = 88
      Width = 113
      Height = 25
      Caption = '1.5 Determine prize'
      TabOrder = 0
      OnClick = btnQ1_5Click
    end
    object ledTransaction: TLabeledEdit
      Left = 16
      Top = 48
      Width = 121
      Height = 21
      EditLabel.Width = 96
      EditLabel.Height = 13
      EditLabel.Caption = 'Transaction Number'
      TabOrder = 1
    end
    object pnlPrize: TPanel
      Left = 16
      Top = 128
      Width = 297
      Height = 49
      TabOrder = 2
    end
  end
end
