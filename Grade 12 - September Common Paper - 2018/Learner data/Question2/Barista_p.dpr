program Barista_p;

uses
  Forms,
  BaristaGUI_u in 'BaristaGUI_u.pas' {frmMain};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
