﻿unit BaristaGUI_u;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, jpeg, ExtCtrls, Grids, DBGrids, ComCtrls, pngimage, TDBConnection_U, Math, DB, ADODB;

type
  TfrmMain = class(TForm)
    imgBackground: TImage;
    pgcQuestions: TPageControl;
    tbsQ2_2: TTabSheet;
    tbsQ2_1: TTabSheet;
    imgLogo: TImage;
    imgQ2_1_Background: TImage;
    imgQ2_2_Background: TImage;
    dbgCustomers: TDBGrid;
    dbgSales: TDBGrid;
    btnQ_2_2_1: TButton;
    dbgSQL: TDBGrid;
    btnQ2_1_2: TButton;
    btnQ_2_1_3: TButton;
    Q_2_1_4: TButton;
    Q_2_1_5: TButton;
    Q_2_1_1: TButton;
    Q_2_2_2: TButton;
    Q_2_2_3: TButton;
    btnDBRestore: TButton;
    procedure btnQ_2_2_1Click(Sender: TObject);
    procedure btnQ2_1_2Click(Sender: TObject);
    procedure btnQ_2_1_3Click(Sender: TObject);
    procedure Q_2_1_4Click(Sender: TObject);
    procedure Q_2_1_5Click(Sender: TObject);
    procedure Q_2_1_1Click(Sender: TObject);
    procedure Q_2_2_2Click(Sender: TObject);
    procedure Q_2_2_3Click(Sender: TObject);
    procedure btnDBRestoreClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    var
      conBarista: TADOConnection;

      tblCustomers: TADOTable;
      tblSales: TADOTable;

      qry: TADOQuery;

      dbsCustomers: TDataSource;
      dbsSales: TDataSource;
      dbsBarista: TDataSource;

      sSQL: string;

    procedure DBSetup();
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;

implementation

{$R *.dfm}

procedure TfrmMain.Q_2_1_1Click(Sender: TObject);
var
  input: String;
begin
  //  QUESTION 2.1.1
  input := InputBox('Data Required', 'Please enter the Customer ID', 'C005');
  sSQL := '';

  qry.SQL.Clear;
  qry.SQL.Add(sSQL);
  qry.Open;
end;

procedure TfrmMain.btnQ2_1_2Click(Sender: TObject);
begin
  //  QUESTION 2.1.2
  sSQL := '';

  qry.SQL.Clear;
  qry.SQL.Add(sSQL);
  qry.Open;
end;

procedure TfrmMain.btnQ_2_1_3Click(Sender: TObject);
begin
  //  QUESTION 2.1.3
  sSQL := '';

  qry.SQL.Clear;
  qry.SQL.Add(sSQL);
  qry.Open;
end;

procedure TfrmMain.Q_2_1_4Click(Sender: TObject);
begin
  //  QUESTION 2.1.4
  sSQL := '';

  qry.SQL.Clear;
  qry.SQL.Add(sSQL);
  qry.Open;
end;

procedure TfrmMain.Q_2_1_5Click(Sender: TObject);
begin
  //  QUESTION 2.1.5
  sSQL := '';

  qry.SQL.Clear;
  qry.SQL.Add(sSQL);
  qry.Open;
end;

procedure TfrmMain.btnQ_2_2_1Click(Sender: TObject);
begin
  //  QUESTION 2.2.1

end;

procedure TfrmMain.Q_2_2_2Click(Sender: TObject);
begin
  //  QUESTION 2.2.2

end;

procedure TfrmMain.Q_2_2_3Click(Sender: TObject);
begin
  //  QUESTION 2.2.3

end;

{$REGION}

procedure TfrmMain.FormCreate(Sender: TObject);
begin
  //  PROVIDED CODE - DO NOT MODIFY!
  DBSetup();
end;

procedure TfrmMain.DBSetup;
begin
  //  PROVIDED CODE - DO NOT MODIFY!
  conBarista := TADOConnection.Create(Self);
  TDBConnection.DBConnect(conBarista, Self);

  tblCustomers := TADOTable.Create(Self);
  tblCustomers.Connection := conBarista;
  tblCustomers.TableName := 'Customers';
  tblCustomers.Open();


  dbsCustomers := TDataSource.Create(Self);
  dbsCustomers.DataSet := tblCustomers;

  dbgCustomers.DataSource := dbsCustomers;

  tblSales := TADOTable.Create(Self);
  tblSales.Connection := conBarista;
  tblSales.TableName := 'Sales';
  tblSales.Open();

  dbsSales := TDataSource.Create(Self);
  dbsSales.DataSet := tblSales;

  dbgSales.DataSource := dbsSales;

  qry := TADOQuery.Create(Self);
  qry.Connection := conBarista;

  dbsBarista := TDataSource.Create(Self);
  dbsBarista.DataSet := qry;

  dbgSQL.DataSource := dbsBarista;
end;

procedure TfrmMain.btnDBRestoreClick(Sender: TObject);
var
  failFlag: Boolean;
begin
  // PROVIDED CODE - DO NOT MODIFY!
  conBarista.Close();

  tblCustomers.Close();
  tblSales.Close();

  DeleteFile('Barista.mdb');
  CopyFile('BaristaBackup.mdb', 'Barista.mdb', failFlag);

  TDBConnection.DBConnect(conBarista, Self);
  tblCustomers.Open();
  tblSales.Open();

  ShowMessage('Database restored!');
end;

{$ENDREGION}

end.
