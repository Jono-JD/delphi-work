object frmQ3: TfrmQ3
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  ClientHeight = 431
  ClientWidth = 554
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 20
    Top = 20
    Width = 321
    Height = 95
    Color = clActiveCaption
    ParentBackground = False
    TabOrder = 0
    object Label1: TLabel
      Left = 24
      Top = 40
      Width = 85
      Height = 16
      Caption = 'Select player'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object cmbPlayers: TComboBox
      Left = 160
      Top = 38
      Width = 145
      Height = 21
      TabOrder = 0
      Items.Strings = (
        'PODILE,T'
        'HORTON,F'
        'MARX,A'
        'WIESEMAN,S'
        'MSIMEKI,N'
        'SMIT,DC'
        'GOUWS,ML'
        'LETSOALO,S'
        'TSHWALE,S'
        'CROWTHER,HS'
        'VAN LOGGERENBERG,R'
        'BARKHUIZEN,JH'
        'MOHALE,KL'
        'BECKER,HM'
        'SMOOK,F'
        'JACKSON,C'
        'VAN ZYL,DR'
        'NGWENYA,J'
        'RAMSAN,L'
        'ISMAIL,Z'
        'ERASMUS,K'
        'RAPELWANA,P'
        'SETHABA,M'
        'VAN WYK,W')
    end
  end
  object btnQ3_2_1: TButton
    Left = 358
    Top = 20
    Width = 150
    Height = 50
    Caption = 'QUESTION 3.2.1'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    OnClick = btnQ3_2_1Click
  end
  object btnQ3_2_2: TButton
    Left = 358
    Top = 129
    Width = 150
    Height = 50
    Caption = 'QUESTION 3.2.2'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    OnClick = btnQ3_2_2Click
  end
  object pnlOut: TPanel
    Left = 20
    Top = 130
    Width = 321
    Height = 263
    Color = clActiveCaption
    ParentBackground = False
    TabOrder = 3
    object Label2: TLabel
      Left = 24
      Top = 88
      Width = 89
      Height = 16
      Caption = 'Player Details'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object TLabel
      Left = 24
      Top = 16
      Width = 129
      Height = 14
      Caption = 'Player Age Category:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblAgeCategory: TLabel
      Left = 24
      Top = 48
      Width = 74
      Height = 13
      Caption = 'lblAgeCategory'
    end
    object redOut: TRichEdit
      Left = 24
      Top = 126
      Width = 233
      Height = 121
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Lines.Strings = (
        'redOut')
      ParentFont = False
      TabOrder = 0
    end
  end
  object btnReset: TBitBtn
    Left = 358
    Top = 343
    Width = 150
    Height = 50
    Caption = '&RESET'
    DoubleBuffered = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Kind = bkRetry
    ParentDoubleBuffered = False
    ParentFont = False
    TabOrder = 4
    OnClick = btnResetClick
  end
  object btnQ3_2_3: TButton
    Left = 358
    Top = 202
    Width = 150
    Height = 50
    Caption = 'QUESTION 3.2.3'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 5
    OnClick = btnQ3_2_3Click
  end
end
