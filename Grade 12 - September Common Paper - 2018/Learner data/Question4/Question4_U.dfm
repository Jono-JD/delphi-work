object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Question 4'
  ClientHeight = 395
  ClientWidth = 597
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label4: TLabel
    Left = 375
    Top = 261
    Width = 41
    Height = 18
    Caption = 'Month'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label1: TLabel
    Left = 367
    Top = 22
    Width = 89
    Height = 19
    Caption = 'DeliveryData'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object memPrices: TMemo
    Left = 24
    Top = 47
    Width = 193
    Height = 205
    TabOrder = 0
  end
  object BtnCalculateDeliveryPrices: TButton
    Left = 24
    Top = 8
    Width = 193
    Height = 33
    Caption = '4.1) Calculate Delivery Prices'
    TabOrder = 1
    OnClick = BtnCalculateDeliveryPricesClick
  end
  object btnHighestSales: TButton
    Left = 327
    Top = 285
    Width = 193
    Height = 25
    Caption = '4.3) Find month with highest sales'
    TabOrder = 2
    OnClick = btnHighestSalesClick
  end
  object edtMonth: TEdit
    Left = 422
    Top = 262
    Width = 57
    Height = 21
    TabOrder = 3
    Text = 'MAR'
  end
  object memOutput: TMemo
    Left = 24
    Top = 316
    Width = 193
    Height = 75
    TabOrder = 4
  end
  object btnUniqueDeliveries: TButton
    Left = 24
    Top = 285
    Width = 193
    Height = 25
    Caption = '4.2) Unique Deliveries '
    TabOrder = 5
    OnClick = btnUniqueDeliveriesClick
  end
  object edt_output: TEdit
    Left = 327
    Top = 316
    Width = 193
    Height = 21
    TabOrder = 6
  end
  object redDisplay: TRichEdit
    Left = 248
    Top = 47
    Width = 337
    Height = 205
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    TabOrder = 7
  end
end
