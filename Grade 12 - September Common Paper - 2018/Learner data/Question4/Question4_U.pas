unit Question4_U;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Grids, ComCtrls, ExtCtrls,StrUtils,Math;

type
  TForm1 = class(TForm)
    memPrices: TMemo;
    BtnCalculateDeliveryPrices: TButton;
    btnHighestSales: TButton;
    edtMonth: TEdit;
    Label4: TLabel;
    memOutput: TMemo;
    btnUniqueDeliveries: TButton;
    edt_output: TEdit;
    redDisplay: TRichEdit;
    Label1: TLabel;
    procedure BtnCalculateDeliveryPricesClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnHighestSalesClick(Sender: TObject);
    procedure btnUniqueDeliveriesClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
   arrDestinations:Array[1..10] of String=(
    'CTN',
    'JHB',
    'JHB',
    'CTN',
    'BLM',
    'CTN',
    'CTN',
    'WND',
    'CTN',
    'PTA');

     arrWeights:Array[1..10] of Double=(
    55.6,
    80,
    260.21,
    83.2,
    310.5,
    88.2,
    160,
    260.3,
    70.8,
    150.2);

     arrDistances:Array[1..6] of String=(
    'JHB:568','PTA:625','CTN:1634','BLM:634','DBN:30','WND:2606');


      arrSalesData: array [1 .. 13,1 .. 7] of String =(
    ('','2012','2013','2014','2015','2016','2017'),
    ('JAN','13','17','12','22','22','46'),
    ('FEB','14','17','3','10','35','44'),
    ('MAR','13','14','7','2','30','29'),
    ('APR','13','13','10','4','30','42'),
    ('MAY','10','12','17','2','30','65'),
    ('JUN','12','17','9','2','33','47'),
    ('JUL','19','28','12','1','30','40'),
    ('AUG','12','25','12','11','30','48'),
    ('SEP','15','26','17','8','39','49'),
    ('OCT','14','10','16','9','35','43'),
    ('NOV','15','12','12','17','33','42'),
    ('DEC','17','10','19','14','32','44'));

    //unique trip km

implementation

{$R *.dfm}

//////////////////////////////Question 4.1/////////////////////////////////////////////////
procedure TForm1.BtnCalculateDeliveryPricesClick(Sender: TObject);
Const
  Price_perkm_perton=23;
begin
  memPrices.Clear;
  CurrencyString:='R';
  ThousandSeparator:=' ';

  //Place your code here

end;

//////////////////////////Question 4.2//////////////////////////////////////////////////////
procedure TForm1.btnUniqueDeliveriesClick(Sender: TObject);
begin

end;


///////////////////////////////Question 4.3/////////////////////////////////////////////////
procedure TForm1.btnHighestSalesClick(Sender: TObject);
begin

end;


////////////////////////////////////////////////////////////////////////////////////////////
procedure TForm1.FormCreate(Sender: TObject);
var
  i,j:integer;
  msg:String;
begin
    msg:='';
   for i := 1 to Length(arrSalesData) do
    begin
        msg:=msg+arrSalesData[i][1];
        for j := 2 to (Length(arrSalesData[1])) do
        begin
             msg:=msg+#9+arrSalesData[i][j];
        end;
        redDisplay.Lines.add(msg);
        msg:='';
     end;
end;

end.
