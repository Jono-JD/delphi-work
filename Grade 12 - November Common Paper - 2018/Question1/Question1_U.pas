
// Enter your examination number here

unit Question1_U;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ExtCtrls, StdCtrls, Math, Buttons;

type
  TfrmQ1 = class(TForm)
    pgcQ1: TPageControl;
    tbsQuestion1_1: TTabSheet;
    tbsQuestion1_2: TTabSheet;
    tbsQuestion1_3: TTabSheet;
    pnlQ1_1: TPanel;
    btnQ1_3: TButton;
    tbsQuestion1_4: TTabSheet;
    btnQ1_4: TButton;
    redQ1_3: TRichEdit;
    pnlBtns: TPanel;
    bmbClose: TBitBtn;
    redQ1_4: TRichEdit;
    Label3: TLabel;
    Label4: TLabel;
    edtHeight: TEdit;
    edtRadius: TEdit;
    btnQ1_2: TButton;
    Label5: TLabel;
    Label6: TLabel;
    procedure pnlQ1_1Click(Sender: TObject);
    procedure btnQ1_2Click(Sender: TObject);
    procedure btnQ1_3Click(Sender: TObject);
    procedure btnQ1_4Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmQ1: TfrmQ1;

implementation

{$R *.dfm}
// ======================================================================
// Question 1.1
// ======================================================================
procedure TfrmQ1.pnlQ1_1Click(Sender: TObject);
begin
 // Question 1.1
 with pnlQ1_1 do
 begin
    Color:=clLime;
    Font.Color:=clRed;
    Font.Size:=20;
    Caption:='Information Technology Paper 1';
 end;


end;

// ======================================================================
// Question 1.2
// ======================================================================
procedure TfrmQ1.btnQ1_2Click(Sender: TObject);
var
    canHeight:real;
    canRadius:real;
    canVolume:real;
begin
  // Question 1.2
  canHeight:=StrToFloat(edtHeight.Text)-1;
  canRadius:=StrToFloat(edtRadius.Text);
  canVolume:= pi * power(canRadius, 2) * canHeight;
  showMessage(FormatFloat('0.0', canVolume));
end;

// ======================================================================
// Question 1.3
// ======================================================================
procedure TfrmQ1.btnQ1_3Click(Sender: TObject);
var
    RandomNum:integer;
    i: Integer;
    Factors:Integer;
begin
    // Question 1.3
    Factors:=0;
    redQ1_3.Clear;
    RandomNum:=RandomRange(5, 50);
    for i := 1 to RandomNum do
    begin
        if RandomNum mod i = 0 then
        begin
          redQ1_3.Lines.Add(IntToStr(i));
          inc(Factors);
        end;
    end;
    if Factors <= 2 then
    begin
        redQ1_3.Lines.Add('');
        redQ1_3.Lines.Add(IntToStr(RandomNum) + ' Is a Prime Number');
    end;


end;

// ======================================================================
// Question 1.4
// ======================================================================
procedure TfrmQ1.btnQ1_4Click(Sender: TObject);
var
  sCommandLine: String;
  iStepsForward:integer;
  i: Integer;

begin
  // Provided code
  sCommandLine := upperCase(InputBox('Robot instructions',
                  'Enter a line of instructions', 'SSSRSLSLLSSR'));
  redQ1_4.Lines.Clear;
//===============================================
// Enter your code here
    redQ1_4.Lines.Add(sCommandLine);
    iStepsForward:=0;
    for i := 1 to length(sCommandline) do
    begin
        if sCommandline[i] = 'S' then
        begin
            inc(iStepsForward);
            if iStepsForward > 10 then
            begin
                redQ1_4.Lines.Add('Number of forward steps exceeds 10');
            end
            else
            begin
                redQ1_4.Lines.Add('One Step Forward') ;
            end;
        end
        else if sCommandLine[i] = 'R' then
        begin
            redQ1_4.Lines.Add('Turn Right');
        end
        else
        begin
            redQ1_4.Lines.Add('Turn Left');
        end;
    end;
end;

// ------------------------------------------------------------
{$REGION 'Provided code - Do not modify'}

procedure TfrmQ1.FormCreate(Sender: TObject);
begin
  pgcQ1.ActivePageIndex := 0;
  CurrencyString := 'R';
end;
{$ENDREGION}

end.
