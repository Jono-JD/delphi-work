object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 415
  ClientWidth = 544
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object redOut: TRichEdit
    Left = 136
    Top = 8
    Width = 400
    Height = 399
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
  end
  object btnDisplayField: TButton
    Left = 8
    Top = 8
    Width = 97
    Height = 25
    Caption = 'btnDisplayField'
    TabOrder = 1
    OnClick = btnDisplayFieldClick
  end
  object btnCalculate: TButton
    Left = 8
    Top = 70
    Width = 97
    Height = 25
    Caption = 'btnCalculate'
    TabOrder = 2
  end
  object btnTotalAverage: TButton
    Left = 8
    Top = 101
    Width = 97
    Height = 25
    Caption = 'btnTotalAverage'
    TabOrder = 3
  end
  object btnDisplayGroupK: TButton
    Left = 8
    Top = 39
    Width = 97
    Height = 25
    Caption = 'btnDisplayGroupK'
    TabOrder = 4
    OnClick = btnDisplayGroupKClick
  end
  object btnFuelCost: TButton
    Left = 8
    Top = 132
    Width = 97
    Height = 25
    Caption = 'btnFuelCost'
    TabOrder = 5
    OnClick = btnFuelCostClick
  end
  object btnAvgTankSize: TButton
    Left = 8
    Top = 163
    Width = 97
    Height = 25
    Caption = 'btnAvgTankSize'
    TabOrder = 6
    OnClick = btnAvgTankSizeClick
  end
  object btnRestore: TButton
    Left = 8
    Top = 382
    Width = 97
    Height = 25
    Caption = 'btnRestore'
    TabOrder = 7
    OnClick = btnRestoreClick
  end
  object btnDeleteEntry: TButton
    Left = 8
    Top = 194
    Width = 97
    Height = 25
    Caption = 'btnDeleteEntry'
    TabOrder = 8
    OnClick = btnDeleteEntryClick
  end
  object btnAddConstData: TButton
    Left = 8
    Top = 225
    Width = 97
    Height = 25
    Caption = 'btnAddConstData'
    TabOrder = 9
    OnClick = btnAddConstDataClick
  end
  object btnEditRecord: TButton
    Left = 8
    Top = 256
    Width = 97
    Height = 25
    Caption = 'btnEditRecord'
    TabOrder = 10
    OnClick = btnEditRecordClick
  end
  object btnLocate: TButton
    Left = 8
    Top = 287
    Width = 97
    Height = 25
    Caption = 'btnLocate'
    TabOrder = 11
    OnClick = btnLocateClick
  end
end
