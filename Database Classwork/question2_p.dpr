program question2_p;

uses
  Forms,
  question2_u in 'question2_u.pas' {Form1},
  dmQuestion2 in 'dmQuestion2.pas' {dmData: TDataModule};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TdmData, dmData);
  Application.Run;
end.
