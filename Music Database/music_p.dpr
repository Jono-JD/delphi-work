program music_p;

uses
  Forms,
  music_u in 'music_u.pas' {Form1},
  dmData_u in 'dmData_u.pas' {dmData: TDataModule};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TdmData, dmData);
  Application.Run;
end.
