program database_p;

uses
  Forms,
  database_u in 'database_u.pas' {Form1},
  dataModule_u in 'dataModule_u.pas' {dmData: TDataModule};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TdmData, dmData);
  Application.Run;
end.
