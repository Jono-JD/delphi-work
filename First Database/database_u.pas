unit database_u;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, dataModule_u, ExtCtrls, DBCtrls, Grids, DBGrids, DBCGrids, StdCtrls;

type
  TForm1 = class(TForm)
    DBGrid1: TDBGrid;
    DBNavigator1: TDBNavigator;
    btnAsc: TButton;
    btnDesc: TButton;
    btnSortTwo: TButton;
    procedure btnAscClick(Sender: TObject);
    procedure btnDescClick(Sender: TObject);
    procedure btnSortTwoClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.btnAscClick(Sender: TObject);
begin
    dmData.tblArtComp.Sort :='[Surname]';
end;

procedure TForm1.btnDescClick(Sender: TObject);
begin
    dmData.tblArtComp.Sort := '[Surname] DESC';
end;

procedure TForm1.btnSortTwoClick(Sender: TObject);
begin
    dmData.tblArtComp.Sort := '[Category], [Surname]';
end;

end.
