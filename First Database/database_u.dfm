object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 355
  ClientWidth = 602
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid1: TDBGrid
    Left = 0
    Top = 0
    Width = 601
    Height = 249
    DataSource = dmData.DataSource
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object DBNavigator1: TDBNavigator
    Left = 168
    Top = 255
    Width = 240
    Height = 25
    DataSource = dmData.DataSource
    TabOrder = 1
  end
  object btnAsc: TButton
    Left = 32
    Top = 304
    Width = 113
    Height = 25
    Caption = 'Sort Ascending'
    TabOrder = 2
    OnClick = btnAscClick
  end
  object btnDesc: TButton
    Left = 168
    Top = 304
    Width = 129
    Height = 25
    Caption = 'Sort Descending'
    TabOrder = 3
    OnClick = btnDescClick
  end
  object btnSortTwo: TButton
    Left = 312
    Top = 304
    Width = 241
    Height = 25
    Caption = 'Sort Category and Surname within Category'
    TabOrder = 4
    OnClick = btnSortTwoClick
  end
end
