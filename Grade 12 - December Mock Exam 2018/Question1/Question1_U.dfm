object frmQuestion1: TfrmQuestion1
  Left = 0
  Top = 0
  Caption = 'Question 1'
  ClientHeight = 465
  ClientWidth = 582
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 16
  object Label7: TLabel
    Left = 8
    Top = 21
    Width = 201
    Height = 33
    AutoSize = False
    Caption = 'LAN FANatics'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -27
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object GroupBox1: TGroupBox
    Left = 8
    Top = 64
    Width = 259
    Height = 177
    Caption = 'Question 1.1'
    TabOrder = 0
    object Label1: TLabel
      Left = 16
      Top = 40
      Width = 80
      Height = 16
      Caption = 'Player Name'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 16
      Top = 71
      Width = 38
      Height = 16
      Caption = 'E-mail'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object edtName: TEdit
      Left = 102
      Top = 38
      Width = 147
      Height = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
    object edtEmail: TEdit
      Left = 102
      Top = 68
      Width = 147
      Height = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
    end
    object btnRegister: TButton
      Left = 27
      Top = 111
      Width = 201
      Height = 33
      Caption = '1.1  Register Player'
      TabOrder = 2
      OnClick = btnRegisterClick
    end
  end
  object GroupBox2: TGroupBox
    Left = 8
    Top = 247
    Width = 257
    Height = 210
    Caption = 'Question 1.2'
    TabOrder = 1
    object Label3: TLabel
      Left = 16
      Top = 30
      Width = 37
      Height = 16
      Caption = 'Meals'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 16
      Top = 152
      Width = 87
      Height = 16
      Caption = 'Cost of Meals'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label8: TLabel
      Left = 16
      Top = 60
      Width = 107
      Height = 16
      Caption = 'Number of Meals'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object edtCost: TEdit
      Left = 128
      Top = 148
      Width = 113
      Height = 24
      TabOrder = 0
    end
    object sedNumMeals: TSpinEdit
      Left = 197
      Top = 54
      Width = 44
      Height = 26
      MaxValue = 0
      MinValue = 0
      TabOrder = 1
      Value = 0
    end
    object chbVegetarian: TCheckBox
      Left = 27
      Top = 76
      Width = 105
      Height = 25
      Caption = 'Vegetarian'
      TabOrder = 2
    end
    object btnCalcCost: TButton
      Left = 27
      Top = 107
      Width = 201
      Height = 33
      Caption = '1.2  Calculate Cost of Meals'
      TabOrder = 3
      OnClick = btnCalcCostClick
    end
    object cbMealOptions: TComboBox
      Left = 72
      Top = 24
      Width = 169
      Height = 24
      TabOrder = 4
      Text = 'Meal Options'
      TextHint = 'Meal Options'
      Items.Strings = (
        'Toasted Sandwich'
        'Burger and Chips'
        'Medium Size Pizza')
    end
  end
  object GroupBox3: TGroupBox
    Left = 273
    Top = 8
    Width = 304
    Height = 233
    Caption = 'Question 1.3'
    TabOrder = 2
    object Label5: TLabel
      Left = 76
      Top = 37
      Width = 118
      Height = 16
      Caption = 'Number of Players'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label6: TLabel
      Left = 23
      Top = 170
      Width = 111
      Height = 16
      Caption = 'Number of Tables'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label9: TLabel
      Left = 23
      Top = 197
      Width = 139
      Height = 16
      Caption = 'Power (kW) Required'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object btnCalcTablesPower: TButton
      Left = 40
      Top = 110
      Width = 209
      Height = 33
      Caption = '1.3  Calculate Tables and Power'
      TabOrder = 0
      OnClick = btnCalcTablesPowerClick
    end
    object edtNumPlayers: TEdit
      Left = 96
      Top = 67
      Width = 73
      Height = 24
      Alignment = taCenter
      TabOrder = 1
    end
    object edtNumTables: TEdit
      Left = 168
      Top = 164
      Width = 57
      Height = 24
      Alignment = taCenter
      TabOrder = 2
    end
    object edtPower: TEdit
      Left = 169
      Top = 194
      Width = 57
      Height = 24
      Alignment = taCenter
      TabOrder = 3
    end
  end
  object GroupBox4: TGroupBox
    Left = 273
    Top = 247
    Width = 304
    Height = 210
    Caption = 'Question 1.4'
    TabOrder = 3
    object Label12: TLabel
      Left = 20
      Top = 26
      Width = 32
      Height = 16
      Caption = 'Units'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label13: TLabel
      Left = 119
      Top = 26
      Width = 74
      Height = 16
      Caption = 'Daily Usage'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object btnCalculate: TButton
      Left = 40
      Top = 54
      Width = 209
      Height = 30
      Caption = '1.4  Calculate Days'
      TabOrder = 0
      OnClick = btnCalculateClick
    end
    object edtUnits: TEdit
      Left = 58
      Top = 24
      Width = 47
      Height = 24
      Alignment = taCenter
      TabOrder = 1
    end
    object edtDailyusage: TEdit
      Left = 199
      Top = 24
      Width = 49
      Height = 24
      Alignment = taCenter
      TabOrder = 2
    end
    object memDisplay: TMemo
      Left = 24
      Top = 96
      Width = 257
      Height = 105
      Lines.Strings = (
        'Display output here')
      TabOrder = 3
    end
  end
end
