unit question3_U;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, player_U, ComCtrls;

type
  TfrmQuestion3 = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    edtName: TEdit;
    edtEmail: TEdit;
    edtDateOfBirth: TEdit;
    btnSubmitDetails: TButton;
    Label6: TLabel;
    Label7: TLabel;
    btnApprove: TButton;
    edtAge: TEdit;
    btnAgeTest: TButton;
    edtAgeResult: TEdit;
    redDisplay: TRichEdit;
    Label5: TLabel;
    edtToday: TEdit;
    Label8: TLabel;
    procedure btnSubmitDetailsClick(Sender: TObject);
    procedure btnAgeTestClick(Sender: TObject);
    procedure btnApproveClick(Sender: TObject);
    procedure FormShow(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmQuestion3: TfrmQuestion3;
  objPlayer : TPlayer;

implementation

{$R *.dfm}


procedure TfrmQuestion3.btnSubmitDetailsClick(Sender: TObject);
begin
  // Question 3.2.1
end;


procedure TfrmQuestion3.btnAgeTestClick(Sender: TObject);
begin
    // Question 3.2.2
end;


procedure TfrmQuestion3.btnApproveClick(Sender: TObject);
begin
   // Question 3.2.3
end;


// Provided code
procedure TfrmQuestion3.FormShow(Sender: TObject);
begin
   edtToday.text := DateToStr(Date);
end;
end.
