object frmQuestion3: TfrmQuestion3
  Left = 0
  Top = 0
  Caption = 'Question 3'
  ClientHeight = 332
  ClientWidth = 538
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object Label1: TLabel
    Left = 18
    Top = 8
    Width = 343
    Height = 25
    Alignment = taCenter
    AutoSize = False
    Caption = 'LAN FANatics - Player Management'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 18
    Top = 95
    Width = 80
    Height = 16
    Caption = 'Player Name'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 18
    Top = 61
    Width = 83
    Height = 16
    Caption = 'Player E-mail'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label4: TLabel
    Left = 18
    Top = 131
    Width = 83
    Height = 16
    Caption = 'Date of Birth'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label6: TLabel
    Left = 18
    Top = 226
    Width = 85
    Height = 16
    Caption = 'Minimum Age'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label7: TLabel
    Left = 296
    Top = 39
    Width = 46
    Height = 16
    Caption = 'Display'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label5: TLabel
    Left = 383
    Top = 15
    Width = 35
    Height = 16
    Caption = 'Date '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label8: TLabel
    Left = 18
    Top = 302
    Width = 103
    Height = 16
    Caption = 'Age Test Result'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object edtName: TEdit
    Left = 128
    Top = 92
    Width = 153
    Height = 24
    TabOrder = 1
  end
  object edtEmail: TEdit
    Left = 128
    Top = 62
    Width = 153
    Height = 24
    TabOrder = 0
  end
  object edtDateOfBirth: TEdit
    Left = 128
    Top = 122
    Width = 153
    Height = 24
    TabOrder = 2
  end
  object btnSubmitDetails: TButton
    Left = 42
    Top = 167
    Width = 215
    Height = 35
    Caption = '3.2.1  Submit Details'
    TabOrder = 3
    OnClick = btnSubmitDetailsClick
  end
  object btnApprove: TButton
    Left = 296
    Top = 248
    Width = 217
    Height = 62
    Caption = '3.2.3  Approve'
    TabOrder = 4
    OnClick = btnApproveClick
  end
  object edtAge: TEdit
    Left = 128
    Top = 223
    Width = 33
    Height = 24
    TabOrder = 5
  end
  object btnAgeTest: TButton
    Left = 42
    Top = 253
    Width = 215
    Height = 35
    Caption = '3.2.2  Test Age'
    TabOrder = 6
    OnClick = btnAgeTestClick
  end
  object edtAgeResult: TEdit
    Left = 127
    Top = 298
    Width = 90
    Height = 24
    TabOrder = 7
  end
  object redDisplay: TRichEdit
    Left = 296
    Top = 61
    Width = 217
    Height = 141
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    Lines.Strings = (
      '')
    ParentFont = False
    TabOrder = 8
  end
  object edtToday: TEdit
    Left = 424
    Top = 12
    Width = 105
    Height = 24
    TabOrder = 9
    Text = 'edtToday'
  end
end
