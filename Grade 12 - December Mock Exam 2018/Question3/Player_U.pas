unit Player_U;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DateUtils;

type
  TPlayer = class(TObject)

  private
      fEmail: String;
      fName: String;
      fDateOfBirth: String;
      fApproved: Boolean;


  public
    constructor create(name, email, dateOfBirth: String);
    function toString: String;

  end;


implementation


{ TPlayer }

// 3.1.1


// 3.1.2


// 3.1.3


// 3.1.4

constructor TPlayer.create(name, email, dateOfBirth: String);
begin
   fName := name;
   fDateOfBirth := dateOfBirth;
   fApproved := false;
end;


// 3.1.5


// Provided Code - do not change

function TPlayer.toString: String;
var
 sOut, sApp: String;
begin
  sOut := 'Name: ' + fName + #13;
  sOut := sOut + 'E-mail: ' + fEmail + #13;
  sOut := sOut + 'Date of Birth: ' + fDateOfBirth + #13;
  if fApproved then
     sApp := 'YES'
    else sApp := 'NO';
  sOut := sOut + 'Approved: ' + sApp + #13;
  result := sOut;
end;




end.
