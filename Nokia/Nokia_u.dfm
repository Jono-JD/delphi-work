object Screen: TScreen
  Left = 0
  Top = 0
  Caption = 'Screen'
  ClientHeight = 562
  ClientWidth = 304
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object memOut: TMemo
    Left = 2
    Top = 2
    Width = 300
    Height = 150
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
    ScrollBars = ssVertical
    TabOrder = 0
  end
  object Grid: TGridPanel
    Left = 2
    Top = 168
    Width = 300
    Height = 375
    ColumnCollection = <
      item
        Value = 33.333333333333350000
      end
      item
        Value = 33.333333333333330000
      end
      item
        Value = 33.333333333333320000
      end>
    ControlCollection = <
      item
        Column = 0
        Control = btnClear
        Row = 0
      end
      item
        Column = 1
        Control = btnSpace
        Row = 0
      end
      item
        Column = 2
        Control = btnBack
        Row = 0
      end
      item
        Column = 0
        Control = btn1
        Row = 1
      end
      item
        Column = 1
        Control = btn2
        Row = 1
      end
      item
        Column = 2
        Control = btn3
        Row = 1
      end
      item
        Column = 0
        Control = btn4
        Row = 2
      end
      item
        Column = 1
        Control = btn5
        Row = 2
      end
      item
        Column = 2
        Control = btn6
        Row = 2
      end
      item
        Column = 0
        Control = btn7
        Row = 3
      end
      item
        Column = 1
        Control = btn8
        Row = 3
      end
      item
        Column = 2
        Control = btn9
        Row = 3
      end
      item
        Column = 0
        Control = btn10
        Row = 4
      end
      item
        Column = 1
        Control = btn11
        Row = 4
      end
      item
        Column = 2
        Control = btn12
        Row = 4
      end>
    ExpandStyle = emFixedSize
    RowCollection = <
      item
        Value = 19.999999967166120000
      end
      item
        Value = 20.000000079262060000
      end
      item
        Value = 20.000000027484810000
      end
      item
        Value = 19.999999959608500000
      end
      item
        Value = 19.999999966478510000
      end>
    TabOrder = 1
    DesignSize = (
      300
      375)
    object btnClear: TButton
      Left = 13
      Top = 13
      Width = 75
      Height = 50
      Anchors = []
      TabOrder = 0
      WordWrap = True
      OnClick = btn12Click
      ExplicitLeft = 287
      ExplicitTop = 475
    end
    object btnSpace: TButton
      Left = 112
      Top = 13
      Width = 75
      Height = 50
      Anchors = []
      TabOrder = 1
      WordWrap = True
      OnClick = btn12Click
      ExplicitLeft = 368
      ExplicitTop = 475
    end
    object btnBack: TButton
      Left = 211
      Top = 13
      Width = 75
      Height = 50
      Anchors = []
      TabOrder = 2
      WordWrap = True
      OnClick = btn12Click
      ExplicitLeft = 463
      ExplicitTop = 475
    end
    object btn1: TButton
      Left = 13
      Top = 87
      Width = 75
      Height = 50
      Anchors = []
      TabOrder = 3
      WordWrap = True
      OnClick = btn1Click
      ExplicitLeft = 2
      ExplicitTop = 174
    end
    object btn2: TButton
      Left = 112
      Top = 87
      Width = 75
      Height = 50
      Anchors = []
      TabOrder = 4
      WordWrap = True
      OnClick = btn2Click
      ExplicitLeft = 104
      ExplicitTop = 174
    end
    object btn3: TButton
      Left = 211
      Top = 87
      Width = 75
      Height = 50
      Anchors = []
      TabOrder = 5
      WordWrap = True
      OnClick = btn3Click
      ExplicitLeft = 207
      ExplicitTop = 174
    end
    object btn4: TButton
      Left = 13
      Top = 161
      Width = 75
      Height = 50
      Anchors = []
      TabOrder = 6
      WordWrap = True
      OnClick = btn4Click
      ExplicitLeft = 2
      ExplicitTop = 248
    end
    object btn5: TButton
      Left = 112
      Top = 161
      Width = 75
      Height = 50
      Anchors = []
      TabOrder = 7
      WordWrap = True
      OnClick = btn5Click
      ExplicitLeft = 104
      ExplicitTop = 248
    end
    object btn6: TButton
      Left = 211
      Top = 161
      Width = 75
      Height = 50
      Anchors = []
      TabOrder = 8
      WordWrap = True
      OnClick = btn6Click
      ExplicitLeft = 207
      ExplicitTop = 248
    end
    object btn7: TButton
      Left = 13
      Top = 235
      Width = 75
      Height = 50
      Anchors = []
      TabOrder = 9
      WordWrap = True
      OnClick = btn7Click
      ExplicitLeft = 2
      ExplicitTop = 328
    end
    object btn8: TButton
      Left = 112
      Top = 235
      Width = 75
      Height = 50
      Anchors = []
      TabOrder = 10
      WordWrap = True
      OnClick = btn8Click
      ExplicitLeft = 104
      ExplicitTop = 328
    end
    object btn9: TButton
      Left = 211
      Top = 235
      Width = 75
      Height = 50
      Anchors = []
      TabOrder = 11
      WordWrap = True
      OnClick = btn9Click
      ExplicitLeft = 207
      ExplicitTop = 328
    end
    object btn10: TButton
      Left = 13
      Top = 310
      Width = 75
      Height = 50
      Anchors = []
      TabOrder = 12
      WordWrap = True
      OnClick = btn10Click
      ExplicitLeft = 2
      ExplicitTop = 403
    end
    object btn11: TButton
      Left = 112
      Top = 310
      Width = 75
      Height = 50
      Anchors = []
      TabOrder = 13
      WordWrap = True
      OnClick = btn11Click
      ExplicitLeft = 104
      ExplicitTop = 403
    end
    object btn12: TButton
      Left = 211
      Top = 310
      Width = 75
      Height = 50
      Anchors = []
      TabOrder = 14
      WordWrap = True
      OnClick = btn12Click
      ExplicitLeft = 207
      ExplicitTop = 403
    end
  end
end
