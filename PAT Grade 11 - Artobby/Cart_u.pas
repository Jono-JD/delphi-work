unit Cart_u;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, jpeg, Grids, DBGrids, DateUtils;

type
  TfrmCart = class(TForm)
    imgBanner: TImage;
    btnBack: TPanel;
    ScrollBox: TScrollBox;
    GridPanel: TGridPanel;
    btnDeleteSelected: TPanel;
    btnCheckout: TPanel;
    Panel1: TPanel;
    Panel4: TPanel;
    Panel5: TPanel;
    Panel6: TPanel;
    Panel7: TPanel;
    CheckBox1: TCheckBox;
    Image2: TImage;
    Label2: TLabel;
    Panel8: TPanel;
    Label1: TLabel;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure ScrollBoxMouseWheelDown(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure ScrollBoxMouseWheelUp(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure btnBackMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure btnBackMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure btnDeleteSelectedMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure btnDeleteSelectedMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure btnCheckoutMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure btnCheckoutMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure FormShow(Sender: TObject);
    procedure btnBackClick(Sender: TObject);
    procedure UpdateDeleteArray(Sender: TObject);
    procedure btnDeleteSelectedClick(Sender: TObject);
    procedure populateArtworkDetailsView(Sender: TObject);
    procedure btnCheckoutClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCart: TfrmCart;
  numChecked:integer;

implementation
uses
    Global_u, ArtworksGallery_u, DataModule_u, ArtworkDetail_u;

{$R *.dfm}

procedure TfrmCart.btnBackClick(Sender: TObject);
begin
    frmArtworksGallery.Show;
    self.Hide;
end;

procedure TfrmCart.btnBackMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
    btnBack.BevelOuter:=bvLowered;
end;

procedure TfrmCart.btnBackMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
    btnBack.BevelOuter:=bvRaised;
end;

procedure TfrmCart.btnCheckoutClick(Sender: TObject);
var
    buttonSelected:Integer;
    F:textFile;
    FileName:string;
    RandomDate:TDateTime;

begin
    // Show a confirmation dialog
    buttonSelected := messagedlg('Would you like to us to Generate an Invoice for you?',mtConfirmation, mbYesNo, 0);

    // Show the button type selected
    if buttonSelected = mrYes     then
    begin
        FileName:='invoices\'+SessionName+'_'+IntToStr(YearOf(RandomDate)) + IntToStr(MonthOf(RandomDate)) + IntToStr(DayOf(RandomDate)) + IntToStr(HourOf(RandomDate)) + IntToStr(MinuteOf(RandomDate)) + IntToStr(SecondOf(RandomDate)) + IntToStr(MilliSecondOf(RandomDate))+'.txt';
        AssignFile(F, FileName);
        Rewrite(F);
        writeln(F, 'Invoice for ' + SessionName + ' ' + SessionSurname);
        writeln(F, '');
        with DM do
        begin
            tblCart.Open;
            tblCart.First;
            while not tblCart.Eof do
            begin
                if tblCart['userID'] = SessionID then
                begin
                    tblArt.Open;
                    tblArt.First;
                    tblArt.Locate('ID', StrToInt(tblCart['artID']), []);
                    writeln(F, 'Name of Artwork: ' + tblArt['artName']);
                    writeln(F, 'Price of Artwork: ' + tblArt['artPrice']);
                    writeln(F, 'Length of Artwork: ' + tblArt['artLength']);
                    writeln(F, 'Width of Artwork: ' + tblArt['artWidth']);
                    writeln(F, 'Depth of Artwork: ' + tblArt['artDepth']);
                    writeln(F, 'Material used to create artwork: ' + tblArt['artMaterial']);
                    writeln(F, '');
                    writeln(F, '');
                    tblArt.Close;
                end;
                tblCart.Next;
            end;
            tblCart.Close;
        end;
        CloseFile(F);
    end
    else
    begin
        showMessage('Next Time');
    end;
end;

procedure TfrmCart.btnCheckoutMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
    btnCheckout.BevelOuter:=bvLowered;
end;

procedure TfrmCart.btnCheckoutMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
    btnCheckout.BevelOuter:=bvRaised;
end;

procedure TfrmCart.btnDeleteSelectedClick(Sender: TObject);
var
  i: Integer;
begin
    with DM do
    begin
       tblCart.Open;
       tblCart.First;
       tblCart.Filtered:=False;
       tblCart.Filter:='[userID] ='+SessionID;
       tblCart.Filtered:=True;
       for i := 0 to length(Delete) - 1 do
       begin
          tblCart.First;
          tblCart.Locate('artID', Delete[i], []);
          //showMessage(Delete[i]);
          tblCart.Delete;
       end;
    end;
    self.Hide;
    self.Show;
end;

procedure TfrmCart.btnDeleteSelectedMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
    btnDeleteSelected.BevelOuter:=bvLowered;
end;

procedure TfrmCart.btnDeleteSelectedMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
    btnDeleteSelected.BevelOuter:=bvRaised;
end;

procedure TfrmCart.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
  Params.ExStyle := Params.ExStyle or WS_EX_APPWINDOW;
  Params.WndParent := GetDesktopWindow;
end;

procedure TfrmCart.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    Application.Terminate;
end;

procedure TfrmCart.FormShow(Sender: TObject);
var
    numRows:integer;
    PCheckBoxWrapper:TPanel;
    PImageWrapper:TPanel;
    PNameWrapper:TPanel;
    PPriceWrapper:TPanel;
    PViewWrapper:TPanel;
    CB:TCheckBox;
    I:TImage;
    LName:TLabel;
    LPrice:TLabel;
    B:TButton;
    objName:string;
    identifier:string;
    currentRow: integer;


begin
    setLength(Delete, 0);
    numRows:=0;
    GridPanel.ControlCollection.Clear;
    GridPanel.RowCollection.Clear;
    GridPanel.Height:=0;
    ScrollBox.VertScrollBar.Range:=0;
    btnDeleteSelected.Hide;
    with DM do
    begin
      tblCart.Open;
      tblCart.First;

      while not tblCart.Eof do
      begin
        if tblCart['userID'] = SessionID then
        begin
            inc(numRows);
        end;
        tblCart.Next;
      end;

      for currentRow:= 0 to numRows-1 do
      begin
          GridPanel.RowCollection.Add;
          GridPanel.RowCollection[currentRow].SizeStyle:=ssAbsolute;
          GridPanel.RowCollection[currentRow].Value:=100;
      end;

      GridPanel.Height:=100*numRows;
      ScrollBox.VertScrollBar.Range:=100*numRows;

      tblCart.First;
      while not tblCart.Eof do
      begin
          if tblCart['userID'] = SessionID then
          begin
              tblArt.Open;
              tblArt.First;
              tblArt.Locate('ID', tblCart['artID'], []);

              identifier:=tblCart['artID'];
              objName:='pnlCheckBoxWrapper'+identifier;
              PCheckBoxWrapper:=TPanel.Create(Self);
              PCheckBoxWrapper.Width:=50 ;
              PCheckBoxWrapper.Height:=100;
              PCheckBoxWrapper.Name:=objName;
              PCheckBoxWrapper.Parent:=GridPanel;
              PCheckBoxWrapper.ShowCaption:=False;

              objName:='pnlImageWrapper'+identifier;
              PImageWrapper:=TPanel.Create(self);
              PImageWrapper.Width:=100;
              PImageWrapper.Height:=100;
              PImageWrapper.Name:=objName;
              PImageWrapper.Parent:=GridPanel;
              PImageWrapper.ShowCaption:=False;

              objName:='pnlNameWrapper'+identifier;
              PNameWrapper:=TPanel.Create(self);
              PNameWrapper.Width:=250;
              PNameWrapper.Height:=100;
              PNameWrapper.Name:=objName;
              PNameWrapper.Parent:=GridPanel;
              PNameWrapper.ShowCaption:=False;

              objName:='pnlPriceWrapper'+identifier;
              PPriceWrapper:=TPanel.Create(self);
              PPriceWrapper.Width:=200;
              PPriceWrapper.Height:=100;
              PPriceWrapper.Name:=objName;
              PPriceWrapper.Parent:=GridPanel;
              PPriceWrapper.ShowCaption:=False;

              objName:='pnlViewWrapper'+identifier;
              PViewWrapper:=TPanel.Create(self);
              PViewWrapper.Width:=200;
              PViewWrapper.Height:=100;
              PViewWrapper.Name:=objName;
              PViewWrapper.Parent:=GridPanel;
              PViewWrapper.ShowCaption:=False;

              objName:='cb'+identifier;
              CB:=TCheckBox.Create(self);
              CB.Width:=17;
              CB.Height:=17;
              CB.Left:=16;
              CB.Top:=41;
              CB.Caption:='';
              CB.Name:=objName;
              CB.Parent:=PCheckBoxWrapper;
              CB.OnClick:=UpdateDeleteArray;

              objName:='img'+identifier;
              I:=TImage.Create(self);
              I.Width:=80;
              I.Height:=80;
              I.Left:=10;
              I.Top:=10;
              I.Name:=objName;
              I.Stretch:=True;
              I.Picture.LoadFromFile(tblArt['artPath']);
              I.Parent:=PImageWrapper;

              objName:='lblName'+identifier;
              LName:=TLabel.Create(self);
              LName.Name:=objName;
              LName.Caption:=tblArt['artName']+' ';
              LName.Parent:=PNameWrapper;
              LName.left := (PNameWrapper.width div 2) - LName.width div 2;
              LName.top := (PNameWrapper.height div 2) - LName.height div 2;


              objName:='lblPrice'+identifier;
              LPrice:=TLabel.Create(self);
              LPrice.Name:=objName;
              LPrice.Caption:='R'+tblArt['artPrice']+'00 ';
              LPrice.Parent:=PPriceWrapper;
              LPrice.left := (PPriceWrapper.width div 2) - LPrice.width div 2;
              LPrice.top := (PPriceWrapper.height div 2) - LPrice.height div 2;

              objName:='btn'+identifier;
              B:=TButton.Create(self);
              B.Name:=objName;
              B.Caption:='View Details ';
              B.Parent:=PViewWrapper;
              B.Height:=40;
              B.Width:=150;
              B.Left:=25;
              B.Top:=30;
              B.OnClick:=PopulateArtworkDetailsView;
              tblArt.Close;
          end;
          tblCart.Next;
      end;
      tblCart.Close;
    end;
end;

procedure TfrmCart.populateArtworkDetailsView(Sender: TObject);
begin
    with Sender as TButton do
    begin
        ArtworkID:=copy(name, 4, length(name));
        //showMessage(id);
        with DM do
        begin
            tblArt.Open;
            tblArt.First;
            while not tblArt.Eof do
            begin
                if tblArt['ID'] = ArtworkID then
                begin
                    ArtworkName:=tblArt['artName'];
                    ArtworkPrice:=tblArt['artPrice'];
                    ArtworkDescription:=tblArt['artDescription'];
                    ArtworkLength:=tblArt['artLength'];
                    ArtworkWidth:=tblArt['artWidth'];
                    ArtworkDepth:=tblArt['artDepth'];
                    ArtworkMaterial:=tblArt['artMaterial'];
                    ArtworkPath:=tblArt['artPath'];
                    ArtworkCreatedAt:=tblArt['artCreatedAt'];
                    frmArtworkDetail.Show;
                    self.Hide;
                end;
                tblArt.Next;
            end;
            tblArt.Close;
        end;
    end;
end;

procedure TfrmCart.ScrollBoxMouseWheelDown(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
    ScrollBox.VertScrollBar.Position :=  ScrollBox.VertScrollBar.Position + 12
end;

procedure TfrmCart.ScrollBoxMouseWheelUp(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
    ScrollBox.VertScrollBar.Position :=  ScrollBox.VertScrollBar.Position - 12;
end;

procedure TfrmCart.UpdateDeleteArray(Sender: TObject);
var
    id:string;
    i: integer;
    deletePos:integer;
    j: integer;
    m: integer;
begin
    deletePos:=0;
    with Sender as TCheckBox do
    begin
        id:=copy(name, 3, length(name));
        //showMessage(id);
        if checked then
        begin
          inc(numChecked);
          //showMessage('It is checked');
          setLength(Delete, Length(Delete)+1);
          Delete[length(Delete)-1]:=id;
        end
        else
        begin
          dec(numChecked);
          //showMessage('It is not Checked');
          for i := 1 to Length(Delete) do
          begin
              if id = delete[i] then
              begin
                  deletePos:=i;
              end;
          end;

          for j:= deletePos to length(Delete)-1 do
          begin
              Delete[j]:=Delete[j+1];
          end;
          setLength(Delete, Length(Delete)-1);
          //showMessage(IntToStr(DeletePos));
        end;

        if numChecked > 0 then
        begin
          btnDeleteSelected.Visible:=True;
        end
        else
        begin
          btnDeleteSelected.Visible:=False;
        end;

//        Label3.Caption:='';
//        for m := 0 to length(Delete)-1 do
//        begin
//            Label3.Caption:=Label3.Caption+' '+Delete[m];
//        end;
    end;
end;

end.
