unit UpdateArtists_u;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, jpeg, ExtDlgs, Math, DateUtils;

type
  TfrmUpdateArtist = class(TForm)
    imgBanner: TImage;
    imgAvatar: TImage;
    memArtistBio: TMemo;
    lblBio: TLabel;
    lblCountry: TLabel;
    edtAritstName: TEdit;
    lblName: TLabel;
    edtArtistContact: TEdit;
    lblContact: TLabel;
    edtArtistEmail: TEdit;
    lblEmail: TLabel;
    edtOldPassword: TEdit;
    edtNewPassword: TEdit;
    btnBack: TPanel;
    btnUpdate: TPanel;
    lblOldPassword: TLabel;
    lblNewPassword: TLabel;
    edtConfirmPassword: TEdit;
    lblConfirmPassword: TLabel;
    cmbCountry: TComboBox;
    dlgImage: TOpenPictureDialog;
    procedure FormShow(Sender: TObject);
    procedure btnBackClick(Sender: TObject);
    procedure btnUpdateMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure btnUpdateMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure btnBackMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure btnBackMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure btnUpdateClick(Sender: TObject);
    procedure imgAvatarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmUpdateArtist: TfrmUpdateArtist;
  relativeAvatarPath:string;
  fullAvatartPath:string;
  imgPath:string;
  updateAvatar:boolean;

implementation
uses
    global_u, datamodule_u, ArtistLanding_u;

{$R *.dfm}

procedure TfrmUpdateArtist.btnBackClick(Sender: TObject);
begin
    frmArtistsLanding.show;
    self.Hide;
end;

procedure TfrmUpdateArtist.btnBackMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
    btnBack.BevelOuter:=bvLowered;
end;

procedure TfrmUpdateArtist.btnBackMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
    btnBack.BevelOuter:=bvRaised;
end;

procedure TfrmUpdateArtist.btnUpdateClick(Sender: TObject);
var
    validInput:boolean;
    passwordChange:boolean;
begin
    validInput:=True;
    passwordChange:=False;

    // Country Field Required
    if cmbCountry.Text = '' then
    begin
        lblCountry.Caption:='*Required ';
        lblCountry.Font.Color:=clRed;
        ValidInput:=False;
    end
    else
    begin
        lblCountry.Caption:='Country ';
        lblCountry.Font.Color:=clBlack;
    end;


    // Contact Field Requried
    if edtArtistContact.Text = '' then
    begin
        lblContact.Caption:='*Required ';
        lblContact.Font.Color:=clRed;
        ValidInput:=False;
    end
    else
    begin
        lblContact.Caption:='Contact Details ';
        lblContact.Font.Color:=clBlack;
    end;


    // If there are values in any one of the three password fields then include the checks into the validation
    if not(edtOldPassword.Text = '') or not(edtNewPassword.Text = '') or not(edtConfirmPassword.Text = '') then
    begin
        // OldPassword Field Requried
        if edtOldPassword.Text = '' then
        begin
            lblOldPassword.Caption:='*Required ';
            lblOldPassword.Font.Color:=clRed;
            edtOldPassword.ShowHint:=True;
            ValidInput:=False;
        end
        else
        begin
            lblOldPassword.Caption:='Old Password ';
            lblOldPassword.Font.Color:=clBlack;
            edtOldPassword.ShowHint:=False;
        end;

        // New Password Requried
        if edtNewPassword.Text = '' then
        begin
            lblNewPassword.Caption:='*Required ';
            lblNewPassword.Font.Color:=clRed;
            edtNewPassword.ShowHint:=True;
            ValidInput:=False;
        end
        else
        begin
            lblNewPassword.Caption:='New Password ';
            lblNewPassword.Font.Color:=clBlack;
            edtNewPassword.ShowHint:=False;
        end;

        // Confirmation of New Password Requried
        if edtConfirmPassword.Text = '' then
        begin
            lblConfirmPassword.Caption:='*Required ';
            lblConfirmPassword.Font.Color:=clRed;
            edtConfirmPassword.ShowHint:=True;
            ValidInput:=False;
        end
        else
        begin
            lblConfirmPassword.Caption:='Confirm Password ';
            lblConfirmPassword.Font.Color:=clBlack;
            edtConfirmPassword.ShowHint:=False;
        end;

        // If all three fields have values then do these checks
        if not(edtOldPassword.Text = '') and not(edtNewPassword.Text='') and not(edtConfirmPassword.Text='') then
        begin
            with DM do
            begin
                tblUser.Open;
                tblUser.First;
                tblUser.Locate('ID', StrToInt(SessionID), []);
                if edtOldPassword.Text = tblUser['userPassword'] then
                begin
                    lblOldPassword.Caption:='Old Password ';
                    lblOldPassword.Font.Color:=clBlack;
                    if not(edtNewPassword.text = edtConfirmPassword.Text) then
                    begin
                        lblOldPassword.Caption:='* Password Do not Match ';
                        lblOldPassword.Font.Color:=clRed;
                        validInput:=False;
                    end
                    else
                    begin
                        lblOldPassword.Caption:='Old Password ';
                        lblOldPassword.Font.Color:=clBlack;
                        PasswordChange:=True;
                    end;
                end
                else
                begin
                    validInput:=False;
                    lblOldPassword.Caption:='* Incorrect Credentials ';
                    lblOldPassword.Font.Color:=clRed;
                end;
            end;
        end;
    end;

    if validInput then
    begin
      with dm do
      begin
          tblUser.Open;
          tblUser.Locate('ID', StrToInt(SessionID), []);
          tblUser.Edit;
          tblUser['userCountry']:= cmbCountry.Text;
          tblUser['userContact']:= edtArtistContact.Text;
          tblUser['userDescription']:=memArtistBio.Text;
          if PasswordChange then
          begin
              tblUser['userPassword']:= edtNewPassword.Text;
          end;

          if UpdateAvatar then
          begin
              if copyFile(PChar(imgPath), PChar(fullAvatartPath), false) then
              begin
                  tblUser['avatarPath']:=relativeAvatarPath;
              end
              else
              begin
                showMessage('Looks like there is a problem with updating you avatar... Try again later!');
              end;

          end;
          tblUser.Post;
          tblUser.Close;
      end;
    end;
end;

procedure TfrmUpdateArtist.btnUpdateMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
    btnUpdate.BevelOuter:=bvLowered;
end;

procedure TfrmUpdateArtist.btnUpdateMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
    btnUpdate.BevelOuter:=bvRaised;
end;

procedure TfrmUpdateArtist.FormShow(Sender: TObject);
begin
    cmbCountry.Items.Clear;
    edtAritstName.Text:=SessionName + ' ' + SessionSurname;
    edtArtistContact.Text:=SessionContact;
    cmbCountry.Text:=SessionCOuntry;
    edtArtistEmail.Text:=SessionEmail;
    memArtistBio.Text:=SessionDescription;
    imgAvatar.Picture.LoadFromFile(SessionAvatar);

    with DM do
    begin
      tblCountry.Open;
      tblCountry.First;
      while not tblCountry.Eof do
      begin
          cmbCountry.Items.Add(tblCountry['Country']);
          tblCountry.Next;
      end;
      tblCountry.Close;
    end;
end;

procedure TfrmUpdateArtist.imgAvatarClick(Sender: TObject);
var
  RandomDate:TDateTime;

begin
    RandomDate:=Now;
    if dlgImage.Execute then
    begin
        imgPath:=dlgImage.FileName;
        relativeAvatarPath:='images\Avatars\' + IntToStr(RandomRange(0, 999)) + '_' + IntToStr(YearOf(RandomDate)) + IntToStr(MonthOf(RandomDate)) + IntToStr(DayOf(RandomDate)) + IntToStr(HourOf(RandomDate)) + IntToStr(MinuteOf(RandomDate)) + IntToStr(SecondOf(RandomDate)) + IntToStr(MilliSecondOf(RandomDate)) + '_' + IntToStr(RandomRange(0, 999)) + ExtractFileExt(imgPath);
        fullAvatartPath:=extractFilePath(Application.Exename) + relativeAvatarPath;
        updateAvatar:=True;
    end
    else
    begin
        updateAvatar:=False;
    end;
end;

end.
