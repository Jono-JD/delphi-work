unit UpdateArtwork_u;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, jpeg, ExtCtrls, ComCtrls, StdCtrls;

type
  TfrmUpdateArtwork = class(TForm)
    memArtworkDescription: TMemo;
    edtArtworkLength: TEdit;
    edtArtworkWidth: TEdit;
    edtArtworkDepth: TEdit;
    edtArtworkMaterial: TEdit;
    dtpCreatedAt: TDateTimePicker;
    edtArtworkName: TEdit;
    edtArtworkPrice: TEdit;
    imgBanner: TImage;
    imgArtwork: TImage;
    btnBack: TPanel;
    btnUpdateArtwork: TPanel;
    lblArtworkTitle: TLabel;
    lblArtworkPrice: TLabel;
    lblArtworkMaterial: TLabel;
    lblArtworkDepth: TLabel;
    lblArtworkWidth: TLabel;
    lblArtworkLength: TLabel;
    lblArtworkCreation: TLabel;
    btnDeleteArtowrk: TPanel;
    lblArtworkDescription: TLabel;
    procedure FormShow(Sender: TObject);
    procedure btnBackClick(Sender: TObject);
    procedure btnDeleteArtowrkClick(Sender: TObject);
    procedure btnUpdateArtworkClick(Sender: TObject);
    procedure btnBackMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure btnBackMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure btnUpdateArtworkMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure btnUpdateArtworkMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmUpdateArtwork: TfrmUpdateArtwork;

implementation
uses
  global_u, ArtistLanding_u, DataModule_u;

{$R *.dfm}

procedure TfrmUpdateArtwork.btnUpdateArtworkClick(Sender: TObject);
var
  validDataSet: boolean;

begin
    // It is valid until proved otherwise
    validDataSet := True;

    // Check for Required Title
    if edtArtworkName.Text = '' then
    begin
      lblArtworkTitle.Caption:= '* Title Required ';
      lblArtworkTitle.Font.Color:=clRed;
      validDataSet := False;
    end
    else
    begin
      lblArtworkTitle.Caption:='Title ';
      lblArtworkTitle.Font.Color:=clBlack;
    end;

    // Check for Required Price
    if edtArtworkPrice.Text = '' then
    begin
      lblArtworkPrice.Caption:= '* Price Required ';
      lblArtworkPrice.Font.Color:=clRed;
      validDataSet := False;
    end
    else
    begin
      lblArtworkPrice.Caption:='Price ';
      lblArtworkPrice.Font.Color:=clBlack;
    end;

    // Check for Required Material
    if edtArtworkMaterial.Text = '' then
    begin
      lblArtworkMaterial.Caption:='* Material Required ';
      lblArtworkMaterial.Font.Color:=clRed;
      validDataSet := False;
    end
    else
    begin
      lblArtworkMaterial.Caption:='Material ';
      lblArtworkMaterial.Font.Color:=clBlack;
    end;

    // Check for Required Length
    if edtArtworkLength.Text = '' then
    begin
      lblArtworkLength.Caption:='* Length Required ';
      lblArtworkLength.Font.Color:=clRed;
      validDataSet := False;
    end
    else
    begin
      lblArtworkLength.Caption:='Length ';
      lblArtworkLength.Font.Color:=clBlack;
    end;

    // Check for Required Width
    if edtArtworkWidth.Text = '' then
    begin
      lblArtworkWidth.Caption:='* Width Required ';
      lblArtworkWidth.Font.Color:=clRed;
      validDataSet := False;
    end
    else
    begin
      lblArtworkWidth.Caption:='Width ';
      lblArtworkWidth.Font.Color:=clBlack;
    end;

    // Check for Required Depth
    if edtArtworkDepth.Text = '' then
    begin
      lblArtworkDepth.Caption:='* Depth Required ';
      lblArtworkDepth.Font.Color:=clRed;
      validDataSet := False;
    end
    else
    begin
      lblArtworkDepth.Caption:='Depth ';
      lblArtworkDepth.Font.Color:=clBlack;
    end;

    // Check for Required Description
    if memArtworkDescription.Text = '' then
    begin
      lblArtworkDescription.Caption:='* Description Required ';
      lblArtworkDescription.Font.Color:=clRed;
      validDataSet := False;
    end
    else
    begin
      lblArtworkDescription.Caption:='Description ';
      lblArtworkDescription.Font.Color:=clBlack;
    end;

    if validDataSet then
    begin
        with DM do
        begin
            tblArt.Open;
            tblArt.First;
            while not tblArt.Eof do
            begin
                if tblArt['ID'] = ArtworkID then
                begin
                    tblArt.Edit;
                    tblArt['artName'] := edtArtworkName.Text;
                    tblArt['artDescription'] := memArtworkDescription.Text;
                    tblArt['artWidth'] := edtArtworkWidth.Text;
                    tblArt['artLength'] := edtArtworkLength.Text;
                    tblArt['artDepth'] := edtArtworkDepth.Text;
                    tblArt['artMaterial'] := edtArtworkMaterial.Text;
                    tblArt['artPrice'] := edtArtworkPrice.Text;
                    tblArt.Post;
                end;
                tblArt.Next;
            end;
            tblArt.Close;
        end;
        showMessage('Artwork Update Successful!');
    end;
end;

procedure TfrmUpdateArtwork.btnUpdateArtworkMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
    btnUpdateArtwork.BevelOuter:=bvLowered;
end;

procedure TfrmUpdateArtwork.btnUpdateArtworkMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
    btnUpdateArtwork.BevelOuter:=bvRaised;
end;

procedure TfrmUpdateArtwork.btnBackClick(Sender: TObject);
begin
    frmArtistsLanding.Show;
    frmUpdateArtwork.Hide;
end;

procedure TfrmUpdateArtwork.FormShow(Sender: TObject);
begin
    imgArtwork.Picture.LoadFromFile(ArtworkPath);
    dtpCreatedAt.Date:=StrToDate(ArtworkCreatedAt);
    edtArtworkName.Text:=ArtworkName;
    edtArtworkPrice.Text:=ArtworkPrice;
    edtArtworkLength.Text:=ArtworkLength;
    edtArtworkWidth.Text:=ArtworkWidth;
    edtArtworkDepth.Text:=ArtworkDepth;
    edtArtworkMaterial.Text:=ArtworkMaterial;
    memArtworkDescription.Text:=ArtworkDescription;
end;

procedure TfrmUpdateArtwork.btnBackMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
    btnBack.BevelOuter:=bvLowered;
end;

procedure TfrmUpdateArtwork.btnBackMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
    btnBack.BevelOuter:=bvRaised;
end;

procedure TfrmUpdateArtwork.btnDeleteArtowrkClick(Sender: TObject);
    var
  buttonSelected : Integer;
begin
  // Show a confirmation dialog
  buttonSelected := messagedlg('Are you sure you want to delete this Artwork? (Note that this data will NOT be retrievable!)',mtWarning, mbOKCancel, 0);

  // Show the button type selected
  if buttonSelected = mrOK     then
  begin
      //ShowMessage('OK pressed');
      with DM do
      begin
          tblArt.Open;
          tblArt.First;
          while not tblArt.Eof  do
          begin
              if tblArt['ID'] = ArtworkID then
              begin
                tblArt.Delete;
              end;
              tblArt.Next;
          end;
          tblArt.Close;
      end;
      frmArtistsLanding.Show;
      frmUpdateArtwork.Hide;
  end;

  //if buttonSelected = mrCancel then ShowMessage('Cancel pressed');
end;

end.
