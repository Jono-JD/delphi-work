unit ArtistsGallery_u;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ButtonGroup, StdCtrls, ExtCtrls, pngimage, Grids, DBGrids;

type
  TfrmArtistsGallery = class(TForm)
    btnLogout: TPanel;
    btnToggleDropDown: TPanel;
    btnViewArtists: TPanel;
    btnViewArtworks: TPanel;
    lblTitle: TLabel;
    navSpacer: TPanel;
    pnlFilter: TPanel;
    lblFilter: TLabel;
    btnFilterCountry: TPanel;
    btnFilterName: TPanel;
    btnFilterNone: TPanel;
    ScrollBox: TScrollBox;
    GridPanel: TGridPanel;
    Panel1: TPanel;
    Image1: TImage;
    Button1: TButton;
    btnPreferences: TPanel;
    btnCart: TPanel;
    cmbCountry: TComboBox;
    edtSearch: TEdit;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure btnViewArtworksClick(Sender: TObject);
    procedure btnViewArtworksMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure btnViewArtworksMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure btnLogoutMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure btnLogoutMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure btnFilterNoneClick(Sender: TObject);
    procedure clearGallery;
    procedure showAllArtists;
    procedure resetFilterBevel;
    procedure populateArtistDetails(Sender: TObject);
    procedure btnFilterNameClick(Sender: TObject);
    procedure btnFilterCountryClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormMouseWheelUp(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure FormMouseWheelDown(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure btnPreferencesClick(Sender: TObject);
    procedure btnPreferencesMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure btnPreferencesMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure btnCartMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure btnCartMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure btnToggleDropDownClick(Sender: TObject);
    procedure btnCartClick(Sender: TObject);
    procedure btnLogoutClick(Sender: TObject);
    procedure cmbCountryChange(Sender: TObject);
    procedure edtSearchChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmArtistsGallery: TfrmArtistsGallery;

implementation
uses
    ArtworksGallery_u, DataModule_u, Global_u, ArtistDetail_u, UpdateViewer_u, Cart_u, Login_u;

{$R *.dfm}


procedure TfrmArtistsGallery.btnCartClick(Sender: TObject);
begin
    frmCart.Show;
    self.Hide;
end;

procedure TfrmArtistsGallery.btnCartMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
    btnCart.BevelOuter:=bvLowered;
end;

procedure TfrmArtistsGallery.btnCartMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
    btnCart.BevelOuter:=bvRaised;
end;

procedure TfrmArtistsGallery.btnFilterCountryClick(Sender: TObject);
begin
    resetFilterBevel;
    btnFilterCountry.BevelOuter:=bvLowered;
    btnFilterCountry.Color:=clGray;
    lblTitle.Caption:='Artists Filtered by Country ';
    cmbCountry.Show;
    edtSearch.Hide;
end;

procedure TfrmArtistsGallery.btnFilterNameClick(Sender: TObject);
begin
    resetFilterBevel;
    btnFilterName.BevelOuter:=bvLowered;
    btnFilterName.Color:=clGray;
    lblTitle.Caption:='Artists Filtered by Name ';
    edtSearch.Show;
    cmbCountry.Hide;
end;

procedure TfrmArtistsGallery.btnFilterNoneClick(Sender: TObject);
begin
    resetFilterBevel;
    btnFilterNone.BevelOuter:=bvLowered;
    btnFilterNone.Color:=clGray;
    lblTitle.Caption:='All Artists ';
    cmbCountry.Hide;
    edtSearch.Hide;
    showAllArtists;

end;

procedure TfrmArtistsGallery.btnLogoutClick(Sender: TObject);
begin
    Global_u.logout;
    frmLogin.Show;
    self.Hide;
end;

procedure TfrmArtistsGallery.btnLogoutMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
    btnLogout.BevelOuter:=bvLowered;
end;

procedure TfrmArtistsGallery.btnLogoutMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
    btnLogout.BevelOuter:=bvRaised;
end;

procedure TfrmArtistsGallery.btnPreferencesClick(Sender: TObject);
begin
    frmUpdateViewer.show;
    self.Hide;
end;

procedure TfrmArtistsGallery.btnPreferencesMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
    btnPreferences.BevelOuter:=bvLowered;
end;

procedure TfrmArtistsGallery.btnPreferencesMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
    btnPreferences.BevelOuter:=bvRaised;
end;

procedure TfrmArtistsGallery.btnToggleDropDownClick(Sender: TObject);
begin
    if btnPreferences.Visible then
    begin
        btnPreferences.Hide;
        btnCart.Hide;
        btnToggleDropDown.BevelOuter:=bvRaised;
    end
    else
    begin
        btnPreferences.Show;
        btnCart.Show;
        btnToggleDropDown.BevelOuter:=bvLowered;
    end;
end;

procedure TfrmArtistsGallery.btnViewArtworksClick(Sender: TObject);
begin
    ArtworksGallery_u.frmArtworksGallery.Show;
    self.Hide;
end;

procedure TfrmArtistsGallery.btnViewArtworksMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
    btnViewArtworks.BevelOuter:=bvLowered;
end;

procedure TfrmArtistsGallery.btnViewArtworksMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
    btnViewArtworks.BevelOuter:=bvRaised;
end;

procedure TfrmArtistsGallery.clearGallery;
begin
    GridPanel.ControlCollection.Clear;
    GridPanel.RowCollection.Clear;
end;

procedure TfrmArtistsGallery.cmbCountryChange(Sender: TObject);
var
  numRecords:integer;
  numRows:integer;
  currentRow:integer;
  identifier:string;
  objName:string;
  P:TPanel;
  B:TButton;
  I:TImage;


begin
    with DM do
    begin
        tblUser.Open;
        tblUser.Filtered:=False;
        tblUser.Filter:='[userCountry] = '+QuotedStr(cmbCountry.Text)+'AND [userType] = '+QuotedStr('Artist');
        tblUser.Filtered:=True;
        numRecords:=tblUser.RecordCount;
        numRows:=numRecords div 4;
        if not(numRecords mod 4 = 0) then
        begin
          inc(numRows);
        end;
        clearGallery;
        for currentRow := 0 to numRows-1 do
        begin
            GridPanel.RowCollection.Add;
            GridPanel.RowCollection[currentRow].SizeStyle:=ssAbsolute;
            GridPanel.RowCollection[currentRow].Value:=250;
        end;
        tblUser.First;
        while not tblUser.Eof do
        begin
            identifier:=tblUser['ID'];
            objName:='pnl'+identifier;
            P:=TPanel.Create(Self);
            P.Width:=180;
            P.Height:=220;
            P.ParentColor:=False;
            P.ParentBackground:=False;
            P.Color:=clBlack;
            P.Name:=objName;
            P.Parent:=GridPanel;

            objName:='btn'+identifier;
            B:=TButton.Create(self);
            B.Parent:=P;
            B.Width:=150;
            B.Height:=33;
            B.Left:=15;
            B.Top:=168;
            B.Name:=objName;
            B.Caption:='View Profile ';
            B.OnClick:=PopulateArtistDetails;

            objName:='img'+identifier;
            I:=TImage.Create(self);
            I.Parent:=P;
            I.Width:=120;
            I.Height:=120;
            I.Left:=30;
            I.Top:=30;
            I.Name:=objName;
            I.Picture.LoadFromFile(tblUser['AvatarPath']);
            I.Stretch:=True;

            tblUser.Next;
        end;
        tblUser.Close;
    end;
end;

procedure TfrmArtistsGallery.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
    Params.ExStyle := Params.ExStyle or WS_EX_APPWINDOW;
    Params.WndParent := GetDesktopWindow;
end;

procedure TfrmArtistsGallery.edtSearchChange(Sender: TObject);
var
  numRecords:integer;
  numRows:integer;
  currentRow:integer;
  identifier:string;
  objName:string;
  P:TPanel;
  B:TButton;
  I:TImage;

begin
    if not(edtSearch.Text = '') then
    begin
        with DM do
        begin
            tblUser.Open;
            tblUser.Filtered:=False;
            tblUser.Filter:='[userName] like '+QuotedStr(edtSearch.Text+'%')+'AND [userType] = '+QuotedStr('Artist');
            tblUser.Filtered:=True;
            numRecords:=tblUser.RecordCount;
            numRows:=numRecords div 4;
            if not(numRecords mod 4 = 0) then
            begin
              inc(numRows);
            end;
            clearGallery;
            for currentRow := 0 to numRows-1 do
            begin
                GridPanel.RowCollection.Add;
                GridPanel.RowCollection[currentRow].SizeStyle:=ssAbsolute;
                GridPanel.RowCollection[currentRow].Value:=250;
            end;
            tblUser.First;
            while not tblUser.Eof do
            begin
                identifier:=tblUser['ID'];
                objName:='pnl'+identifier;
                P:=TPanel.Create(Self);
                P.Width:=180;
                P.Height:=220;
                P.ParentColor:=False;
                P.ParentBackground:=False;
                P.Color:=clBlack;
                P.Name:=objName;
                P.Parent:=GridPanel;

                objName:='btn'+identifier;
                B:=TButton.Create(self);
                B.Parent:=P;
                B.Width:=150;
                B.Height:=33;
                B.Left:=15;
                B.Top:=168;
                B.Name:=objName;
                B.Caption:='View Profile ';
                B.OnClick:=PopulateArtistDetails;

                objName:='img'+identifier;
                I:=TImage.Create(self);
                I.Parent:=P;
                I.Width:=120;
                I.Height:=120;
                I.Left:=30;
                I.Top:=30;
                I.Name:=objName;
                I.Picture.LoadFromFile(tblUser['AvatarPath']);
                I.Stretch:=True;

                tblUser.Next;
            end;
            tblUser.Close;
        end;
    end
    else
    begin
      DM.tblUser.Filtered:=False;
      showAllArtists;
    end;
end;

procedure TfrmArtistsGallery.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    Application.Terminate;
end;

procedure TfrmArtistsGallery.FormMouseWheelDown(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
    ScrollBox.VertScrollBar.Position :=  ScrollBox.VertScrollBar.Position + 12;
end;

procedure TfrmArtistsGallery.FormMouseWheelUp(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
    ScrollBox.VertScrollBar.Position :=  ScrollBox.VertScrollBar.Position - 12;
end;

procedure TfrmArtistsGallery.FormShow(Sender: TObject);
begin
    resetFilterBevel;
    btnFilterNone.BevelOuter:=bvLowered;
    btnFilterNone.Color:=clGray;
    btnPreferences.Hide;
    btnCart.Hide;
    cmbCountry.Hide;
    edtSearch.Hide;
    btnToggleDropDown.BevelOuter:=bvRaised;
    btnToggleDropDown.Caption:=SessionName+' '+SessionSurname;
    lblTitle.Caption:='All Artists ';
    showAllArtists;
    cmbCountry.Items.Clear;
    with DM do
    begin
      tblCountry.Open;
      tblCountry.First;
      while not tblCountry.Eof do
      begin
          cmbCountry.Items.Add(tblCountry['Country']);
          tblCountry.Next;
      end;
      tblCountry.Close;
    end;
end;

procedure TfrmArtistsGallery.populateArtistDetails(Sender: TObject);
begin
    with Sender as TButton do
    begin
        ArtistID:=copy(name, 4, length(name));
        with DM do
        begin
            tblUser.Open;
            tblUser.First;
            while not tblUser.Eof do
            begin
                if tblUser['ID'] = ArtistID then
                begin
                    ArtistName:=        tblUser['userName'];
                    ArtistSurname:=     tblUser['userSurname'];
                    ArtistEmail:=       tblUser['userEmail'];
                    ArtistContact:=     tblUser['userContact'];
                    ArtistDescription:= tblUser['userDescription'];
                    ArtistAvatar:=      tblUser['avatarPath'];
                    ArtistCountry:=     tblUser['userCountry'];
                end;
                tblUser.Next;
            end;
            tblUser.Close;
            frmArtistDetail.Show;
            self.Hide;
        end;
    end;
end;

procedure TfrmArtistsGallery.resetFilterBevel;
begin
    btnFilterNone.BevelOuter:=bvRaised;
    btnFilterNone.Color:=clSilver;
    btnFilterCountry.BevelOuter:=bvRaised;
    btnFilterCountry.Color:=clSilver;
    btnFilterName.BevelOuter:=bvRaised;
    btnFilterName.Color:=clSilver;
end;

procedure TfrmArtistsGallery.showAllArtists;
var
    P:TPanel;
    B:TButton;
    I:TImage;
    identifier:string;
    objName:string;
    numRows:integer;
    numRecords:integer;
    currentRow:integer;

begin
    numRecords:=0;
    numRows:=0;
    identifier:='';
    objName:='';

    with DM do
    begin
        clearGallery;
        tblUser.Open;
        tblUser.Filtered:=False;
        tblUser.First;
        while not tblUser.Eof do
        begin
            if tblUser['userType'] = 'Artist' then
            begin
                inc(numRecords);
            end;
            tblUser.Next;
        end;

        numRows:=numRecords div 4;

        if not(numRecords mod 4 = 0) then
        begin
            inc(numRows);
        end;

        for currentRow := 0 to numRows-1 do
        begin
            GridPanel.RowCollection.Add;
            GridPanel.RowCollection[currentRow].SizeStyle:=ssAbsolute;
            GridPanel.RowCollection[currentRow].Value:=250;
        end;

        // Sets the height for the grid panel and the scroll view
        GridPanel.Height:=250*numRows;
        ScrollBox.VertScrollBar.Range:=250*numRows;

        tblUser.First;
        while not tblUser.Eof do
        begin
            if tblUser['userType'] = 'Artist' then
            begin
                identifier:=tblUser['ID'];
                objName:='pnl'+identifier;
                P:=TPanel.Create(Self);
                P.Width:=180;
                P.Height:=220;
                P.ParentColor:=False;
                P.ParentBackground:=False;
                P.Color:=clBlack;
                P.Name:=objName;
                P.Parent:=GridPanel;

                objName:='btn'+identifier;
                B:=TButton.Create(self);
                B.Parent:=P;
                B.Width:=150;
                B.Height:=33;
                B.Left:=15;
                B.Top:=168;
                B.Name:=objName;
                B.Caption:='View Profile ';
                B.OnClick:=PopulateArtistDetails;

                objName:='img'+identifier;
                I:=TImage.Create(self);
                I.Parent:=P;
                I.Width:=120;
                I.Height:=120;
                I.Left:=30;
                I.Top:=30;
                I.Name:=objName;
                I.Picture.LoadFromFile(tblUser['AvatarPath']);
                I.Stretch:=True;
            end;
            tblUser.Next;
        end;
        tblUser.Close;
    end;
end;

end.
