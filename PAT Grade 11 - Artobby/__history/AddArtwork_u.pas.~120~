unit AddArtwork_u;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtDlgs, StdCtrls, FileCtrl, XPMan, ActnMan, ActnColorMaps, jpeg,
  ExtCtrls, ComCtrls, pngimage, Math, DateUtils;

type
  TfrmAddArtwork = class(TForm)
    dlgImg: TOpenPictureDialog;
    imgBanner: TImage;
    imgAddImage: TImage;
    edtArtworkName: TEdit;
    edtArtworkPrice: TEdit;
    edtArtworkMaterial: TEdit;
    edtArtworkLength: TEdit;
    dtpCreatedAt: TDateTimePicker;
    edtArtworkDepth: TEdit;
    edtArtworkWidth: TEdit;
    memArtworkDescription: TMemo;
    btnBack: TPanel;
    btnAddArtwork: TPanel;
    lblArtworkTitle: TLabel;
    lblArtworkPrice: TLabel;
    lblArtworkDate: TLabel;
    lblArtworkMaterial: TLabel;
    lblArtworkLength: TLabel;
    lblArtworkWidth: TLabel;
    lblArtworkDepth: TLabel;
    lblArtworkDescription: TLabel;
    lblArtworkImage: TLabel;
    procedure imgAddImageClick(Sender: TObject);
    procedure btnBackClick(Sender: TObject);
    procedure btnAddArtworkMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure btnAddArtworkMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure btnBackMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure btnBackMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure btnAddArtworkClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAddArtwork: TfrmAddArtwork;
  newFullFileName:string;
  newRelativeFileName:string;
  imgPath:string;
  imageAdded:boolean;

implementation

uses
  ArtistLanding_u, DataModule_u, Global_u;
{$R *.dfm}

procedure TfrmAddArtwork.btnAddArtworkClick(Sender: TObject);
var
  validDataSet: boolean;

begin
  // It is valid until proved otherwise
  validDataSet := True;

  // Check for Required Image Loaded
  if not(imageAdded) then
  begin
    lblArtworkImage.Visible := True;
    validDataSet := False;
  end
  else
  begin
    lblArtworkImage.Visible := False;
  end;

  // Check for Required Title
  if edtArtworkName.Text = '' then
  begin
    lblArtworkTitle.Visible := True;
    validDataSet := False;
  end
  else
  begin
    lblArtworkTitle.Visible := False;
  end;

  // Check for Required Price
  if edtArtworkPrice.Text = '' then
  begin
    lblArtworkPrice.Visible := True;
    validDataSet := False;
  end
  else
  begin
    lblArtworkPrice.Visible := False;
  end;

  // Check for Required Material
  if edtArtworkMaterial.Text = '' then
  begin
    lblArtworkMaterial.Visible := True;
    validDataSet := False;
  end
  else
  begin
    lblArtworkMaterial.Visible := False;
  end;

  // Check for Required Length
  if edtArtworkLength.Text = '' then
  begin
    lblArtworkLength.Visible := True;
    validDataSet := False;
  end
  else
  begin
    lblArtworkLength.Visible := False;
  end;

  // Check for Required Width
  if edtArtworkWidth.Text = '' then
  begin
    lblArtworkWidth.Visible := True;
    validDataSet := False;
  end
  else
  begin
    lblArtworkWidth.Visible := False;
  end;

  // Check for Required Depth
  if edtArtworkDepth.Text = '' then
  begin
    lblArtworkDepth.Visible := True;
    validDataSet := False;
  end
  else
  begin
    lblArtworkDepth.Visible := False;
  end;

  // Check for Required Description
  if memArtworkDescription.Text = '' then
  begin
    lblArtworkDescription.Visible := True;
    validDataSet := False;
  end
  else
  begin
    lblArtworkDescription.Visible := False;
  end;

  // If the Data is Valid then it will be added to the Database
  if validDataSet then
  begin
    if CopyFile(Pchar(imgPath), PChar(newFullFileName), False) then
    begin
        showMessage('Success');
        with DM do
        begin
          tblArt.Open;
          tblArt.Insert;
          tblArt['artistID'] := SessionId;
          tblArt['artName'] := edtArtworkName.Text;
          tblArt['artDescription'] := memArtworkDescription.Text;
          tblArt['artPath'] := newRelativeFileName;
          tblArt['artWidth'] := edtArtworkWidth.Text;
          tblArt['artLength'] := edtArtworkLength.Text;
          tblArt['artDepth'] := edtArtworkDepth.Text;
          tblArt['artMaterial'] := edtArtworkMaterial.Text;
          tblArt['artPrice'] := edtArtworkPrice.Text;
          tblArt['artCreatedAt']:=DateToStr(dtpCreatedAt.Date);
          tblArt.Post;
        end
    end
    else
    begin
        showMessage('Oops... Looks like we have a problem. Please Try again later');
    end;
  end;
end;

procedure TfrmAddArtwork.btnAddArtworkMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  btnAddArtwork.BevelOuter := bvLowered;
end;

procedure TfrmAddArtwork.btnAddArtworkMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  btnAddArtwork.BevelOuter := bvRaised;
end;

procedure TfrmAddArtwork.btnBackClick(Sender: TObject);
begin
  frmArtistsLanding.Show;
  Hide;
end;

procedure TfrmAddArtwork.btnBackMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  btnBack.BevelOuter := bvLowered;
end;

procedure TfrmAddArtwork.btnBackMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  btnBack.BevelOuter := bvRaised;
end;

procedure TfrmAddArtwork.FormShow(Sender: TObject);
begin
  lblArtworkTitle.Visible := False;
  lblArtworkPrice.Visible := False;
  lblArtworkDate.Visible := False;
  lblArtworkMaterial.Visible := False;
  lblArtworkLength.Visible := False;
  lblArtworkWidth.Visible := False;
  lblArtworkDepth.Visible := False;
  lblArtworkDescription.Visible := False;
  lblArtworkImage.Visible := False;
  imgAddImage.Picture.LoadFromFile('images/UI/picture.png');
  edtArtworkName.Clear;
  edtArtworkPrice.Clear;
  edtArtworkMaterial.Clear;
  edtArtworkLength.Clear;
  edtArtworkDepth.Clear;
  edtArtworkWidth.Clear;
  memArtworkDescription.Clear;
  newRelativeFileName:='';
  newFullFileName:= '';
  imgPath:='';
  imageAdded:=False;
end;

procedure TfrmAddArtwork.imgAddImageClick(Sender: TObject);
var
  RandomDate: TDateTime;

begin
  imgPath := '';
  newFullFilename:='';
  newRelativeFileName:='';
  RandomDate:=Now;
  dlgImg.Options := [ofFileMustExist];

  if dlgImg.Execute then
  begin
    imgPath := dlgImg.FileName;
    newRelativeFileName:='images\Artworks\' + IntToStr(RandomRange(0, 999)) + '_' + IntToStr(YearOf(RandomDate)) + IntToStr(MonthOf(RandomDate)) + IntToStr(DayOf(RandomDate)) + IntToStr(HourOf(RandomDate)) + IntToStr(MinuteOf(RandomDate)) + IntToStr(SecondOf(RandomDate)) + IntToStr(MilliSecondOf(RandomDate)) + '_' + IntToStr(RandomRange(0, 999)) + ExtractFileExt(imgPath);
    newFullFileName:=extractFilePath(Application.Exename) + newRelativeFileName;
    imgAddImage.Picture.LoadFromFile(imgPath);
    imageAdded:=True;
  end
  else
  begin
    imageAdded:=False;
  end;
end;

end.
