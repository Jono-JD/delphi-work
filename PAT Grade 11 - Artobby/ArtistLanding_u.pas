unit ArtistLanding_u;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, jpeg;

type
  TfrmArtistsLanding = class(TForm)
    ScrollBox: TScrollBox;
    btnMyArtwork: TPanel;
    btnAddArtwork: TPanel;
    navSpacer: TPanel;
    btnToggleDropDown: TPanel;
    btnLogout: TPanel;
    lblTitle: TLabel;
    GridPanel: TGridPanel;
    btnPreferences: TPanel;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure FormMouseWheelDown(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure FormMouseWheelUp(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure btnMyArtworkMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure btnMyArtworkMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure btnAddArtworkMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure btnAddArtworkMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure btnLogoutMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure btnLogoutMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure FormShow(Sender: TObject);
    procedure btnLogoutClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnAddArtworkClick(Sender: TObject);
    procedure PopulateArtworkEditor(Sender: TObject);
    procedure btnToggleDropDownClick(Sender: TObject);
    procedure btnPreferencesMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure btnPreferencesMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure btnPreferencesClick(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmArtistsLanding: TfrmArtistsLanding;

implementation
uses
    global_u, login_u, DataModule_u, AddArtwork_u, UpdateArtwork_u, UpdateArtists_u;

{$R *.dfm}

procedure TfrmArtistsLanding.FormShow(Sender: TObject);
var
  objName:string;
  identifier:string;
  numRecords:integer;
  currentRow:integer;
  numRows:integer;
  P:TPanel;
  B:TButton;
  I:TImage;

begin
    btnPreferences.Hide;
    btnToggleDropDown.BevelOuter:=bvRaised;
    btnToggleDropDown.Caption:= SessionName + ' ' + SessionSurname;
    numRecords:=0;
    numRows:=0;
    identifier:='';
    objName:='';

    //  Clear Rows Form Grid Panel for a fresh start
    GridPanel.ControlCollection.Clear;
    GridPanel.RowCollection.Clear;

    // Count Number of Artworks that belongs to specific artist
    with DM do
    begin
        tblArt.Open;
        tblArt.First;
        while not tblArt.Eof do
        begin
            if tblArt['artistId'] = SessionId then
            begin
                inc(numRecords);
            end;
            tblArt.Next;
        end;

        // Calculate number of rows needed in Grid Panel to accomodate all artworks
        numRows:=numRecords div 4;
        if not(numRecords mod 4 = 0) then   // Adds an extra row if needed
        begin
            inc(numRows);
        end;

        // Initialises each row with specific properties
        for currentRow := 0 to numRows-1 do
        begin
            GridPanel.RowCollection.Add;
            GridPanel.RowCollection[currentRow].SizeStyle:=ssAbsolute;
            GridPanel.RowCollection[currentRow].Value:=300;
        end;

        // Sets the height for the grid panel and the scroll view
        GridPanel.Height:=300*numRows;
        ScrollBox.VertScrollBar.Range:=300*numRows;

//        showMessage(intTOStr(numRecords));
//        showMessage(intToStr(numRecords div 4));
//        showMessage(intTOStr(numRecords mod 4));

        tblArt.First;
        while not tblArt.Eof do
        begin
            if tblArt['artistId'] = SessionId then
            begin
                identifier:=tblArt['ID'];
                objName:='pnl'+identifier;
                P:=TPanel.Create(Self);
                P.Width:=200;
                P.Height:=250;
                P.ParentColor:=False;
                P.ParentBackground:=False;
                P.Color:=clBlack;
                P.Name:=objName;
                P.Parent:=GridPanel;

                objName:='btn'+identifier;
                B:=TButton.Create(self);
                B.Parent:=P;
                B.Width:=150;
                B.Height:=35;
                B.Left:=25;
                B.Top:=192;
                B.Name:=objName;
                B.Caption:='Edit Artwork';
                B.OnClick:=PopulateArtworkEditor;

                objName:='img'+identifier;
                I:=TImage.Create(self);
                I.Parent:=P;
                I.Width:=150;
                I.Height:=150;
                I.Left:=25;
                I.Top:=25;
                I.Name:=objName;
                I.Picture.LoadFromFile(tblArt['artPath']);
                I.Stretch:=True;
            end;
            tblArt.Next;
        end;
        tblArt.Close;
    end;
//    showMessage(intToStr(GridPanel.ControlCount));
//    GridPanel.ControlCollection.Clear;
//    GridPanel.RowCollection.Clear;
end;

procedure TfrmArtistsLanding.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    Application.Terminate;
end;

procedure TfrmArtistsLanding.FormMouseWheelDown(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
    ScrollBox.VertScrollBar.Position :=  ScrollBox.VertScrollBar.Position + 12;
end;

procedure TfrmArtistsLanding.FormMouseWheelUp(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
    ScrollBox.VertScrollBar.Position :=  ScrollBox.VertScrollBar.Position - 12;
end;

procedure TfrmArtistsLanding.PopulateArtworkEditor(Sender: TObject);
begin
    with Sender as TButton do
    begin
        //showMessage(name);
        ArtworkID:=copy(name, 4, length(name));
        with DM do
        begin
          tblArt.Open;
          tblArt.First;
          while not tblArt.Eof do
          begin
              if tblArt['ID'] = ArtworkID then
              begin
                  ArtworkName:=tblArt['artName'];
                  ArtworkDescription:=tblArt['artDescription'];
                  ArtworkWidth:=tblArt['artWidth'];
                  ArtworkLength:=tblArt['artLength'];
                  ArtworkDepth:=tblArt['artDepth'];
                  ArtworkMaterial:=tblArt['artMaterial'];
                  ArtworkPrice:=tblArt['artPrice'];
                  ArtworkPath:=tblArt['artPath'];
                  ArtworkCreatedAt:=tblArt['artCreatedAt'];
              end;
              tblArt.Next;
          end;
          tblArt.Close;
        end;
        frmUpdateArtwork.show;
        frmArtistsLanding.Hide;

    end;
end;

procedure TfrmArtistsLanding.btnAddArtworkClick(Sender: TObject);
begin
    frmAddArtwork.Show;
    hide;
end;

procedure TfrmArtistsLanding.btnAddArtworkMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
    btnAddArtwork.BevelOuter:=bvLowered;
end;

procedure TfrmArtistsLanding.btnAddArtworkMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
    btnAddArtwork.BevelOuter:=bvRaised;
end;

procedure TfrmArtistsLanding.btnLogoutClick(Sender: TObject);
begin
    global_u.logout;
    frmLogin.Show;
    hide;
end;

procedure TfrmArtistsLanding.btnLogoutMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
    btnLogout.BevelOuter:=bvLowered;
end;

procedure TfrmArtistsLanding.btnLogoutMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
    btnLogout.BevelOuter:=bvRaised;
end;

procedure TfrmArtistsLanding.btnMyArtworkMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
    btnMyArtwork.BevelOuter:=bvLowered;
end;

procedure TfrmArtistsLanding.btnMyArtworkMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
    btnMyArtwork.BevelOuter:=bvRaised;
end;

procedure TfrmArtistsLanding.btnPreferencesClick(Sender: TObject);
begin
    frmUpdateArtist.show;
    self.Hide;
end;

procedure TfrmArtistsLanding.btnPreferencesMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
    btnPreferences.BevelOuter:=bvLowered;
end;

procedure TfrmArtistsLanding.btnPreferencesMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
    btnPreferences.BevelOuter:=bvRaised;
end;

procedure TfrmArtistsLanding.btnToggleDropDownClick(Sender: TObject);
begin
    if btnPreferences.Visible then
    begin
        btnPreferences.Hide;
        btnToggleDropDown.BevelOuter:=bvRaised;
    end
    else
    begin
        btnPreferences.Show;
        btnToggleDropDown.BevelOuter:=bvLowered;
    end;
end;

procedure TfrmArtistsLanding.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
  Params.ExStyle := Params.ExStyle or WS_EX_APPWINDOW;
  Params.WndParent := GetDesktopWindow;
end;

end.
