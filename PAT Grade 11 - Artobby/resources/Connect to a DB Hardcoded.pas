PROCEDURE TForm1.FormActivate(Sender: TObject);
var
path : string;
BEGIN
	path := extractFilePath(Application.Exename); //this ensures that the db file in the folder is seen
	WITH DM DO //everything here happens with the Datamodule
	BEGIN
	connDB.close; //make sure the database is closed before making changes
	connDB.ConnectionString := 'Provider=Microsoft.Jet.OLEDB.4.0; Data Source= '+path+'myDatabase.mdb; Persist Security info=false';
	connDB.LoginPrompt := FALSE;
	connDB.open; //open the database again
	tblMyTable.connection := connDB; //connects ADOtable to connDB
	tblMyTable.TableName := 'MyTable'; //connects to the table in database
	tblMyTable.active := TRUE; //open the connection to the table
	dsMyTable.Dataset := tblMyTable; //links datasource to the table
	dbgMyData.DataSource := dsMyTable;
END;