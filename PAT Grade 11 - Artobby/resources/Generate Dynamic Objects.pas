procedure TForm1.myClick(Sender: TObject);
begin
  with Sender as TPanel do
    Self.Caption := ClassName + ' ' + Name;
end;

procedure TForm1.btnGenerateClick(Sender: TObject);
begin
    with TPanel.Create(self) do
    begin
      Left := 380;
      Top := 8 + i;
      Width := 120;
      Height := 40;
      Name := 'ThisButton' + IntToStr(i);
      Caption := 'There' + IntToStr(i);
      OnClick :=  MyClick;  { a procedure I defined somewhere else }
      Parent := Form1;
    end; {end with}
  inc(i, 40);
end;