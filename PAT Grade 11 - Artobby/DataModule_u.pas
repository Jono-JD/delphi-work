unit DataModule_u;

interface

uses
  SysUtils, Classes, DB, ADODB;

type
  TDM = class(TDataModule)
    connArtobby: TADOConnection;
    tblArt: TADOTable;
    tblCart: TADOTable;
    tblCountry: TADOTable;
    tblUser: TADOTable;
    dsArt: TDataSource;
    dsCart: TDataSource;
    dsCountry: TDataSource;
    dsUser: TDataSource;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DM: TDM;

implementation

{$R *.dfm}

end.
