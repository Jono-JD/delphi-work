unit SignUp_u;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, pngimage, ExtCtrls;

type
  TfrmSignUp = class(TForm)
    imgDisplay: TImage;
    edtName: TEdit;
    edtSurname: TEdit;
    edtEmail: TEdit;
    edtPassword: TEdit;
    edtPasswordConfirm: TEdit;
    edtContact: TEdit;
    cmbUserType: TComboBox;
    cmbCountry: TComboBox;
    btnSignUp: TButton;
    lblName: TLabel;
    lblSurname: TLabel;
    lblContact: TLabel;
    lblUserType: TLabel;
    lblCountry: TLabel;
    lblEmail: TLabel;
    lblPassword: TLabel;
    lblPasswordConfirm: TLabel;
    btnBack: TButton;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure btnBackClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnSignUpClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmSignUp: TfrmSignUp;

implementation
uses
    Login_u, DataModule_u;

var
    ValidInput:Boolean;

{$R *.dfm}

procedure TfrmSignUp.btnBackClick(Sender: TObject);
begin
    frmLogin.Show;
    hide;
end;

procedure TfrmSignUp.btnSignUpClick(Sender: TObject);
var
  i: Integer;
  validEmail:boolean;
  uniqueEmail:boolean;

begin
    ValidInput:=True;
    validEmail:=False;
    uniqueEmail:=True;

    // Name Field Required
    if edtName.Text = '' then
    begin
        lblName.Caption:='*Required ';
        lblName.Visible:=True;
        ValidInput:=False;
    end
    else
    begin
        lblName.Visible:=False;

    end;

    // Surname Field Required
    if edtSurname.Text = '' then
    begin
        lblSurname.Caption:='*Required ';
        lblSurname.Visible:=True;
        ValidInput:=False;
    end
    else
    begin
        lblSurname.Visible:=False;

    end;

    // Contact Field Requried
    if edtContact.Text = '' then
    begin
        lblContact.Caption:='*Required ';
        lblContact.Visible:=True;
        ValidInput:=False;
    end
    else
    begin
        lblContact.Visible:=False;

    end;

    // UserType Field Required
    if cmbUserType.Text = '' then
    begin
        lblUserType.Caption:='*Required ';
        lblUserType.Visible:=True;
        ValidInput:=False;
    end
    else
    begin
        lblUserType.Visible:=False;

    end;

    // Country Field Required
    if cmbCountry.Text = '' then
    begin
        lblCountry.Caption:='*Required ';
        lblCountry.Visible:=True;
        ValidInput:=False;
    end
    else
    begin
        lblCountry.Visible:=False;
    end;

    // Email Field Required
    if edtEmail.Text = '' then
    begin
        lblEmail.Caption:='*Required ';
        lblEmail.Visible:=True;
        ValidInput:=False;
    end
    else
    begin
        for i := 1 to length(edtEmail.Text) do
        begin
            if edtEmail.Text[i] = '@' then
            begin
                validEmail:=True;
            end;
        end;

        if validEmail then
        begin
            lblEmail.Visible:=False;
            with DM do
            begin
                tblUser.Open;
                tblUser.First;
                while not tblUser.Eof do
                begin
                    if edtEmail.Text = tblUser['userEmail'] then
                    begin
                        //showmessage(tblUser['userEmail']);
                        uniqueEmail:=False;
                    end;
                    tblUser.Next;
                end;
                tblUser.Close;

                if not uniqueEmail then
                begin
                    lblEmail.Caption:='*This Email is Already in use ';
                    lblEmail.Visible:=True;
                    validInput:=False;
                end;
            end;
        end
        else
        begin
            lblEmail.Caption:='*Invalid Email ';
            lblEmail.Visible:=True;
            ValidInput:=False;
        end;
    end;

    // Passowrd Field Required
    if edtPassword.Text = '' then
    begin
        lblPassword.Caption:='*Required ';
        lblPassword.Visible:=True;
        ValidInput:=False;
    end
    else
    begin

        lblPassword.Visible:=False;

    end;

    // Passowrd Confirm Field Required
    if edtPasswordConfirm.Text = '' then
    begin
        lblPasswordConfirm.Caption:='*Required ';
        lblPasswordConfirm.Visible:=True;
        ValidInput:=False;
    end
    else
    begin
        lblPasswordConfirm.Visible:=False;
    end;

    // Checks if Password in both Fields match
    if not(edtPassword.Text = '') and not(edtPasswordConfirm.Text = '') then
    begin
      if not(edtPassword.Text = edtPasswordConfirm.Text) then
      begin
         lblPassword.Caption:='*The Passwords do not match';
         lblPassword.Visible:=True;
         validInput:=False;
      end;
    end;

    // Checks to see if all inputs are valid
    if validInput then
    begin
        showMessage('We are a GO!!');
        with DM do
        begin
            tblUser.Open;
            tblUser.Insert;
            tblUser.FieldByName('userName').AsString:=edtName.Text;
            tblUser.FieldByName('userSurname').AsString:=edtSurname.Text;
            tblUser.FieldByName('userEmail').AsString:=edtEmail.Text;
            tblUser.FieldByName('userContact').AsString:=edtContact.Text;
            tblUser.FieldByName('userPassword').AsString:=edtPassword.Text;
            tblUser.FieldByName('userCountry').AsString:=cmbCountry.Text;
            tblUser.FieldByName('userType').AsString:=cmbUserType.Text;
            tblUser.Post;
            tblUser.Refresh;
            tblUser.Close;
        end;
        frmLogin.Show;
        hide;
    end;
end;

procedure TfrmSignUp.CreateParams(var Params: TCreateParams);
begin
    inherited CreateParams(Params);
    Params.ExStyle := Params.ExStyle or WS_EX_APPWINDOW;
    Params.WndParent := GetDesktopWindow;
end;

procedure TfrmSignUp.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    Application.Terminate;
end;

procedure TfrmSignUp.FormShow(Sender: TObject);
begin
    lblName.Visible:=False;
    lblSurname.Visible:=False;
    lblContact.Visible:=False;
    lblUserType.Visible:=False;
    lblCountry.Visible:=False;
    lblEmail.Visible:=False;
    lblPassword.Visible:=False;
    lblPasswordConfirm.Visible:=False;

    edtName.Clear;
    edtSurname.Clear;
    edtContact.Clear;
    cmbUserType.Clear;
    cmbCountry.Clear;
    edtEmail.Clear;
    edtPassword.Clear;
    edtPasswordConfirm.Clear;

    cmbCountry.Items.Clear;
    with DM do
    begin
        tblCountry.Open;
        tblCountry.First;
        while not tblCountry.Eof do
        begin
            cmbCountry.Items.Add(tblCountry['Country']);
            tblCountry.Next;
        end;
        tblCountry.Close;
    end;

    cmbUserType.Items.Clear;
    cmbUserType.Items.Add('Viewer');
    cmbUserType.Items.Add('Artist');
end;

end.
