program Artobby;

uses
  Forms,
  Login_u in 'Login_u.pas' {frmLogin},
  SignUp_u in 'SignUp_u.pas' {frmSignUp},
  DataModule_u in 'DataModule_u.pas' {DM: TDataModule},
  ArtistLanding_u in 'ArtistLanding_u.pas' {frmArtistsLanding},
  Global_u in 'Global_u.pas',
  ArtistsGallery_u in 'ArtistsGallery_u.pas' {frmArtistsGallery},
  AddArtwork_u in 'AddArtwork_u.pas' {frmAddArtwork},
  UpdateArtwork_u in 'UpdateArtwork_u.pas' {frmUpdateArtwork},
  ArtworksGallery_u in 'ArtworksGallery_u.pas' {frmArtworksGallery},
  ArtistDetail_u in 'ArtistDetail_u.pas' {frmArtistDetail},
  ArtworkDetail_u in 'ArtworkDetail_u.pas' {frmArtworkDetail},
  Cart_u in 'Cart_u.pas' {frmCart},
  UpdateArtists_u in 'UpdateArtists_u.pas' {frmUpdateArtist},
  UpdateViewer_u in 'UpdateViewer_u.pas' {frmUpdateViewer},
  DebugForm_u in 'DebugForm_u.pas' {Form1};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.Title := 'Artobby';
  Application.CreateForm(TfrmLogin, frmLogin);
  Application.CreateForm(TfrmSignUp, frmSignUp);
  Application.CreateForm(TDM, DM);
  Application.CreateForm(TfrmArtistsLanding, frmArtistsLanding);
  Application.CreateForm(TfrmArtistsGallery, frmArtistsGallery);
  Application.CreateForm(TfrmAddArtwork, frmAddArtwork);
  Application.CreateForm(TfrmUpdateArtwork, frmUpdateArtwork);
  Application.CreateForm(TfrmArtworksGallery, frmArtworksGallery);
  Application.CreateForm(TfrmArtworksGallery, frmArtworksGallery);
  Application.CreateForm(TfrmArtistDetail, frmArtistDetail);
  Application.CreateForm(TfrmArtworkDetail, frmArtworkDetail);
  Application.CreateForm(TfrmCart, frmCart);
  Application.CreateForm(TfrmUpdateArtist, frmUpdateArtist);
  Application.CreateForm(TfrmUpdateViewer, frmUpdateViewer);
  Application.CreateForm(TForm1, Form1);
  Application.Run;
  //Application.Run;
end.
