unit ArtworksGallery_u;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, jpeg, ComCtrls, Spin;

type
  TfrmArtworksGallery = class(TForm)
    btnViewArtists: TPanel;
    btnViewArtworks: TPanel;
    navSpacer: TPanel;
    btnToggleDropDown: TPanel;
    btnLogout: TPanel;
    lblTitle: TLabel;
    pnlFilter: TPanel;
    btnFilterPrice: TPanel;
    btnFilterArtist: TPanel;
    btnFilterTitle: TPanel;
    btnFilterCreation: TPanel;
    ScrollBox: TScrollBox;
    GridPanel: TGridPanel;
    lblFilter: TLabel;
    edtTitleSearch: TEdit;
    btnFilterNone: TPanel;
    Panel1: TPanel;
    Image1: TImage;
    Button1: TButton;
    Button2: TButton;
    Panel2: TPanel;
    Image2: TImage;
    Button3: TButton;
    Button4: TButton;
    btnPreferences: TPanel;
    btnCart: TPanel;
    dtpSearch: TDateTimePicker;
    btnFilterCountry: TPanel;
    cmbCountry: TComboBox;
    edtArtistSearch: TEdit;
    seditMinPrice: TSpinEdit;
    seditMaxPrice: TSpinEdit;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure btnViewArtistsMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure btnViewArtistsMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure btnViewArtworksMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure btnViewArtworksMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure btnLogoutMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure btnLogoutMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure clearGallery();
    procedure showAllArtwork();
    procedure resetFilterBevel();
    procedure AddArtToCart(Sender: TObject);
    procedure RouteToCart(Sender: TObject);
    procedure populateArtworkDetail(Sender: TObject);
    procedure btnFilterTitleClick(Sender: TObject);
    procedure btnFilterArtistClick(Sender: TObject);
    procedure btnFilterPriceClick(Sender: TObject);
    procedure btnFilterCountryClick(Sender: TObject);
    procedure btnFilterCreationClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormMouseWheelUp(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure FormMouseWheelDown(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure btnFilterNoneClick(Sender: TObject);
    procedure btnViewArtistsClick(Sender: TObject);
    procedure btnPreferencesMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure btnPreferencesMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure btnCartMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure btnCartMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure btnToggleDropDownClick(Sender: TObject);
    procedure btnCartClick(Sender: TObject);
    procedure btnPreferencesClick(Sender: TObject);
    procedure btnLogoutClick(Sender: TObject);
    procedure edtTitleSearchChange(Sender: TObject);
    procedure dtpSearchChange(Sender: TObject);
    procedure cmbCountryChange(Sender: TObject);
    procedure edtArtistSearchChange(Sender: TObject);
    procedure seditMinPriceChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmArtworksGallery: TfrmArtworksGallery;
  filter:string;

implementation
uses
    Global_u, DataModule_u, Login_u, ArtistsGallery_u, ArtworkDetail_u, Cart_u, UpdateViewer_u;

{$R *.dfm}

procedure TfrmArtworksGallery.AddArtToCart(Sender: TObject);
var
    artworkID:string;

begin
    with Sender as TButton do
    begin
        artworkID:=copy(name, 8, length(name));
        //showMessage(ArtworkID);
        with DM do
        begin
            tblCart.Open;
            tblCart.First;
            tblCart.Insert;
            tblCart['userID']:=SessionID;
            tblCart['artId']:=artworkID;
            tblCart.Post;
            tblCart.Close;
        end;
        Caption:='View in Cart';
        onClick:=RouteToCart;
    end;
end;

procedure TfrmArtworksGallery.btnCartClick(Sender: TObject);
begin
    frmCart.show;
    self.hide;
end;

procedure TfrmArtworksGallery.btnCartMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
    btnCart.BevelOuter:=bvLowered;
end;

procedure TfrmArtworksGallery.btnCartMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
    btnCart.BevelOuter:=bvRaised;
end;

procedure TfrmArtworksGallery.btnFilterArtistClick(Sender: TObject);
begin
    resetFilterBevel;
    btnFilterArtist.BevelOuter:=bvLowered;
    btnFilterArtist.Color:=clGray;
    lblTitle.Caption:='Artworks Filtered by Artist ';
    edtTitleSearch.hide;
    edtArtistSearch.Show;
    cmbCOuntry.Hide;
    dtpSearch.Hide;
end;

procedure TfrmArtworksGallery.btnFilterCountryClick(Sender: TObject);
begin
    resetFilterBevel;
    btnFilterCountry.BevelOuter:=bvLowered;
    btnFilterCountry.Color:=clGray;
    lblTitle.Caption:='Artworks Filtered by Country ';
    edtTItleSearch.Hide;
    cmbCOuntry.show;
    dtpSearch.Hide;
    edtArtistSearch.Hide;
    seditMinPrice.hide;
    seditMaxPrice.hide;
end;

procedure TfrmArtworksGallery.btnFilterCreationClick(Sender: TObject);
begin
    resetFilterBevel;
    btnFilterCreation.BevelOuter:=bvLowered;
    btnFilterCreation.Color:=clGray;
    lblTitle.Caption:='Artworks Filtered by Date of Creation ';
    edtTitleSearch.Hide;
    dtpSearch.Show;
    cmbCountry.hide;
    edtArtistSearch.Hide;
    seditMinPrice.hide;
    seditMaxPrice.hide;
end;

procedure TfrmArtworksGallery.btnFilterNoneClick(Sender: TObject);
begin
    resetFilterBevel;
    btnFilterNone.BevelOuter:=bvLowered;
    btnFilterNone.Color:=clGray;
    lblTitle.Caption:='All Artworks ';
    edtTitleSearch.Hide;
    edtArtistSearch.Hide;
    cmbCOuntry.Hide;
    dtpSearch.Hide;
    seditMinPrice.hide;
    seditMaxPrice.hide;
    edtTitleSearch.Text:='';
    edtArtistSearch.Text:='';
    cmbCountry.Text:='';
    showAllArtwork;
end;

procedure TfrmArtworksGallery.btnFilterPriceClick(Sender: TObject);
begin
    resetFilterBevel;
    btnFilterPrice.BevelOuter:=bvLowered;
    btnFilterPrice.Color:=clGray;
    lblTitle.Caption:='Arworks Filtered by Price Range';
    edtTitleSearch.hide;
    edtArtistSearch.Hide;
    cmbCountry.Hide;
    dtpSearch.Hide;
    seditMinPrice.Show;
    seditMaxPrice.Show;
end;

procedure TfrmArtworksGallery.btnFilterTitleClick(Sender: TObject);
begin
    resetFilterBevel;
    btnFilterTitle.BevelOuter:=bvLowered;
    btnFilterTitle.Color:=clGray;
    lblTitle.Caption:='Artworks Filtered by Name ';
    edtTitleSearch.Show;
    dtpSearch.Hide;
    cmbCountry.hide;
    edtArtistSearch.hide;
    seditMinPrice.hide;
    seditMaxPrice.hide;
end;

procedure TfrmArtworksGallery.btnLogoutClick(Sender: TObject);
begin
    global_u.logout;
    frmLogin.Show;
    self.Hide;
end;

procedure TfrmArtworksGallery.btnLogoutMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
    btnLogout.BevelOuter:=bvLowered;
end;

procedure TfrmArtworksGallery.btnLogoutMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
    btnLogout.BevelOuter:=bvRaised;
end;

procedure TfrmArtworksGallery.btnPreferencesClick(Sender: TObject);
begin
    frmUpdateViewer.show;
    self.hide;
end;

procedure TfrmArtworksGallery.btnPreferencesMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
    btnPreferences.BevelOuter:=bvLowered;
end;

procedure TfrmArtworksGallery.btnPreferencesMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
    btnPreferences.BevelOuter:=bvRaised;
end;

procedure TfrmArtworksGallery.btnToggleDropDownClick(Sender: TObject);
begin
    if btnPreferences.Visible then
    begin
        btnPreferences.Hide;
        btnCart.Hide;
        btnToggleDropDown.BevelOuter:=bvRaised;
    end
    else
    begin
        btnPreferences.Show;
        btnCart.show;
        btnToggleDropDown.BevelOuter:=bvLowered;
    end;
end;

procedure TfrmArtworksGallery.btnViewArtistsClick(Sender: TObject);
begin
    frmArtistsGallery.Show;
    self.Hide;
end;

procedure TfrmArtworksGallery.btnViewArtistsMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
    btnViewArtists.BevelOuter:=bvLowered;
end;

procedure TfrmArtworksGallery.btnViewArtistsMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
    btnViewArtists.BevelOuter:=bvRaised;
end;

procedure TfrmArtworksGallery.btnViewArtworksMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
    btnViewArtworks.BevelOuter:=bvLowered;
end;

procedure TfrmArtworksGallery.btnViewArtworksMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
    btnViewArtworks.BevelOuter:=bvRaised;
end;

procedure TfrmArtworksGallery.clearGallery;
begin
    GridPanel.ControlCollection.Clear;
    GridPanel.RowCollection.Clear;
end;



procedure TfrmArtworksGallery.cmbCountryChange(Sender: TObject);
var
  numRecords:integer;
  numRows:integer;
  currentRow:integer;
  identifier:string;
  objName:string;
  BView:TButton;
  BCart:TButton;
  I:TImage;
  P:TPanel;
  notInCart:boolean;

begin
    numRecords:=0;
    numRows:=0;
    identifier:='';
    objName:='';

    with DM do
    begin
        tblUser.Open;
        tblUser.First;
        tblUser.Filtered:=False;
        tblUser.Filter:='[userCountry] = '+QuotedStr(cmbCountry.Text)+'AND [userType] = '+QuotedStr('Artist');
        tblUser.Filtered:=True;
        while not tblUser.Eof do
        begin
          tblArt.Open;
          tblArt.First;
          tblArt.Filtered:=False;
          tblArt.Filter:='[artistID] = '+QuotedStr(tblUser['ID']);
          tblArt.Filtered:=True;
          numRecords:=numRecords+tblArt.RecordCount;
          tblUser.Next;
        end;
        tblUser.Close;
        clearGallery;
        numRows:=numRecords div 3;
        if not(numRecords mod 3 = 0) then
        begin
            inc(numRows);
        end;

        for currentRow := 0 to numRows-1 do
        begin
            GridPanel.RowCollection.Add;
            GridPanel.RowCollection[currentRow].SizeStyle:=ssAbsolute;
            GridPanel.RowCollection[currentRow].Value:=350;
        end;

        // Sets the height for the grid panel and the scroll view
        GridPanel.Height:=350*numRows;
        ScrollBox.VertScrollBar.Range:=350*numRows;

        tblUser.Filtered:=False;
        tblArt.Filtered:=False;

        tblUser.Open;
        tblUser.First;
        tblUser.Filtered:=False;
        tblUser.Filter:='[userCountry] = '+QuotedStr(cmbCountry.Text)+'AND [userType] = '+QuotedStr('Artist');
        tblUser.Filtered:=True;
        while not tblUser.Eof do
        begin
          tblArt.Open;
          tblArt.First;
          tblArt.Filtered:=False;
          tblArt.Filter:='[artistID] = '+QuotedStr(tblUser['ID']);
          tblArt.Filtered:=True;
          tblArt.First;
          while not tblArt.Eof do
          begin
              identifier:=tblArt['ID'];
              objName:='pnl'+identifier;
              P:=TPanel.Create(Self);
              P.Width:=220;
              P.Height:=300;
              P.ParentColor:=False;
              P.ParentBackground:=False;
              P.Color:=clBlack;
              P.Name:=objName;
              P.Parent:=GridPanel;

              objName:='img'+identifier;
              I:=TImage.Create(self);
              I.Parent:=P;
              I.Width:=180;
              I.Height:=180;
              I.Left:=20;
              I.Top:=20;
              I.Name:=objName;
              I.Picture.LoadFromFile(tblArt['artPath']);
              I.Stretch:=True;

              objName:='btnView'+identifier;
              BView:=TButton.Create(self);
              BView.Parent:=P;
              BView.Width:=180;
              BView.Height:=32;
              BView.Left:=20;
              BView.Top:=210;
              BView.Name:=objName;
              BView.Caption:='View Details ';
              BView.OnClick := PopulateArtworkDetail;

              objName:='btnCart'+identifier;
              BCart:=TButton.Create(self);
              BCart.Parent:=P;
              BCart.Width:=180;
              BCart.Height:=32;
              BCart.Left:=20;
              BCart.Top:=252;
              BCart.Name:=objName;

              tblCart.Open;
              tblCart.First;
              notInCart:=True;
              while not tblCart.Eof do
              begin
                  //showMessage(SessionID);
                  if tblCart['userID'] = SessionId then
                  begin
                      if tblCart['artID'] = identifier then
                      begin
                          notInCart:=False;
                          //showMessage('');
                      end;
                  end;
                  tblCart.Next;
              end;
              tblCart.Close;
              if notInCart then
              begin
                   BCart.Caption:='Add to Cart ';
                   BCart.OnClick:=AddArtToCart;
              end
              else
              begin
                  BCart.Caption:='View in Cart';
                  BCart.OnClick:=RouteToCart;
              end;
              tblArt.Next
          end;
          tblArt.Close;
          tblUser.Next;
        end;
        tblUser.Close;
    end;
end;

procedure TfrmArtworksGallery.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
    Params.ExStyle := Params.ExStyle or WS_EX_APPWINDOW;
    Params.WndParent := GetDesktopWindow;
end;

procedure TfrmArtworksGallery.dtpSearchChange(Sender: TObject);
var
  numRecords:integer;
  numRows:integer;
  currentRow:integer;
  identifier:string;
  objName:string;
  BView:TButton;
  BCart:TButton;
  I:TImage;
  P:TPanel;
  notInCart:boolean;

begin
    numRecords:=0;
    numRows:=0;
    identifier:='';
    objName:='';

    with DM do
    begin
        tblArt.Open;
        tblArt.First;
        tblArt.Filtered:=False;
        tblArt.Filter:='[artCreatedAt] = '+QuotedStr(DateToStr(dtpSearch.Date));
        tblArt.Filtered:=True;
        clearGallery;
        numRecords:=tblArt.RecordCount;
        numRows:=numRecords div 3;
        if not(numRecords mod 3 = 0) then
        begin
            inc(numRows);
        end;

        for currentRow := 0 to numRows-1 do
        begin
            GridPanel.RowCollection.Add;
            GridPanel.RowCollection[currentRow].SizeStyle:=ssAbsolute;
            GridPanel.RowCollection[currentRow].Value:=350;
        end;

        // Sets the height for the grid panel and the scroll view
        GridPanel.Height:=350*numRows;
        ScrollBox.VertScrollBar.Range:=350*numRows;


        tblArt.First;
        while not tblArt.Eof do
        begin
            identifier:=tblArt['ID'];
            objName:='pnl'+identifier;
            P:=TPanel.Create(Self);
            P.Width:=220;
            P.Height:=300;
            P.ParentColor:=False;
            P.ParentBackground:=False;
            P.Color:=clBlack;
            P.Name:=objName;
            P.Parent:=GridPanel;

            objName:='img'+identifier;
            I:=TImage.Create(self);
            I.Parent:=P;
            I.Width:=180;
            I.Height:=180;
            I.Left:=20;
            I.Top:=20;
            I.Name:=objName;
            I.Picture.LoadFromFile(tblArt['artPath']);
            I.Stretch:=True;

            objName:='btnView'+identifier;
            BView:=TButton.Create(self);
            BView.Parent:=P;
            BView.Width:=180;
            BView.Height:=32;
            BView.Left:=20;
            BView.Top:=210;
            BView.Name:=objName;
            BView.Caption:='View Details ';
            BView.OnClick := PopulateArtworkDetail;

            objName:='btnCart'+identifier;
            BCart:=TButton.Create(self);
            BCart.Parent:=P;
            BCart.Width:=180;
            BCart.Height:=32;
            BCart.Left:=20;
            BCart.Top:=252;
            BCart.Name:=objName;

            tblCart.Open;
            tblCart.First;
            notInCart:=True;
            while not tblCart.Eof do
            begin
                //showMessage(SessionID);
                if tblCart['userID'] = SessionId then
                begin
                    if tblCart['artID'] = identifier then
                    begin
                        notInCart:=False;
                        //showMessage('');
                    end;
                end;
                tblCart.Next;
            end;
            tblCart.Close;
            if notInCart then
            begin
                 BCart.Caption:='Add to Cart ';
                 BCart.OnClick:=AddArtToCart;
            end
            else
            begin
                BCart.Caption:='View in Cart';
                BCart.OnClick:=RouteToCart;
            end;


            tblArt.Next
        end;
        tblArt.Close;
    end;
end;


procedure TfrmArtworksGallery.edtArtistSearchChange(Sender: TObject);
var
  numRecords:integer;
  numRows:integer;
  currentRow:integer;
  identifier:string;
  objName:string;
  BView:TButton;
  BCart:TButton;
  I:TImage;
  P:TPanel;
  notInCart:boolean;

begin
    numRecords:=0;
    numRows:=0;
    identifier:='';
    objName:='';

    if not(edtArtistSearch.Text = '') then
    begin
        with DM do
        begin
            tblUser.Open;
            tblUser.First;
            tblUser.Filtered:=False;
            tblUser.Filter:='[userName] like '+QuotedStr(edtArtistSearch.Text+'%')+'AND [userType] = '+QuotedStr('Artist');
            tblUser.Filtered:=True;
            while not tblUser.Eof do
            begin
                tblArt.Open;
                tblArt.First;
                tblArt.Filtered:=False;
                tblArt.Filter:='[artistID] = '+QuotedStr(tblUser['ID']);
                tblArt.Filtered:=True;
                while not tblArt.Eof do
                begin
                  numRecords:=numRecords+tblArt.RecordCount;
                  tblArt.Next;
                end;
                tblArt.Close;
                tblUser.Next;
            end;
            tblUser.Close;

            clearGallery;
            numRows:=numRecords div 3;
            if not(numRecords mod 3 = 0) then
            begin
                inc(numRows);
            end;

            for currentRow := 0 to numRows-1 do
            begin
                GridPanel.RowCollection.Add;
                GridPanel.RowCollection[currentRow].SizeStyle:=ssAbsolute;
                GridPanel.RowCollection[currentRow].Value:=350;
            end;

            // Sets the height for the grid panel and the scroll view
            GridPanel.Height:=350*numRows;
            ScrollBox.VertScrollBar.Range:=350*numRows;

            tblUser.Filtered:=False;
            tblArt.Filtered:=False;
            tblUser.Open;
            tblUser.First;
            tblUser.Filtered:=False;
            tblUser.Filter:='[userName] like '+QuotedStr(edtArtistSearch.Text+'%')+'AND [userType] = '+QuotedStr('Artist');
            tblUser.Filtered:=True;
            while not tblUser.Eof do
            begin
                tblArt.Open;
                tblArt.First;
                tblArt.Filtered:=False;
                tblArt.Filter:='[artistID] = '+QuotedStr(tblUser['ID']);
                tblArt.Filtered:=True;
                while not tblArt.Eof do
                begin
                    identifier:=tblArt['ID'];
                    objName:='pnl'+identifier;
                    P:=TPanel.Create(Self);
                    P.Width:=220;
                    P.Height:=300;
                    P.ParentColor:=False;
                    P.ParentBackground:=False;
                    P.Color:=clBlack;
                    P.Name:=objName;
                    P.Parent:=GridPanel;

                    objName:='img'+identifier;
                    I:=TImage.Create(self);
                    I.Parent:=P;
                    I.Width:=180;
                    I.Height:=180;
                    I.Left:=20;
                    I.Top:=20;
                    I.Name:=objName;
                    I.Picture.LoadFromFile(tblArt['artPath']);
                    I.Stretch:=True;

                    objName:='btnView'+identifier;
                    BView:=TButton.Create(self);
                    BView.Parent:=P;
                    BView.Width:=180;
                    BView.Height:=32;
                    BView.Left:=20;
                    BView.Top:=210;
                    BView.Name:=objName;
                    BView.Caption:='View Details ';
                    BView.OnClick := PopulateArtworkDetail;

                    objName:='btnCart'+identifier;
                    BCart:=TButton.Create(self);
                    BCart.Parent:=P;
                    BCart.Width:=180;
                    BCart.Height:=32;
                    BCart.Left:=20;
                    BCart.Top:=252;
                    BCart.Name:=objName;

                    tblCart.Open;
                    tblCart.First;
                    notInCart:=True;
                    while not tblCart.Eof do
                    begin
                        //showMessage(SessionID);
                        if tblCart['userID'] = SessionId then
                        begin
                            if tblCart['artID'] = identifier then
                            begin
                                notInCart:=False;
                                //showMessage('');
                            end;
                        end;
                        tblCart.Next;
                    end;
                    tblCart.Close;
                    if notInCart then
                    begin
                         BCart.Caption:='Add to Cart ';
                         BCart.OnClick:=AddArtToCart;
                    end
                    else
                    begin
                        BCart.Caption:='View in Cart';
                        BCart.OnClick:=RouteToCart;
                    end;
                    tblArt.Next
                end;
                tblArt.Close;
                tblUser.Next;
            end;
            tblUser.Close;
        end;
    end
    else
    begin
      showAllArtwork;
    end;
end;

procedure TfrmArtworksGallery.edtTitleSearchChange(Sender: TObject);
var
  numRecords:integer;
  numRows:integer;
  currentRow:integer;
  identifier:string;
  objName:string;
  BView:TButton;
  BCart:TButton;
  I:TImage;
  P:TPanel;
  notInCart:boolean;

begin
    numRecords:=0;
    numRows:=0;
    identifier:='';
    objName:='';

    if not(edtTitleSearch.Text='') then
    begin
        with DM do
        begin
            tblArt.Open;
            tblArt.First;
            tblArt.Filtered:=False;
            tblArt.Filter:='[artName] like '+QuotedStr(edtTitleSearch.Text+'%');
            tblArt.Filtered:=True;
            clearGallery;
            numRecords:=tblArt.RecordCount;
            numRows:=numRecords div 3;
            if not(numRecords mod 3 = 0) then
            begin
                inc(numRows);
            end;

            for currentRow := 0 to numRows-1 do
            begin
                GridPanel.RowCollection.Add;
                GridPanel.RowCollection[currentRow].SizeStyle:=ssAbsolute;
                GridPanel.RowCollection[currentRow].Value:=350;
            end;

            // Sets the height for the grid panel and the scroll view
            GridPanel.Height:=350*numRows;
            ScrollBox.VertScrollBar.Range:=350*numRows;


            tblArt.First;
            while not tblArt.Eof do
            begin
                identifier:=tblArt['ID'];
                objName:='pnl'+identifier;
                P:=TPanel.Create(Self);
                P.Width:=220;
                P.Height:=300;
                P.ParentColor:=False;
                P.ParentBackground:=False;
                P.Color:=clBlack;
                P.Name:=objName;
                P.Parent:=GridPanel;

                objName:='img'+identifier;
                I:=TImage.Create(self);
                I.Parent:=P;
                I.Width:=180;
                I.Height:=180;
                I.Left:=20;
                I.Top:=20;
                I.Name:=objName;
                I.Picture.LoadFromFile(tblArt['artPath']);
                I.Stretch:=True;

                objName:='btnView'+identifier;
                BView:=TButton.Create(self);
                BView.Parent:=P;
                BView.Width:=180;
                BView.Height:=32;
                BView.Left:=20;
                BView.Top:=210;
                BView.Name:=objName;
                BView.Caption:='View Details ';
                BView.OnClick := PopulateArtworkDetail;

                objName:='btnCart'+identifier;
                BCart:=TButton.Create(self);
                BCart.Parent:=P;
                BCart.Width:=180;
                BCart.Height:=32;
                BCart.Left:=20;
                BCart.Top:=252;
                BCart.Name:=objName;

                tblCart.Open;
                tblCart.First;
                notInCart:=True;
                while not tblCart.Eof do
                begin
                    //showMessage(SessionID);
                    if tblCart['userID'] = SessionId then
                    begin
                        if tblCart['artID'] = identifier then
                        begin
                            notInCart:=False;
                            //showMessage('');
                        end;
                    end;
                    tblCart.Next;
                end;
                tblCart.Close;
                if notInCart then
                begin
                     BCart.Caption:='Add to Cart ';
                     BCart.OnClick:=AddArtToCart;
                end
                else
                begin
                    BCart.Caption:='View in Cart';
                    BCart.OnClick:=RouteToCart;
                end;


                tblArt.Next
            end;
            tblArt.Close;
        end;
    end
    else
    begin
        DM.tblArt.Filtered:=False;
        showAllArtwork;
    end;
end;

procedure TfrmArtworksGallery.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    Application.Terminate;
end;

procedure TfrmArtworksGallery.FormMouseWheelDown(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
    ScrollBox.VertScrollBar.Position :=  ScrollBox.VertScrollBar.Position + 12;
end;

procedure TfrmArtworksGallery.FormMouseWheelUp(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
    ScrollBox.VertScrollBar.Position :=  ScrollBox.VertScrollBar.Position - 12;
end;

procedure TfrmArtworksGallery.FormShow(Sender: TObject);
begin
    resetFilterBevel;
    btnPreferences.Hide;
    btnCart.Hide;
    btnToggleDropDown.BevelOuter:=bvRaised;
    btnToggleDropDown.Caption:= SessionName + ' ' + SessionSurname;
    edtTitleSearch.Hide;
    edtArtistSearch.Hide;
    dtpSearch.Hide;
    cmbCountry.Hide;
    seditMinPrice.hide;
    seditMaxPrice.hide;
    btnFilterNone.BevelOuter:=bvLowered;
    btnFilterNone.Color:=clGray;
    filter:='None';
    lblTitle.Caption:='All Artworks ';
    showAllArtwork;
    cmbCountry.items.clear;
    with DM do
    begin
      tblCountry.Open;
      tblCountry.First;
      while not tblCountry.Eof do
      begin
          cmbCountry.Items.Add(tblCountry['Country']);
          tblCountry.Next;
      end;
      tblCountry.Close;
    end;
end;

procedure TfrmArtworksGallery.populateArtworkDetail(Sender: TObject);
begin
    with Sender as TButton do
    begin
        ArtworkID:=copy(name, 8, length(name));
        //showMessage(ArtworkID);
        with DM do
        begin
            tblArt.Open;
            tblArt.First;
            while not tblArt.Eof do
            begin
                if tblArt['ID'] = ArtworkID then
                begin
                    ArtworkName:=tblArt['artName'];
                    ArtworkPrice:=tblArt['artPrice'];
                    ArtworkDescription:=tblArt['artDescription'];
                    ArtworkLength:=tblArt['artLength'];
                    ArtworkWidth:=tblArt['artWidth'];
                    ArtworkDepth:=tblArt['artDepth'];
                    ArtworkMaterial:=tblArt['artMaterial'];
                    ArtworkPath:=tblArt['artPath'];
                    ArtworkCreatedAt:=tblArt['artCreatedAt'];
                end;
                tblArt.Next;
            end;
            tblArt.Close;
        end;
        frmArtworkDetail.Show;
        self.Hide;
    end;
end;

procedure TfrmArtworksGallery.resetFilterBevel;
begin
    btnFilterNone.BevelOuter:=bvRaised;
    btnFilterNone.Color:=clSilver;
    btnFilterPrice.BevelOuter:=bvRaised;
    btnFilterPrice.Color:=clSilver;
    btnFilterCountry.BevelOuter:=bvRaised;
    btnFilterCountry.Color:=clSilver;
    btnFilterArtist.BevelOuter:=bvRaised;
    btnFilterArtist.Color:=clSilver;
    btnFilterTitle.BevelOuter:=bvRaised;
    btnFilterTitle.Color:=clSilver;
    btnFilterCreation.BevelOuter:=bvRaised;
    btnFilterCreation.Color:=clSilver;
end;

procedure TfrmArtworksGallery.RouteToCart(Sender: TObject);
begin
    frmCart.Show;
    self.Hide;
end;

procedure TfrmArtworksGallery.seditMinPriceChange(Sender: TObject);
var
  numRecords:integer;
  numRows:integer;
  currentRow:integer;
  identifier:string;
  objName:string;
  BView:TButton;
  BCart:TButton;
  I:TImage;
  P:TPanel;
  notInCart:boolean;

begin
    numRecords:=0;
    numRows:=0;
    identifier:='';
    objName:='';


    with DM do
    begin
        tblArt.Open;
        tblArt.First;
        tblArt.Filtered:=False;
        tblArt.Filter:='[artPrice] >= '+IntToStr(seditMinPrice.Value) + ' AND [artPrice] <= ' +INtToStr(seditMaxPrice.Value);
        tblArt.Filtered:=True;
        clearGallery;
        numRecords:=tblArt.RecordCount;
        numRows:=numRecords div 3;
        if not(numRecords mod 3 = 0) then
        begin
            inc(numRows);
        end;

        for currentRow := 0 to numRows-1 do
        begin
            GridPanel.RowCollection.Add;
            GridPanel.RowCollection[currentRow].SizeStyle:=ssAbsolute;
            GridPanel.RowCollection[currentRow].Value:=350;
        end;

        // Sets the height for the grid panel and the scroll view
        GridPanel.Height:=350*numRows;
        ScrollBox.VertScrollBar.Range:=350*numRows;


        tblArt.First;
        while not tblArt.Eof do
        begin
            identifier:=tblArt['ID'];
            objName:='pnl'+identifier;
            P:=TPanel.Create(Self);
            P.Width:=220;
            P.Height:=300;
            P.ParentColor:=False;
            P.ParentBackground:=False;
            P.Color:=clBlack;
            P.Name:=objName;
            P.Parent:=GridPanel;

            objName:='img'+identifier;
            I:=TImage.Create(self);
            I.Parent:=P;
            I.Width:=180;
            I.Height:=180;
            I.Left:=20;
            I.Top:=20;
            I.Name:=objName;
            I.Picture.LoadFromFile(tblArt['artPath']);
            I.Stretch:=True;

            objName:='btnView'+identifier;
            BView:=TButton.Create(self);
            BView.Parent:=P;
            BView.Width:=180;
            BView.Height:=32;
            BView.Left:=20;
            BView.Top:=210;
            BView.Name:=objName;
            BView.Caption:='View Details ';
            BView.OnClick := PopulateArtworkDetail;

            objName:='btnCart'+identifier;
            BCart:=TButton.Create(self);
            BCart.Parent:=P;
            BCart.Width:=180;
            BCart.Height:=32;
            BCart.Left:=20;
            BCart.Top:=252;
            BCart.Name:=objName;

            tblCart.Open;
            tblCart.First;
            notInCart:=True;
            while not tblCart.Eof do
            begin
                //showMessage(SessionID);
                if tblCart['userID'] = SessionId then
                begin
                    if tblCart['artID'] = identifier then
                    begin
                        notInCart:=False;
                        //showMessage('');
                    end;
                end;
                tblCart.Next;
            end;
            tblCart.Close;
            if notInCart then
            begin
                 BCart.Caption:='Add to Cart ';
                 BCart.OnClick:=AddArtToCart;
            end
            else
            begin
                BCart.Caption:='View in Cart';
                BCart.OnClick:=RouteToCart;
            end;
            tblArt.Next
        end;
        tblArt.Close;
    end;
end;

procedure TfrmArtworksGallery.showAllArtwork;
var
  numRecords:integer;
  numRows:integer;
  currentRow:integer;
  identifier:string;
  objName:string;
  BView:TButton;
  BCart:TButton;
  I:TImage;
  P:TPanel;
  notInCart:boolean;

begin
    numRecords:=0;
    numRows:=0;
    identifier:='';
    objName:='';


    with DM do
    begin
        tblArt.Open;
        tblArt.Filtered:=False;
        tblArt.First;
        clearGallery;
        numRecords:=tblArt.RecordCount;
        numRows:=numRecords div 3;
        if not(numRecords mod 3 = 0) then
        begin
            inc(numRows);
        end;

        for currentRow := 0 to numRows-1 do
        begin
            GridPanel.RowCollection.Add;
            GridPanel.RowCollection[currentRow].SizeStyle:=ssAbsolute;
            GridPanel.RowCollection[currentRow].Value:=350;
        end;

        // Sets the height for the grid panel and the scroll view
        GridPanel.Height:=350*numRows;
        ScrollBox.VertScrollBar.Range:=350*numRows;


        tblArt.First;
        while not tblArt.Eof do
        begin
            identifier:=tblArt['ID'];
            objName:='pnl'+identifier;
            P:=TPanel.Create(Self);
            P.Width:=220;
            P.Height:=300;
            P.ParentColor:=False;
            P.ParentBackground:=False;
            P.Color:=clBlack;
            P.Name:=objName;
            P.Parent:=GridPanel;

            objName:='img'+identifier;
            I:=TImage.Create(self);
            I.Parent:=P;
            I.Width:=180;
            I.Height:=180;
            I.Left:=20;
            I.Top:=20;
            I.Name:=objName;
            I.Picture.LoadFromFile(tblArt['artPath']);
            I.Stretch:=True;

            objName:='btnView'+identifier;
            BView:=TButton.Create(self);
            BView.Parent:=P;
            BView.Width:=180;
            BView.Height:=32;
            BView.Left:=20;
            BView.Top:=210;
            BView.Name:=objName;
            BView.Caption:='View Details ';
            BView.OnClick := PopulateArtworkDetail;

            objName:='btnCart'+identifier;
            BCart:=TButton.Create(self);
            BCart.Parent:=P;
            BCart.Width:=180;
            BCart.Height:=32;
            BCart.Left:=20;
            BCart.Top:=252;
            BCart.Name:=objName;

            tblCart.Open;
            tblCart.First;
            notInCart:=True;
            while not tblCart.Eof do
            begin
                //showMessage(SessionID);
                if tblCart['userID'] = SessionId then
                begin
                    if tblCart['artID'] = identifier then
                    begin
                        notInCart:=False;
                        //showMessage('');
                    end;
                end;
                tblCart.Next;
            end;
            tblCart.Close;
            if notInCart then
            begin
                 BCart.Caption:='Add to Cart ';
                 BCart.OnClick:=AddArtToCart;
            end
            else
            begin
                BCart.Caption:='View in Cart';
                BCart.OnClick:=RouteToCart;
            end;


            tblArt.Next
        end;
        tblArt.Close;
    end;
end;

end.
