unit ArtistDetail_u;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, jpeg;

type
  TfrmArtistDetail = class(TForm)
    lblBanner: TImage;
    imgArtistAvatar: TImage;
    lblName: TLabel;
    edtAritstName: TEdit;
    lblCountry: TLabel;
    edtArtistCountry: TEdit;
    edtArtistEmail: TEdit;
    edtArtistContact: TEdit;
    lblContact: TLabel;
    lblEmail: TLabel;
    lblBio: TLabel;
    memArtistBio: TMemo;
    btnBack: TPanel;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure btnBackMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure btnBackMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure btnBackClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmArtistDetail: TfrmArtistDetail;

implementation
uses
    ArtistsGallery_u, Global_u;

{$R *.dfm}

procedure TfrmArtistDetail.btnBackClick(Sender: TObject);
begin
    frmArtistsGallery.Show;
    self.Hide;
end;

procedure TfrmArtistDetail.btnBackMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
    btnBack.BevelOuter:=bvLowered;
end;

procedure TfrmArtistDetail.btnBackMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
    btnBack.BevelOuter:=bvRaised;
end;

procedure TfrmArtistDetail.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
    Params.ExStyle := Params.ExStyle or WS_EX_APPWINDOW;
    Params.WndParent := GetDesktopWindow;
end;

procedure TfrmArtistDetail.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    Application.Terminate;
end;

procedure TfrmArtistDetail.FormShow(Sender: TObject);
begin
    edtAritstName.Text:=ArtistName + ' ' + ArtistSurname;
    edtArtistCountry.Text:=ArtistCountry;
    edtArtistContact.Text:=ArtistContact;
    edtArtistEmail.Text:=ArtistEmail;
    memArtistBio.Text:=ArtistDescription;
    imgArtistAvatar.Picture.LoadFromFile(ArtistAvatar);
    imgArtistAvatar.Stretch:=True;
end;

end.
