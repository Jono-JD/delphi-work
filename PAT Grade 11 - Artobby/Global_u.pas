unit Global_u;

interface
procedure logout();

var
    SessionId: string;
    SessionName: string;
    SessionSurname: string;
    SessionEmail: string;
    SessionContact: string;
    SessionCountry: string;
    SessionUserType: string;
    SessionDescription:string;
    SessionAvatar:string;

    ArtworkID:string;
    ArtworkName:string;
    ArtworkDescription:string;
    ArtworkWidth:string;
    ArtworkLength:string;
    ArtworkDepth:string;
    ArtworkMaterial:string;
    ArtworkPrice:string;
    ArtworkPath:string;
    ArtworkCreatedAt:string;

    ArtistID:string;
    ArtistName:string;
    ArtistSurname:string;
    ArtistEmail:string;
    ArtistContact:string;
    ArtistDescription:string;
    ArtistAvatar:string;
    ArtistCountry:string;

    Delete: Array of string;

implementation
procedure logout();
begin
    SessionId:='';
    SessionName:='';
    SessionSurname:='';
    SessionEmail:='';
    SessionContact:='';
    SessionCountry:='';
    SessionUserType:='';
    SessionDescription:='';
    SessionAvatar:='';

    ArtworkID:='';
    ArtworkName:='';
    ArtworkDescription:='';
    ArtworkWidth:='';
    ArtworkLength:='';
    ArtworkDepth:='';
    ArtworkMaterial:='';
    ArtworkPrice:='';
    ArtworkPath:='';
    ArtworkCreatedAt:='';

    ArtistID:='';
    ArtistName:='';
    ArtistSurname:='';
    ArtistEmail:='';
    ArtistContact:='';
    ArtistDescription:='';
    ArtistAvatar:='';
    ArtistCountry:='';

    SetLength(Delete, 0);
end;

end.
